﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
using System.Web.Services;
using System.Configuration;

namespace rpa
{ 
    public partial class dec_ca : System.Web.UI.Page
    {
        public string decno;
        public string genid;
        string sNik = "";
        public string msg;
        cbll bll = new cbll();
        public static DataTable dtaaddcat = new DataTable();
        private int gveditindex = 0;
        public string js = "";
        string ServerUpload = ConfigurationManager.AppSettings["FileServer"];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }

            if (IsPostBack == false)
            {
                dtaaddcat = bll.FetchDec();
                Session["myDatatable"] = dtaaddcat;
                fillgrid();

                if (Request.QueryString["ID"] != null)
                {
                    List<cArrayList> arr = new List<cArrayList>();
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@decno", Request.QueryString["ID"]));
                    DataTable dtah = new DataTable();
                    bll.vGetDec(ref dtah, arr);

                    if (dtah.Rows.Count > 0)
                    {
                        hdecno.Value = Request.QueryString["ID"];
                        tripno1.Text = dtah.Rows[0]["pa_no"].ToString();
                        htripno.Value = dtah.Rows[0]["pa_no"].ToString();
                        dep.Value = dtah.Rows[0]["nmdepar"].ToString();
                        kddepar.Value = dtah.Rows[0]["kddepar"].ToString();
                        nmdepar.Value = dtah.Rows[0]["nmdepar"].ToString();
                        nik.Value = dtah.Rows[0]["nik"].ToString();
                        hnik.Value = dtah.Rows[0]["nik"].ToString();
                        nama.Value = dtah.Rows[0]["nama"].ToString();
                        hnama.Value= dtah.Rows[0]["nama"].ToString();
                        decdate.Text = dtah.Rows[0]["reqdate"].ToString();
                        hdecdate.Value = dtah.Rows[0]["reqdate"].ToString();
                        remark.Value = dtah.Rows[0]["remarks"].ToString();
                        site.Value = dtah.Rows[0]["kdsite"].ToString();
                        hsite.Value = dtah.Rows[0]["kdsite"].ToString();
                        superior.Value = dtah.Rows[0]["sup_nm"].ToString();
                        hsuperior.Value = dtah.Rows[0]["sup_nm"].ToString();
                        supnik.Value = dtah.Rows[0]["sup_nik"].ToString();
                        nmjabat.Value = dtah.Rows[0]["kdjabat"].ToString();
                        position.Value = dtah.Rows[0]["nmjabat"].ToString();
                        hposition.Value = dtah.Rows[0]["nmjabat"].ToString();
                        costcode.Value = dtah.Rows[0]["costcode"].ToString();
                        hcostcode.Value = dtah.Rows[0]["costcode"].ToString();
                        costcd.Value = dtah.Rows[0]["costname"].ToString();
                        fstatus.Value = dtah.Rows[0]["fstatus"].ToString();
                        hfstatus.Value = dtah.Rows[0]["fstatus"].ToString();
                        type.Value = dtah.Rows[0]["Detailtrip"].ToString();
                        htype.Value = dtah.Rows[0]["Detailtrip"].ToString();
                        cmpltsts.Value = dtah.Rows[0]["statusWF"].ToString();
                    }

                    Session["decno"] = Request.QueryString["ID"];

                    DataTable dtad = new DataTable();
                    bll.vGetDecList(ref dtad, arr);

                    if (dtad.Rows.Count > 0)
                    {
                        dtaaddcat = new DataTable();

                        dtaaddcat = dtad;


                        GridView1.DataSource = dtaaddcat;
                        GridView1.DataBind();
                        fillddlcat("ddlcatlistnew");
                    }
                    
                    List<cArrayList> arr2 = new List<cArrayList>();
                    arr2.Add(new cArrayList("@pa_no", Session["decno"].ToString()));
                    DataTable dta = new DataTable();
                    bll.vGetListfupld(ref dta, arr2);

                    gvfupld.DataSource = dta;
                    gvfupld.DataBind();
                }
                else
                {
                    sNik = HttpContext.Current.Session["sNik"].ToString();
                    if (sNik == "")
                    {
                        Response.Redirect("login.aspx");
                    }

                    decdate.Text = DateTime.Today.ToShortDateString();
                    hdecdate.Value = decdate.Text;

                    var conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
                    conn2.Open();
                    var dtb2 = new DataTable();
                    string strcon2 = "Select max(genid) as genid from CASH_ADV_DEC_D";
                    var sda2 = new SqlDataAdapter(strcon2, conn2);
                    sda2.Fill(dtb2);
                    if (dtb2.Rows.Count == 1)
                    {
                        if (genid == null || genid == "")
                        {
                            genid = "0";
                        }
                        else
                        {
                            genid = dtb2.Rows[0]["genid"].ToString();
                        }
                    }
                    else
                    {
                        genid = "0";
                    }

                    conn2.Close();

                    btnUpload.Enabled = false;
                    fuploadPA.Enabled = false;
                }
                js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtdtperiodnew").ClientID + "', '%m/%d/%Y');</script>";
                checktxtsts();
            }

            

            //List<cArrayList> arr2 = new List<cArrayList>();
            //arr2 = new List<cArrayList>();
            //DataTable dtadesc = new DataTable();
            //arr2.Add(new cArrayList("@desc", "%%"));
            //bll.vGetDescList(ref dtadesc, arr2);

            //expensetype1
        }

        protected void UploadFile(object sender, EventArgs e)
        {
            //string folderPath = Server.MapPath("\\20.110.1.30\\Files\\upload_" + DateTime.Now.ToString("yyyyMMdd") + "/");
            //string folderPath = "\\\\20.110.1.30\\Files\\upload_" + DateTime.Now.ToString("yyyyMMdd") + "\\" + hdecno.Value + "\\";
            string folderPath = "\\\\" + ServerUpload + "\\Files\\upload_" + DateTime.Now.ToString("yyyyMMdd") + "\\" + hdecno.Value + "\\";

            //20.110.1.30
            //Check whether Directory (Folder) exists.
            if (!Directory.Exists(folderPath))
            {
                //If Directory (Folder) does not exists. Create it.
                Directory.CreateDirectory(folderPath);
            }
            if (fuploadPA.PostedFile.ContentLength < 5000000)
            {
                if (fuploadPA.FileName != "")
                {
                    DataTable dtafn = new DataTable();
                    List<cArrayList> arrfn = new List<cArrayList>();
                    arrfn = new List<cArrayList>();
                    arrfn.Add(new cArrayList("@pa_no", hdecno.Value));
                    DataTable dtah = new DataTable();
                    bll.vGetFilenm(ref dtafn, arrfn);

                    if (dtafn.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtafn.Rows.Count; q++)
                        {
                            if (fuploadPA.FileName == dtafn.Rows[q]["filename"].ToString())
                            {
                                msg = "<script type='text/javascript'> alert('File already exist please select another file'); </script>"; ;
                                
                                fillgrid();
                                return;
                            }
                        }
                    }

                    //Save the File to the Directory (Folder).
                    fuploadPA.SaveAs(folderPath + Path.GetFileName(fuploadPA.FileName));

                    //Display the success message.
                    lblMessage.Text = Path.GetFileName(fuploadPA.FileName) + " has been uploaded.";

                    List<cArrayList> arr = new List<cArrayList>();

                    arr.Add(new cArrayList("@FileName", fuploadPA.FileName));
                    arr.Add(new cArrayList("@FileExtension", Path.GetExtension(fuploadPA.PostedFile.FileName)));
                    arr.Add(new cArrayList("@fpath", folderPath + Path.GetFileName(fuploadPA.FileName)));
                    arr.Add(new cArrayList("@pa_no", Session["decno"].ToString()));
                    arr.Add(new cArrayList("@fsize", fuploadPA.PostedFile.ContentLength));

                    bll.vInsertfupload(arr);

                    List<cArrayList> arr2 = new List<cArrayList>();
                    arr2.Add(new cArrayList("@pa_no", Session["decno"].ToString()));
                    DataTable dta = new DataTable();
                    bll.vGetListfupld(ref dta, arr2);

                    gvfupld.DataSource = dta;
                    gvfupld.DataBind();

                    var result = dta.AsEnumerable()
                            .Sum(x => Convert.ToInt32(x["fsize"]));
                    hfsize.Value = result.ToString();
                }
                else
                {
                    msg = "<script type='text/javascript'> alert('File empty, please select file first'); </script>";
                }
            }
            else
            {
                msg = "<script type='text/javascript'> alert('File Upload Cannot more than 5MB'); </script>";
            }
            fillgrid();
        }

        protected void btnprint_Click(object sender, EventArgs e)
        {
            Response.Redirect("payment_application_internal_form.aspx?id=" + Session["decno"].ToString()); 
        }

        protected void cetak_Click(object sender, EventArgs e)
        {

        }

        protected void cetak_Click_Payment(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            List<cArrayList> arr = new List<cArrayList>();
            arr.Add(new cArrayList("@DocNumber", Session["decno"].ToString()));


            bll.vSubmitDec(arr);
            cmpltsts.Value = "INT";
            checktxtsts();
            msg = "<script type='text/javascript'> alert('Data has been successfully submit'); </script>";
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;

            fillgridedit();
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtdtperiodnew").ClientID + "', '%m/%d/%Y');</script>";
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            //gveditindex = GridView1.EditIndex;
            fillgridedit();

            List<cArrayList> arr = new List<cArrayList>();
            DropDownList ddlcatlist = (DropDownList)GridView1.Rows[GridView1.EditIndex].FindControl("ddlcatlist");

            arr = new List<cArrayList>();
            DataTable dtadesc = new DataTable();
            arr.Add(new cArrayList("@desc", "%%"));
            bll.vGetDescList(ref dtadesc, arr);

            if (dtadesc.Rows.Count > 0)
            {
                ddlcatlist.DataSource = dtadesc;
                ddlcatlist.DataTextField = "DESCRIPTION";
                ddlcatlist.DataValueField = "COA";
                ddlcatlist.DataBind();
            }

            ddlcatlist.SelectedValue = dtaaddcat.Rows[GridView1.EditIndex]["dectype-id"].ToString();
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtdtperiodnew").ClientID + "', '%m/%d/%Y');</script>";
            //csscp = "style='display:none;'";
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TextBox txtdatedec = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtdtperiod");
            DropDownList ddlcatlist = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlcatlist");
            DropDownList ddlcurrlist = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlcurrlist");
            TextBox txtNewbiayatot = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtbiayatot");
            TextBox txtNewremarks = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtremarks");

            bll.UpdateDec(txtdatedec.Text, ddlcatlist.SelectedItem.ToString(), ddlcurrlist.SelectedItem.ToString(), txtNewbiayatot.Text, txtNewremarks.Text, dtaaddcat, ddlcatlist.SelectedValue.ToString(),e.RowIndex);
            GridView1.EditIndex = -1;
            fillgrid();

            var result = dtaaddcat.AsEnumerable()
                    .Sum(x => Convert.ToInt32(x["amt"]));
            htotamt.Value = result.ToString();
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtdtperiodnew").ClientID + "', '%m/%d/%Y');</script>";
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                TextBox txtdatedec = (TextBox)GridView1.FooterRow.FindControl("txtdtperiodnew");
                DropDownList ddlcatlist = (DropDownList)GridView1.FooterRow.FindControl("ddlcatlistnew");
                DropDownList ddlcurrlist = (DropDownList)GridView1.FooterRow.FindControl("ddlcurrlistnew");
                TextBox txtNewbiayatot = (TextBox)GridView1.FooterRow.FindControl("txtNewbiayatot");
                TextBox txtNewremarks = (TextBox)GridView1.FooterRow.FindControl("txtNewremarks");

                if (txtNewbiayatot.Text == "" || txtNewbiayatot.Text == "0")
                {
                    msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
                    //checkalokasi();
                    fillgrid();
                    return;
                }
                //bll.InsertDec("1", "12", "IDR", txtNewbiayatot.Text, txtNewremarks.Text, dtaaddcat, "12");
                bll.InsertDec(txtdatedec.Text,ddlcatlist.SelectedItem.ToString(),ddlcurrlist.SelectedItem.ToString(), txtNewbiayatot.Text, txtNewremarks.Text, dtaaddcat, ddlcatlist.SelectedValue.ToString());
                fillgrid();

                var result = dtaaddcat.AsEnumerable()
                    .Sum(x => Convert.ToInt32(x["amt"]));
                htotamt.Value = result.ToString();

                js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtdtperiodnew").ClientID + "', '%m/%d/%Y');</script>";
                readdheader();
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            bll.DeleteDec(dtaaddcat, e.RowIndex);
            fillgrid();
            if (dtaaddcat.Rows[0]["amt"].ToString() == "")
            {
                dtaaddcat.Rows[0]["amt"] = "0";
            }
                var result = dtaaddcat.AsEnumerable()
                        .Sum(x => Convert.ToInt32(x["amt"]));
                htotamt.Value = result.ToString();

                js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtdtperiodnew").ClientID + "', '%m/%d/%Y');</script>";            
        }

        private void fillgrid()
        {
            if (dtaaddcat.Rows.Count > 0)
            {
                if (dtaaddcat.Rows[0]["amt"].ToString() == "")
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();
                    int TotalColumns = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                    GridView1.Rows[0].Cells[0].Text = "Entry Declaration";
                    GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                    Session["myDatatable"] = dtaaddcat;

                    fillddlcat("ddlcatlist");
                }
                else
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();

                    fillddlcat("ddlcatlistnew");
                }
            }
            else
            {
                dtaaddcat.Rows.Add(dtaaddcat.NewRow());
                GridView1.DataSource = dtaaddcat;
                GridView1.DataBind();
                int TotalColumns = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                GridView1.Rows[0].Cells[0].Text = "Entry Declaration";
                GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                Session["myDatatable"] = dtaaddcat;

                fillddlcat("ddlcatlistnew");
            }
        }

        private void fillgridedit()
        {
            if (dtaaddcat.Rows.Count > 0)
            {
                if (dtaaddcat.Rows[0]["amt"].ToString() == "")
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();
                    int TotalColumns = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                    GridView1.Rows[0].Cells[0].Text = "Entry Declaration";
                    GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                    Session["myDatatable"] = dtaaddcat;

                    fillddlcat("ddlcatlist");
                }
                else
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();
                    fillddlcat("ddlcatlistnew");
                }
            }
            else
            {
                dtaaddcat.Rows.Add(dtaaddcat.NewRow());
                GridView1.DataSource = dtaaddcat;
                GridView1.DataBind();
                int TotalColumns = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                GridView1.Rows[0].Cells[0].Text = "Entry Declaration";
                GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                Session["myDatatable"] = dtaaddcat;

                fillddlcat("ddlcatlistnew");
            }
        }

        private void fillddlcat(string sddlcatid)
        {
            List<cArrayList> arr = new List<cArrayList>();

            GridViewRow footrow = GridView1.FooterRow;
            GridViewRow editrow = GridView1.SelectedRow;
            //DropDownList ddlcatlist = (DropDownList)GridView1.Rows[0].FindControl("ddlcatlist");
            DropDownList ddlcatlist = (DropDownList)footrow.FindControl(sddlcatid);

            arr = new List<cArrayList>();
            DataTable dtadesc = new DataTable();
            arr.Add(new cArrayList("@desc", "%%"));
            bll.vGetDescList(ref dtadesc, arr);

            if (dtadesc.Rows.Count > 0)
            {
                if (ddlcatlist != null)
                {
                    ddlcatlist.DataSource = dtadesc;
                    ddlcatlist.DataTextField = "DESCRIPTION";
                    ddlcatlist.DataValueField = "COA";
                    ddlcatlist.DataBind();
                }
                else
                {
                    ddlcatlist = (DropDownList)footrow.FindControl("ddlcatlistnew");
                    ddlcatlist.DataSource = dtadesc;
                    ddlcatlist.DataTextField = "DESCRIPTION";
                    ddlcatlist.DataValueField = "COA";
                    ddlcatlist.DataBind();
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string decno = "";
            string dtmnow;
            string dtynow;
            string stripno;
            string vald = "";
            int i;
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
            if (hdecno.Value.ToString() != "")
            {
                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@DecNo", hdecno.Value));
                arr.Add(new cArrayList("@pa_no", htripno.Value));

                arr.Add(new cArrayList("@Remarks", remark.Value));
                arr.Add(new cArrayList("@check_sup", "0"));
                arr.Add(new cArrayList("@currcode", "IDR"));
                arr.Add(new cArrayList("@ModifiedBy", Session["sNiksite"].ToString()));
                arr.Add(new cArrayList("@StEdit", "0"));
                arr.Add(new cArrayList("@fstatus", "0"));
                arr.Add(new cArrayList("@nik", hnik.Value));
                arr.Add(new cArrayList("@kddepar", kddepar.Value));
                arr.Add(new cArrayList("@cashadvamt", htotamt.Value));
                bll.vUpdateDecH(arr);

                List<cArrayList> arrdeletedet = new List<cArrayList>();
                arrdeletedet.Add(new cArrayList("@decno", hdecno.Value));
                

                if (dtaaddcat.Rows.Count > 0)
                {
                    bll.vDeleteDecList(arrdeletedet);
                    for (int q = 0; q < dtaaddcat.Rows.Count; q++)
                    {
                        List<cArrayList> arrd = new List<cArrayList>();
                        arrd.Add(new cArrayList("@DecNo", hdecno.Value));
                        arrd.Add(new cArrayList("@genId", dtaaddcat.Rows[q]["no"].ToString()));
                        arrd.Add(new cArrayList("@decdate", dtaaddcat.Rows[q]["decdate"].ToString()));
                        arrd.Add(new cArrayList("@expensetype", dtaaddcat.Rows[q]["dectype-id"].ToString()));
                        arrd.Add(new cArrayList("@currency", dtaaddcat.Rows[q]["curr"].ToString()));
                        arrd.Add(new cArrayList("@subtotal", dtaaddcat.Rows[q]["amt"].ToString()));
                        arrd.Add(new cArrayList("@Remarks", dtaaddcat.Rows[q]["remarks"].ToString()));
                        bll.vInsDecD(arrd, ref vald);
                    }
                }

                fillgrid();
                checktxtsts();
                msg = "<script type='text/javascript'> alert('Data has been successfully update'); </script>";
            }
            else
            {
                try
                {
                    conn.Open();
                    dtmnow = DateTime.Now.ToString("MM");
                    dtynow = DateTime.Now.Year.ToString();
                    stripno = dtynow + dtmnow + "/" + "PAI" + "/" + "D" + "/";
                    var dtb = new DataTable();
                    string strcon = "select Decno from CASH_ADV_DEC where Decno like '%" + stripno + "%'";
                    var sda = new SqlDataAdapter(strcon, conn);
                    sda.Fill(dtb);
                    if (dtb.Rows.Count == 0)
                    {
                        decno = stripno + "000001";
                    }
                    else if (dtb.Rows.Count > 0)
                    {
                        i = 0;
                        i = dtb.Rows.Count + 1;
                        if (dtb.Rows.Count > 0 & dtb.Rows.Count < 9)
                        {
                            decno = stripno + "00000" + i.ToString();
                        }
                        else if (dtb.Rows.Count >= 9 & dtb.Rows.Count < 99)
                        {
                            decno = stripno + "0000" + i.ToString();
                        }
                        else if (dtb.Rows.Count >= 99 & dtb.Rows.Count < 999)
                        {
                            decno = stripno + "000" + i.ToString();
                        }
                        else if (dtb.Rows.Count >= 999 & dtb.Rows.Count < 9999)
                        {
                            decno = stripno + "00" + i.ToString();
                        }
                        else if (dtb.Rows.Count >= 9999 & dtb.Rows.Count < 99999)
                        {
                            decno = stripno + "0" + i.ToString();
                        }
                        else if (dtb.Rows.Count >= 99999 & dtb.Rows.Count < 999999)
                        {
                            decno = stripno + i.ToString();
                        }
                        else if (dtb.Rows.Count >= 999999)
                        {
                            decno = "Error on generate Tripnum";
                        }
                    }

                    conn.Close();
                    
                    List<cArrayList> arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@DecNo", decno));
                    arr.Add(new cArrayList("@pa_no", htripno.Value));

                    arr.Add(new cArrayList("@Remarks", remark.Value));
                    arr.Add(new cArrayList("@check_sup", "0"));
                    arr.Add(new cArrayList("@currcode", "IDR"));
                    arr.Add(new cArrayList("@CreatedBy", Session["sNiksite"].ToString()));
                    arr.Add(new cArrayList("@ModifiedBy", Session["sNiksite"].ToString()));
                    arr.Add(new cArrayList("@StEdit", "0"));
                    arr.Add(new cArrayList("@fstatus", "0"));
                    arr.Add(new cArrayList("@nik", hnik.Value));
                    arr.Add(new cArrayList("@kddepar", kddepar.Value));
                    arr.Add(new cArrayList("@sup_nik", supnik.Value));
                    arr.Add(new cArrayList("@cashadvamt", htotamt.Value));
                    bll.vInsDecH(arr, ref vald);

                    hdecno.Value = decno;
                    Session["decno"] = decno;
                    if (dtaaddcat.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtaaddcat.Rows.Count; q++)
                        {
                            List<cArrayList> arrd = new List<cArrayList>();
                            arrd.Add(new cArrayList("@DecNo", decno));
                            arrd.Add(new cArrayList("@genId", dtaaddcat.Rows[q]["no"].ToString()));
                            arrd.Add(new cArrayList("@decdate", dtaaddcat.Rows[q]["decdate"].ToString()));
                            arrd.Add(new cArrayList("@expensetype", dtaaddcat.Rows[q]["dectype-id"].ToString()));
                            arrd.Add(new cArrayList("@currency", dtaaddcat.Rows[q]["curr"].ToString()));
                            arrd.Add(new cArrayList("@subtotal", dtaaddcat.Rows[q]["amt"].ToString()));
                            arrd.Add(new cArrayList("@Remarks", dtaaddcat.Rows[q]["remarks"].ToString()));
                            bll.vInsDecD(arrd, ref vald);
                        }
                    }

                    fillgrid();
                    btnUpload.Enabled = true;
                    fuploadPA.Enabled = true;
                    msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                }

                catch (Exception ex)
                {
                    msg = ex.ToString();
                }
            }
            
        }

        private void readdheader()
        {
            tripno1.Text = htripno.Value;
            dep.Value = nmdepar.Value;
            nik.Value = hnik.Value;
            nama.Value = hnama.Value;
            site.Value = hsite.Value;
            superior.Value = hsuperior.Value;
            position.Value = hposition.Value;
            costcode.Value = hcostcode.Value;
            fstatus.Value = hfstatus.Value;
            type.Value = htype.Value;
        }

        private void checktxtsts()
        {
            //fstatus.Value
            if (cmpltsts.Value != "")
            {
                btnsubmit.Enabled = false;
                btnsave.Enabled = false;
                GridView1.Enabled = false;
                fuploadPA.Enabled = false;
                gvfupld.Enabled = false;
                btnUpload.Enabled = false;
            }
            else
            {
                btnsubmit.Enabled = true;
                btnsave.Enabled = true;
            }
        }

        protected void gvfupld_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //e.RowIndex

            List<cArrayList> arrdeletedet = new List<cArrayList>();
            arrdeletedet.Add(new cArrayList("@pa_no", hdecno.Value));
            arrdeletedet.Add(new cArrayList("@filenm", gvfupld.Rows[e.RowIndex].Cells[2].Text));
            bll.vDeleteFilename(arrdeletedet);


            List<cArrayList> arr2 = new List<cArrayList>();
            arr2.Add(new cArrayList("@pa_no", hdecno.Value));
            DataTable dta = new DataTable();
            bll.vGetListfupld(ref dta, arr2);

            gvfupld.DataSource = dta;
            gvfupld.DataBind();
            
        }
    }
}