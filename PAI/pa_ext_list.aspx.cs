﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;

namespace rpa
{
    public partial class pa_ext_list : System.Web.UI.Page
    {
        cbll bll = new cbll();
        string sNik = "";
        string sNiksite = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }

            if (IsPostBack == false)
            {
                sNik = HttpContext.Current.Session["sNik"].ToString();
                sNiksite = HttpContext.Current.Session["sNiksite"].ToString();

                DataTable dtadesc = new DataTable();
                bll.vGetListSite(ref dtadesc);
                if (dtadesc.Rows.Count > 0)
                {
                    ddlsite.DataSource = dtadesc;
                    ddlsite.DataTextField = "nmsite";
                    ddlsite.DataValueField = "compid";
                    ddlsite.DataBind();
                }

                dtadesc = new DataTable();
                bll.vGetListDept(ref dtadesc);
                if (dtadesc.Rows.Count > 0)
                {
                    ddldpt.DataSource = dtadesc;
                    ddldpt.DataTextField = "NmDepar";
                    ddldpt.DataValueField = "KdDepar";
                    ddldpt.DataBind();
                }

                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@nikrequestor", sNiksite));
                DataTable dta = new DataTable();
                bll.vGetListPAEXT(ref dta, arr);

                GridView1.DataSource = dta;
                GridView1.DataBind();
            }
        }

        protected void btnNewReq_Click(object sender, EventArgs e)
        {
            Response.Redirect("pa_ext_entry.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("pa_ext_list.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string sdepar = ddldpt.SelectedValue;
            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();  
            if (sdepar == "-") { sdepar = null; }

            List<cArrayList> arr = new List<cArrayList>();
            arr.Add(new cArrayList("@nikrequestor", sNiksite));
            arr.Add(new cArrayList("@loc_id", ddlsite.SelectedValue));
            arr.Add(new cArrayList("@KdDepar", sdepar));
            arr.Add(new cArrayList("@VendorName", txtvendor.Text));
            arr.Add(new cArrayList("@request_type", ddlusagetyplist.SelectedValue));
            arr.Add(new cArrayList("@Status", ddlreqstslist.SelectedValue));
            DataTable dta = new DataTable();
            bll.vGetListPAEXT(ref dta, arr);

            GridView1.DataSource = dta;
            GridView1.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["pa_nox"] = Convert.ToString(GridView1.SelectedRow.Cells[1].Text);
            Session["bListPax"] = true;
            Response.Redirect("pa_ext_entry.aspx");
        }

        protected void gridViewPAList(object sender, GridViewRowEventArgs e)
        {

        }
    }
}