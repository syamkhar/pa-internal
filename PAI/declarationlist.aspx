﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="declarationlist.aspx.cs" Inherits="rpa.declarationlist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
        <meta charset="utf-8" />
        <title>CASH ADVANCE DECLARATION LIST</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for editable datatable samples" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        
        <script src="assets/calendar/jquery.min.js" type="text/javascript"></script>
         <script src="assets/calendar/tcal.js" type="text/javascript"></script>
        <link href="assets/calendar/tcal.css" rel="stylesheet" type="text/css" />
        <script src="assets/calendar/jscal2.js" type="text/javascript"></script>
        <script src="assets/calendar/lang/en.js" type="text/javascript"></script>
        <link href="assets/calendar/jscal2.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/border-radius.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/win2k/win2k.css" rel="stylesheet" type="text/css" />
    <script src="assets/calendar/jsm01.js" type="text/javascript"></script>
    <script src="assets/calendar/jquery-1.3.2.min.js" type="text/javascript"></script>
        
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white form-group">
    <form id="form1" runat="server" class="page-wrapper">
    <div class="page-wrapper">
             <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="rpa_entry.aspx">
                            <img src="assets/layouts/layout/img/logo.jpg" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->                           
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <%--<li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>--%>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
             <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <%--<form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>--%>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <%--<li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_2.html" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Dashboard 2</span>
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_3.html" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">Dashboard 3</span>
                                            <span class="badge badge-danger">5</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>--%>
                           <%-- <li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li>--%>
                            
                            
                            <li class="nav-item  active open">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">RPA</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="rpa_entry.aspx" class="nav-link ">
                                            <span class="title">
                                                Entry  
                                                <br/>Payment Aplication</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                    
                                
                                    <li class="nav-item">
                                        <a href="rpa_pa_list.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Payment Aplication</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>

                                     <li class="nav-item">
                                        <a href="dec_ca.aspx" class="nav-link ">
                                            <span class="title">Declaration  
                                                <br/>Cash Advanse Non BTR</span>
                                            <span></span>
                                        </a>
                                    </li>

                                    <li class="nav-item active open">
                                        <a href="declarationlist.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Declaration Non BTR</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>

                                    <li class="nav-item" >
                                        <a href="reports.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Reports</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>

                                 </ul>
                            </li>
                            
                            <%--<li class="heading">
                                <h3 class="uppercase">Layouts</h3>
                            </li>--%>
                            
                           
                            <%--<li class="heading">
                                <h3 class="uppercase">Pages</h3>
                            </li>--%>
                            
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <%--<div class="theme-panel hidden-xs hidden-sm">
                            <div class="toggler"> </div>
                            <div class="toggler-close"> </div>
                            <div class="theme-options">
                                <div class="theme-option theme-colors clearfix">
                                    <span> THEME COLOR </span>
                                    <ul>
                                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                    </ul>
                                </div>
                                <div class="theme-option">
                                    <span> Theme Style </span>
                                    <select class="layout-style-option form-control input-sm">
                                        <option value="square" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Layout </span>
                                    <select class="layout-option form-control input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Header </span>
                                    <select class="page-header-option form-control input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Top Menu Dropdown</span>
                                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                                        <option value="light" selected="selected">Light</option>
                                        <option value="dark">Dark</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Mode</span>
                                    <select class="sidebar-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Menu </span>
                                    <select class="sidebar-menu-option form-control input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Style </span>
                                    <select class="sidebar-style-option form-control input-sm">
                                        <option value="default" selected="selected">Default</option>
                                        <option value="light">Light</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Position </span>
                                    <select class="sidebar-pos-option form-control input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Footer </span>
                                    <select class="page-footer-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                            </div>
                        </div>--%>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Payment Application</span>
                                </li>
                                
                               
                            </ul>
                           <%-- <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">
                                                <i class="icon-bell"></i> Action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-shield"></i> Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-user"></i> Something else here</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-bag"></i> Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>--%>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> PAYMENT APPLICATION (INTERNAL)
                            <small>Declaration Cash Advance List</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
    <div class="row ">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-dark"></i>
                        <span class="caption-subject font-dark bold uppercase">ENTRY DEC</span>
                    </div>
                    <%--<div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>--%>
                    <br />
                    <hr />
    <div class="row">
                <!-- DEC START-->
                

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Business Declaration List</caption>
<tr>
<td style="width:5%;">Periode</td>
<td style="width:8%;">
<asp:DropDownList ID="ddlbulan" runat="server"></asp:DropDownList>
<asp:TextBox ID="tripfrom" style="width:70px;" runat="server" width="90px" ReadOnly="true" Visible ="false"></asp:TextBox></td>
<td style="width:5%;"><asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList></td>
<td style="width:5%;"><asp:Button ID="btnSearch" runat="server" Text="Search" 
        onclick="btnSearch_Click"/></td>
<td style="width:5%;"><input type="button" id="cetak" value="Print" onclick="frmcetak()"  style="width: 80px; font-family: Calibri; visibility:hidden;" visible="false"  /></td>
<td>
 <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>
</td>
<td></td>
</tr></table>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="decno" 
                    AllowPaging="true" PageSize="100" Width="1000px" 
                    onpageindexchanged="GridView1_PageIndexChanged" 
                    onpageindexchanging="GridView1_PageIndexChanging" 
                    onselectedindexchanged="GridView1_SelectedIndexChanged">
    <RowStyle Font-Size="Smaller" />
<Columns>
<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  
        HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:CommandField>
            <asp:BoundField DataField="decno" HeaderText="Declaration Number" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="pa_no" HeaderText="Trip Number" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="transdate" HeaderText="Create Date" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="nik" HeaderText="NIK" 
        HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="nama" HeaderText="Name" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <asp:BoundField DataField="modifiedtime" HeaderText="Last Edit" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
<asp:BoundField DataField="fstatus" HeaderText="Status" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>   

</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>
   
    
    </div>
        
        </div>
        </div>
        </div>

        <!-- END SAMPLE FORM PORTLET-->
                                
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            
            </div>
            </div>
    </form>
</body>
</html>
