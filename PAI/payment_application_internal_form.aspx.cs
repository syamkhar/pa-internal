﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace rpa
{
    public partial class payment_application_internal_form : System.Web.UI.Page
    {
        laporan.ReportLogic rptlogic = new laporan.ReportLogic();
        string sNik = "";
        string sNiksite = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            sNik = HttpContext.Current.Session["sNik"].ToString();
            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();
            if (IsPostBack == false)
            {
                //ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/laporan/rpt_payment_application_internal.rdlc");

                //DataTable table1 = rptlogic.GetPaymentApplicationUsage(sNik, cd.iPano.ToString(), cd.sPaReasn, cd.sPaAmt, cd.sPaDt, sNiksite);
                //DataSet dspa = new DataSet("PaymentApplicationInternal");
                //dspa.Tables.Add(table1);

                //ReportDataSource datasource = new ReportDataSource("DataSetReport_PaymentApplicationInternal", dspa.Tables[0]);
                //ReportViewer1.LocalReport.DataSources.Clear();
                //ReportViewer1.LocalReport.DataSources.Add(datasource);

                string decno = Request.QueryString["id"];
                arg1.Text = decno;
                arg2.Text = decno;
                arg3.Text = decno;
            }
        }
    }
}