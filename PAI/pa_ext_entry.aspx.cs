﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;

namespace rpa
{
    public partial class pa_ext_entry : System.Web.UI.Page
    {
        cbll bll = new cbll();
        public string msg;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            if (IsPostBack == false)
            {
                txtdtpa.Text = DateTime.Now.ToString("yyyy-MM-dd hh:mm tt");
                lblpemohon.Text = Session["sNama"].ToString();
                hnikpemohon.Value = Session["sNiksite"].ToString();
                btnsubmit.Enabled = false;

                DataTable dtadesc = new DataTable();
                bll.vGetListSite(ref dtadesc);
                if (dtadesc.Rows.Count > 0)
                {
                    ddlsite.DataSource = dtadesc;
                    ddlsite.DataTextField = "nmsite";
                    ddlsite.DataValueField = "compid";
                    ddlsite.DataBind();
                }

                dtadesc = new DataTable();
                bll.vGetListDept(ref dtadesc);
                if (dtadesc.Rows.Count > 0)
                {
                    ddldpt.DataSource = dtadesc;
                    ddldpt.DataTextField = "NmDepar";
                    ddldpt.DataValueField = "KdDepar";
                    ddldpt.DataBind();
                }

                DataTable dtacurr = new DataTable();
                bll.vGetListCurr(ref dtacurr);

                if (dtacurr.Rows.Count > 0)
                {
                    ddlcurbayar.DataSource = dtacurr;
                    ddlcurbayar.DataTextField = "currtyp";
                    ddlcurbayar.DataValueField = "currtyp";
                    ddlcurbayar.DataBind();
                    ddlcurbayar.SelectedValue = "IDR";

                    ddlcurbayar2.DataSource = dtacurr;
                    ddlcurbayar2.DataTextField = "currtyp";
                    ddlcurbayar2.DataValueField = "currtyp";
                    ddlcurbayar2.DataBind();
                    ddlcurbayar2.SelectedValue = "IDR";

                    ddlcurbayar3.DataSource = dtacurr;
                    ddlcurbayar3.DataTextField = "currtyp";
                    ddlcurbayar3.DataValueField = "currtyp";
                    ddlcurbayar3.DataBind();
                    ddlcurbayar3.SelectedValue = "IDR";

                    ddlcurbayar4.DataSource = dtacurr;
                    ddlcurbayar4.DataTextField = "currtyp";
                    ddlcurbayar4.DataValueField = "currtyp";
                    ddlcurbayar4.DataBind();
                    ddlcurbayar4.SelectedValue = "IDR";

                    ddlcurbayar5.DataSource = dtacurr;
                    ddlcurbayar5.DataTextField = "currtyp";
                    ddlcurbayar5.DataValueField = "currtyp";
                    ddlcurbayar5.DataBind();
                    ddlcurbayar5.SelectedValue = "IDR";

                    ddlcurbayar6.DataSource = dtacurr;
                    ddlcurbayar6.DataTextField = "currtyp";
                    ddlcurbayar6.DataValueField = "currtyp";
                    ddlcurbayar6.DataBind();
                    ddlcurbayar6.SelectedValue = "IDR";

                    ddlcurbayar7.DataSource = dtacurr;
                    ddlcurbayar7.DataTextField = "currtyp";
                    ddlcurbayar7.DataValueField = "currtyp";
                    ddlcurbayar7.DataBind();
                    ddlcurbayar7.SelectedValue = "IDR";

                    ddlcurbayar8.DataSource = dtacurr;
                    ddlcurbayar8.DataTextField = "currtyp";
                    ddlcurbayar8.DataValueField = "currtyp";
                    ddlcurbayar8.DataBind();
                    ddlcurbayar8.SelectedValue = "IDR";

                    ddlcurbayar9.DataSource = dtacurr;
                    ddlcurbayar9.DataTextField = "currtyp";
                    ddlcurbayar9.DataValueField = "currtyp";
                    ddlcurbayar9.DataBind();
                    ddlcurbayar9.SelectedValue = "IDR";

                    ddlcurbayar10.DataSource = dtacurr;
                    ddlcurbayar10.DataTextField = "currtyp";
                    ddlcurbayar10.DataValueField = "currtyp";
                    ddlcurbayar10.DataBind();
                    ddlcurbayar10.SelectedValue = "IDR";
                }

                if (Convert.ToBoolean(Session["bListPax"].ToString()) == true)
                {
                    List<cArrayList> arr = new List<cArrayList>();
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", Session["pa_nox"].ToString()));
                    DataTable dtah = new DataTable();
                    bll.vGetPAEXTbyid(ref dtah, arr);
                    if (dtah.Rows.Count > 0)
                    {
                        txtpano.Text = dtah.Rows[0]["pa_ext_no"].ToString();
                        txtdtpa.Text = dtah.Rows[0]["dt"].ToString();
                        ddlsite.SelectedValue = dtah.Rows[0]["loc_id"].ToString();
                        ddldpt.SelectedValue = dtah.Rows[0]["KdDepar"].ToString();
                        lblpemohon.Text = dtah.Rows[0]["Nama"].ToString();
                        hnikpemohon.Value = dtah.Rows[0]["NikRequestor"].ToString();
                        txtvendor.Text = dtah.Rows[0]["VendorName"].ToString();
                        hvendorid.Value = dtah.Rows[0]["VendorId"].ToString();
                        txtsts.Text = dtah.Rows[0]["request_sts"].ToString();
                        ddlreqtyp.SelectedValue = dtah.Rows[0]["request_type"].ToString();
                        txtremarksh.Text = dtah.Rows[0]["HRemarks"].ToString();
                        hstatuswf.Value = dtah.Rows[0]["StatusWF"].ToString();

                        if (hstatuswf.Value == "INP" || hstatuswf.Value == "INT" || hstatuswf.Value == "RJC" || hstatuswf.Value == "COMPLT")
                        {
                            btnsave.Enabled = false;
                            btnsubmit.Enabled = false;
                        }
                        else
                        {
                            btnsave.Enabled = true;
                            btnsubmit.Enabled = true;
                        }
                    }

                    DataTable dtad = new DataTable();
                    bll.vGetPAEXTDbyid(ref dtad, arr);

                    if (dtad.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtad.Rows.Count; i++)
                        {
                            if (dtad.Rows[i]["col_no"].ToString() == "1")
                            {
                                txtdocno.Text = dtad.Rows[i]["DocNo"].ToString();
                                //txtvendorcur.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                ddlcurbayar.SelectedValue = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt.Text = string.Format("{0:n}", Convert.ToDecimal(dtad.Rows[i]["VendorAmt"].ToString()));
                                ddlpmttyp.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt.Text = string.Format("{0:n}", Convert.ToDecimal(dtad.Rows[i]["PaymentAmt"].ToString()));
                                txtcostcenter.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks.Text = dtad.Rows[i]["Remarks"].ToString();
                            }
                            
                            if (dtad.Rows[i]["col_no"].ToString() == "2")
                            {
                                txtdocno2.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar2.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt2.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp2.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt2.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter2.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks2.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "3")
                            {
                                txtdocno3.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar3.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt3.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp3.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt3.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter3.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks3.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "4")
                            {
                                txtdocno4.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar4.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt4.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp4.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt4.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter4.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks4.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "5")
                            {
                                txtdocno5.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar5.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt5.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp5.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt5.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter5.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks5.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "6")
                            {
                                txtdocno6.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar6.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt6.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp6.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt6.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter6.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks6.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "7")
                            {
                                txtdocno7.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar7.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt7.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp7.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt7.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter7.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks7.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "8")
                            {
                                txtdocno8.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar8.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt8.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp8.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt8.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter8.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks8.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "9")
                            {
                                txtdocno9.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar9.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt9.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp9.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt9.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter9.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks9.Text = dtad.Rows[i]["Remarks"].ToString();
                            }

                            if (dtad.Rows[i]["col_no"].ToString() == "10")
                            {
                                txtdocno10.Text = dtad.Rows[i]["DocNo"].ToString();
                                ddlcurbayar10.Text = dtad.Rows[i]["VendorCurr"].ToString();
                                txtvendoramt10.Text = dtad.Rows[i]["VendorAmt"].ToString();
                                ddlpmttyp10.SelectedValue = dtad.Rows[i]["PaymentTyp"].ToString();
                                txtpmtamt10.Text = dtad.Rows[i]["PaymentAmt"].ToString();
                                txtcostcenter10.Text = dtad.Rows[i]["coa"].ToString();
                                txtremarks10.Text = dtad.Rows[i]["Remarks"].ToString();
                            }
                        }
                    }

                    DataTable dtadoc = new DataTable();
                    bll.vGetPAEXTDocbyid(ref dtadoc, arr);

                    if (dtadoc.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtadoc.Rows.Count; i++)
                        {
                            if (dtadoc.Rows[i]["col_no"].ToString() == "1")
                            {
                                txtdocname.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "2")
                            {
                                txtdocname2.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl2.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks2.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "3")
                            {
                                txtdocname3.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl3.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks3.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "4")
                            {
                                txtdocname4.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl4.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks4.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "5")
                            {
                                txtdocname5.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl5.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks5.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "6")
                            {
                                txtdocname6.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl6.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks6.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "7")
                            {
                                txtdocname7.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl7.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks7.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "8")
                            {
                                txtdocname8.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl8.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks8.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "9")
                            {
                                txtdocname9.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl9.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks9.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }

                            if (dtadoc.Rows[i]["col_no"].ToString() == "10")
                            {
                                txtdocname10.Text = dtadoc.Rows[i]["DocName"].ToString();
                                txtdocurl10.Text = dtadoc.Rows[i]["DocUrl"].ToString();
                                docremarks10.Text = dtadoc.Rows[i]["Remarks"].ToString();
                            }
                        }
                    }
                    List<cArrayList> arr2 = new List<cArrayList>();
                    arr2.Add(new cArrayList("@DOC", Session["pa_nox"].ToString()));
                    DataTable dta = new DataTable();
                    bll.vGetPAExtHist(ref dta, arr2);

                    grdpahist.DataSource = dta;
                    grdpahist.DataBind();

                    Session["bListPax"] = false;
                }
            }

           
        }

        class MyClient : WebClient
        {
            public bool HeadOnly { get; set; }
            protected override WebRequest GetWebRequest(Uri address)
            {
                WebRequest req = base.GetWebRequest(address);
                if (HeadOnly && req.Method == "GET")
                {
                    req.Method = "HEAD";
                }
                return req;
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string val = "";
            string s1 = "";
            string no = "";

            if (ddlpmttyp.SelectedValue == "Partial" && txtvendoramt.Text != "" && txtpmtamt.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt.Text) >= Convert.ToDecimal(txtvendoramt.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 1 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp.SelectedValue == "Full" && txtvendoramt.Text != "" && txtpmtamt.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt.Text) < Convert.ToDecimal(txtvendoramt.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 1 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp2.SelectedValue == "Partial" && txtvendoramt2.Text != "" && txtpmtamt2.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt2.Text) >= Convert.ToDecimal(txtvendoramt2.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 2 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp2.SelectedValue == "Full" && txtvendoramt2.Text != "" && txtpmtamt2.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt2.Text) < Convert.ToDecimal(txtvendoramt2.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 2 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp3.SelectedValue == "Partial" && txtvendoramt2.Text != "" && txtpmtamt2.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt3.Text) >= Convert.ToDecimal(txtvendoramt3.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 3 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp3.SelectedValue == "Full" && txtvendoramt2.Text != "" && txtpmtamt2.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt3.Text) < Convert.ToDecimal(txtvendoramt3.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 3 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp4.SelectedValue == "Partial" && txtvendoramt4.Text != "" && txtpmtamt4.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt4.Text) >= Convert.ToDecimal(txtvendoramt4.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 4 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp4.SelectedValue == "Full" && txtvendoramt4.Text != "" && txtpmtamt4.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt4.Text) < Convert.ToDecimal(txtvendoramt4.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 4 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp5.SelectedValue == "Partial" && txtvendoramt5.Text != "" && txtpmtamt5.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt5.Text) >= Convert.ToDecimal(txtvendoramt5.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 5 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp5.SelectedValue == "Full" && txtvendoramt5.Text != "" && txtpmtamt5.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt5.Text) < Convert.ToDecimal(txtvendoramt5.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 5 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp6.SelectedValue == "Partial" && txtvendoramt6.Text != "" && txtpmtamt.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt6.Text) >= Convert.ToDecimal(txtvendoramt6.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 6 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp6.SelectedValue == "Full" && txtvendoramt6.Text != "" && txtpmtamt.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt6.Text) < Convert.ToDecimal(txtvendoramt6.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 6 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp7.SelectedValue == "Partial" && txtvendoramt7.Text != "" && txtpmtamt7.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt7.Text) >= Convert.ToDecimal(txtvendoramt7.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 7 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp7.SelectedValue == "Full" && txtvendoramt7.Text != "" && txtpmtamt7.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt7.Text) < Convert.ToDecimal(txtvendoramt7.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 7 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp8.SelectedValue == "Partial" && txtvendoramt8.Text != "" && txtpmtamt8.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt8.Text) >= Convert.ToDecimal(txtvendoramt8.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 8 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp8.SelectedValue == "Full" && txtvendoramt8.Text != "" && txtpmtamt8.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt8.Text) < Convert.ToDecimal(txtvendoramt8.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 8 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp9.SelectedValue == "Partial" && txtvendoramt9.Text != "" && txtpmtamt9.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt9.Text) >= Convert.ToDecimal(txtvendoramt9.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 9 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp9.SelectedValue == "Full" && txtvendoramt9.Text != "" && txtpmtamt9.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt9.Text) < Convert.ToDecimal(txtvendoramt9.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 9 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }

            if (ddlpmttyp10.SelectedValue == "Partial" && txtvendoramt10.Text != "" && txtpmtamt10.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt10.Text) >= Convert.ToDecimal(txtvendoramt10.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 10 partial payment amount cannot same with vendor amount'); </script>";
                    return;
                }
            }
            else if (ddlpmttyp10.SelectedValue == "Full" && txtvendoramt10.Text != "" && txtpmtamt10.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt10.Text) < Convert.ToDecimal(txtvendoramt10.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 10 full payment amount must same with vendor amount'); </script>";
                    return;
                }
            }
            
            using (var client = new MyClient())
            {
                if (txtpmtamt.Text == "" && txtpmtamt2.Text == "" && txtpmtamt3.Text == "" && txtpmtamt4.Text == "" && txtpmtamt5.Text == "" && txtpmtamt6.Text == "" && txtpmtamt7.Text == "" && txtpmtamt8.Text == "" && txtpmtamt9.Text == "" && txtpmtamt10.Text == "")
                {
                    msg = "<script type='text/javascript'> alert('payment cannot 0'); </script>";
                    return;
                }

                if (txtdocurl.Text == "" && txtdocurl2.Text == "" && txtdocurl3.Text == "" && txtdocurl4.Text == "" && txtdocurl5.Text == "" && txtdocurl6.Text == "" && txtdocurl7.Text == "" && txtdocurl8.Text == "" && txtdocurl9.Text == "" && txtdocurl10.Text == "")
                {
                    msg = "<script type='text/javascript'> alert('document url can not empty'); </script>";
                    return;
                }
                
                client.HeadOnly = true;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                try
                {
                    if (txtdocurl.Text != "")
                    {
                        no = "1";
                        s1 = client.DownloadString(txtdocurl.Text);
                    }

                    if (txtdocurl2.Text != "")
                    {
                        no = "2";
                        s1 = client.DownloadString(txtdocurl2.Text);
                    }

                    if (txtdocurl3.Text != "")
                    {
                        no = "3";
                        s1 = client.DownloadString(txtdocurl3.Text);
                    }

                    if (txtdocurl4.Text != "")
                    {
                        no = "4";
                        s1 = client.DownloadString(txtdocurl4.Text);
                    }

                    if (txtdocurl5.Text != "")
                    {
                        no = "5";
                        s1 = client.DownloadString(txtdocurl5.Text);
                    }

                    if (txtdocurl6.Text != "")
                    {
                        no = "6";
                        s1 = client.DownloadString(txtdocurl6.Text);
                    }

                    if (txtdocurl7.Text != "")
                    {
                        no = "7";
                        s1 = client.DownloadString(txtdocurl7.Text);
                    }

                    if (txtdocurl8.Text != "")
                    {
                        no = "8";
                        s1 = client.DownloadString(txtdocurl8.Text);
                    }

                    if (txtdocurl9.Text != "")
                    {
                        no = "9";
                        s1 = client.DownloadString(txtdocurl9.Text);
                    }

                    if (txtdocurl10.Text != "")
                    {
                        no = "10";
                        s1 = client.DownloadString(txtdocurl10.Text);
                    }

                }
                catch (Exception ex)
                {
                    if (ex.ToString().Contains("Could not find file"))
                    {
                        msg = "<script type='text/javascript'> alert('no. " + no + " url not exist Could not find file'); </script>";
                        return;
                    }
                }
            }

            txtvendoramt.Text = txtvendoramt.Text.Replace(",", "");
            txtvendoramt2.Text = txtvendoramt2.Text.Replace(",", "");
            txtvendoramt3.Text = txtvendoramt3.Text.Replace(",", "");
            txtvendoramt4.Text = txtvendoramt4.Text.Replace(",", "");
            txtvendoramt5.Text = txtvendoramt5.Text.Replace(",", "");
            txtvendoramt6.Text = txtvendoramt6.Text.Replace(",", "");
            txtvendoramt7.Text = txtvendoramt7.Text.Replace(",", "");
            txtvendoramt8.Text = txtvendoramt8.Text.Replace(",", "");
            txtvendoramt9.Text = txtvendoramt9.Text.Replace(",", "");
            txtvendoramt10.Text = txtvendoramt10.Text.Replace(",", "");

            txtpmtamt.Text = txtpmtamt.Text.Replace(",", "");
            txtpmtamt2.Text = txtpmtamt2.Text.Replace(",", "");
            txtpmtamt3.Text = txtpmtamt3.Text.Replace(",", "");
            txtpmtamt4.Text = txtpmtamt4.Text.Replace(",", "");
            txtpmtamt5.Text = txtpmtamt5.Text.Replace(",", "");
            txtpmtamt6.Text = txtpmtamt6.Text.Replace(",", "");
            txtpmtamt7.Text = txtpmtamt7.Text.Replace(",", "");
            txtpmtamt8.Text = txtpmtamt8.Text.Replace(",", "");
            txtpmtamt9.Text = txtpmtamt9.Text.Replace(",", "");
            txtpmtamt10.Text = txtpmtamt10.Text.Replace(",", "");

            if (txtsts.Text.Trim() == "")
            {
                txtsts.Text = "draft";

                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@pa_ext_no", txtpano.Text));
                arr.Add(new cArrayList("@dt", txtdtpa.Text));
                arr.Add(new cArrayList("@loc_id", ddlsite.SelectedValue));
                arr.Add(new cArrayList("@kddepar", ddldpt.SelectedValue));
                arr.Add(new cArrayList("@nikrequestor", hnikpemohon.Value ));
                arr.Add(new cArrayList("@nama", lblpemohon.Text));
                arr.Add(new cArrayList("@vendorid", hvendorid.Value));
                arr.Add(new cArrayList("@vendorname", txtvendor.Text));
                arr.Add(new cArrayList("@request_sts", txtsts.Text));
                arr.Add(new cArrayList("@request_type", ""));
                arr.Add(new cArrayList("@HRemarks", txtremarksh.Text));

                bll.vInsPAEXT(arr, ref val);

                txtpano.Text = val;
                

                //detail
                if (txtdocno.Text != "" || txtdocno.Text != null)
                {
                    if (txtvendoramt.Text == "")
                    {
                        msg = "Vendor Amount(row 1) Cannot empty";
                        return;
                    }

                    if (txtpmtamt.Text == "")
                    {
                        msg = "Payment Amount(row 1) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno.Text));
                    //arr.Add(new cArrayList("@VendorCurr", txtvendorcur.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar.SelectedValue));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks.Text));
                    arr.Add(new cArrayList("@col_no", "1"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno2.Text != "")
                {
                    if (txtvendoramt2.Text == "")
                    {
                        msg = "Vendor Amount(row 2) Cannot empty";
                        return;
                    }

                    if (txtpmtamt2.Text == "")
                    {
                        msg = "Payment Amount(row 2) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype2.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno2.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar2.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt2.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp2.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt2.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter2.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks2.Text));
                    arr.Add(new cArrayList("@col_no", "2"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno3.Text != "")
                {
                    if (txtvendoramt3.Text == "")
                    {
                        msg = "Vendor Amount(row 3) Cannot empty";
                        return;
                    }

                    if (txtpmtamt3.Text == "")
                    {
                        msg = "Payment Amount(row 3) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype3.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno3.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar3.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt3.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp3.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt3.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter3.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks3.Text));
                    arr.Add(new cArrayList("@col_no", "3"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno4.Text != "")
                {
                    if (txtvendoramt4.Text == "")
                    {
                        msg = "Vendor Amount(row 4) Cannot empty";
                        return;
                    }

                    if (txtpmtamt4.Text == "")
                    {
                        msg = "Payment Amount(row 4) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype4.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno4.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar4.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt4.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp4.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt4.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter4.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks4.Text));
                    arr.Add(new cArrayList("@col_no", "4"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno5.Text != "")
                {
                    if (txtvendoramt5.Text == "")
                    {
                        msg = "Vendor Amount(row 5) Cannot empty";
                        return;
                    }

                    if (txtpmtamt5.Text == "")
                    {
                        msg = "Payment Amount(row 5) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype5.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno5.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar5.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt5.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp5.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt5.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter5.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks5.Text));
                    arr.Add(new cArrayList("@col_no", "5"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno6.Text != "")
                {
                    if (txtvendoramt6.Text == "")
                    {
                        msg = "Vendor Amount(row 6) Cannot empty";
                        return;
                    }

                    if (txtpmtamt6.Text == "")
                    {
                        msg = "Payment Amount(row 6) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype6.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno6.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar6.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt6.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp6.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt6.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter6.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks6.Text));
                    arr.Add(new cArrayList("@col_no", "6"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno7.Text != "")
                {
                    if (txtvendoramt7.Text == "")
                    {
                        msg = "Vendor Amount(row 7) Cannot empty";
                        return;
                    }

                    if (txtpmtamt7.Text == "")
                    {
                        msg = "Payment Amount(row 7) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype7.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno7.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar7.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt7.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp7.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt7.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter7.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks7.Text));
                    arr.Add(new cArrayList("@col_no", "7"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno8.Text != "")
                {
                    if (txtvendoramt8.Text == "")
                    {
                        msg = "Vendor Amount(row 8) Cannot empty";
                        return;
                    }

                    if (txtpmtamt8.Text == "")
                    {
                        msg = "Payment Amount(row 8) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype8.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno8.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar8.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt8.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp8.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt8.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter8.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks8.Text));
                    arr.Add(new cArrayList("@col_no", "8"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno9.Text != "")
                {
                    if (txtvendoramt9.Text == "")
                    {
                        msg = "Vendor Amount(row 9) Cannot empty";
                        return;
                    }

                    if (txtpmtamt9.Text == "")
                    {
                        msg = "Payment Amount(row 9) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype9.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno9.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar9.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt9.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp9.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt9.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter9.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks9.Text));
                    arr.Add(new cArrayList("@col_no", "9"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno10.Text != "")
                {
                    if (txtvendoramt10.Text == "")
                    {
                        msg = "Vendor Amount(row 10) Cannot empty";
                        return;
                    }

                    if (txtpmtamt10.Text == "")
                    {
                        msg = "Payment Amount(row 10) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype10.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno10.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar10.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt10.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp10.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt10.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter10.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks10.Text));
                    arr.Add(new cArrayList("@col_no", "10"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                //end detail

                //doc
                if (txtdocname.Text != "" || txtdocurl.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks.Text));
                    arr.Add(new cArrayList("@col_no", "1"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname2.Text != "" || txtdocurl2.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname2.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl2.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks2.Text));
                    arr.Add(new cArrayList("@col_no", "2"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname3.Text != "" || txtdocurl3.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname3.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl3.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks3.Text));
                    arr.Add(new cArrayList("@col_no", "3"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname4.Text != "" || txtdocurl4.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname4.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl4.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks4.Text));
                    arr.Add(new cArrayList("@col_no", "4"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname5.Text != "" || txtdocurl5.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname5.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl5.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks5.Text));
                    arr.Add(new cArrayList("@col_no", "5"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname6.Text != "" || txtdocurl6.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname6.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl6.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks6.Text));
                    arr.Add(new cArrayList("@col_no", "6"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname7.Text != "" || txtdocurl7.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname7.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl7.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks7.Text));
                    arr.Add(new cArrayList("@col_no", "1"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname8.Text != "" || txtdocurl8.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname8.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl8.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks8.Text));
                    arr.Add(new cArrayList("@col_no", "8"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname9.Text != "" || txtdocurl9.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname9.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl9.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks9.Text));
                    arr.Add(new cArrayList("@col_no", "9"));
                    bll.vInsPAEXTDoc(arr);
                }

                if (txtdocname10.Text != "" || txtdocurl10.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname10.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl10.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks10.Text));
                    arr.Add(new cArrayList("@col_no", "10"));
                    bll.vInsPAEXTDoc(arr);
                }
                //end doc
                msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                btnsubmit.Enabled = true;
            }
            else
            {
                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@pa_ext_no", txtpano.Text));
                arr.Add(new cArrayList("@dt", txtdtpa.Text));
                arr.Add(new cArrayList("@loc_id", ddlsite.SelectedValue));
                arr.Add(new cArrayList("@kddepar", ddldpt.SelectedValue));
                arr.Add(new cArrayList("@nikrequestor", hnikpemohon.Value));
                arr.Add(new cArrayList("@nama", lblpemohon.Text));
                arr.Add(new cArrayList("@vendorid", hvendorid.Value));
                arr.Add(new cArrayList("@vendorname", txtvendor.Text));
                arr.Add(new cArrayList("@request_sts", txtsts.Text));
                arr.Add(new cArrayList("@request_type", ""));
                arr.Add(new cArrayList("@HRemarks", txtremarksh.Text));

                bll.vInsPAEXT(arr, ref val);

                //edit detail

                if (txtdocno.Text != "")
                {
                    if (txtvendoramt.Text == "")
                    {
                        msg = "Vendor Amount(row 1) Cannot empty";
                        return;
                    }

                    if (txtpmtamt.Text == "")
                    {
                        msg = "Payment Amount(row 1) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar.SelectedValue));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks.Text));
                    arr.Add(new cArrayList("@col_no", "1"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno2.Text != "")
                {
                    if (txtvendoramt2.Text == "")
                    {
                        msg = "Vendor Amount(row 2) Cannot empty";
                        return;
                    }

                    if (txtpmtamt2.Text == "")
                    {
                        msg = "Payment Amount(row 2) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype2.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno2.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar2.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt2.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp2.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt2.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter2.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks2.Text));
                    arr.Add(new cArrayList("@col_no", "2"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno3.Text != "")
                {
                    if (txtvendoramt3.Text == "")
                    {
                        msg = "Vendor Amount(row 3) Cannot empty";
                        return;
                    }

                    if (txtpmtamt3.Text == "")
                    {
                        msg = "Payment Amount(row 3) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype3.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno3.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar3.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt3.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp3.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt3.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter3.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks3.Text));
                    arr.Add(new cArrayList("@col_no", "3"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno4.Text != "")
                {
                    if (txtvendoramt4.Text == "")
                    {
                        msg = "Vendor Amount(row 4) Cannot empty";
                        return;
                    }

                    if (txtpmtamt4.Text == "")
                    {
                        msg = "Payment Amount(row 4) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype4.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno4.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar4.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt4.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp4.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt4.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter4.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks4.Text));
                    arr.Add(new cArrayList("@col_no", "4"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno5.Text != "")
                {
                    if (txtvendoramt5.Text == "")
                    {
                        msg = "Vendor Amount(row 5) Cannot empty";
                        return;
                    }

                    if (txtpmtamt5.Text == "")
                    {
                        msg = "Payment Amount(row 5) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype5.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno5.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar5.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt5.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp5.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt5.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter5.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks5.Text));
                    arr.Add(new cArrayList("@col_no", "5"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno6.Text != "")
                {
                    if (txtvendoramt6.Text == "")
                    {
                        msg = "Vendor Amount(row 6) Cannot empty";
                        return;
                    }

                    if (txtpmtamt6.Text == "")
                    {
                        msg = "Payment Amount(row 6) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype6.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno6.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar6.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt6.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp6.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt6.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter6.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks6.Text));
                    arr.Add(new cArrayList("@col_no", "6"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno7.Text != "")
                {
                    if (txtvendoramt7.Text == "")
                    {
                        msg = "Vendor Amount(row 7) Cannot empty";
                        return;
                    }

                    if (txtpmtamt7.Text == "")
                    {
                        msg = "Payment Amount(row 7) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype7.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno7.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar7.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt7.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp7.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt7.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter7.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks7.Text));
                    arr.Add(new cArrayList("@col_no", "7"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vInsPAEXTD(arr);
                }

                if (txtdocno8.Text != "")
                {
                    if (txtvendoramt8.Text == "")
                    {
                        msg = "Vendor Amount(row 8) Cannot empty";
                        return;
                    }

                    if (txtpmtamt8.Text == "")
                    {
                        msg = "Payment Amount(row 8) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype8.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno8.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar8.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt8.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp8.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt8.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter8.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks8.Text));
                    arr.Add(new cArrayList("@col_no", "8"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno9.Text != "")
                {
                    if (txtvendoramt9.Text == "")
                    {
                        msg = "Vendor Amount(row 9) Cannot empty";
                        return;
                    }

                    if (txtpmtamt9.Text == "")
                    {
                        msg = "Payment Amount(row 9) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype9.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno9.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar9.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt9.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp9.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt9.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter9.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks9.Text));
                    arr.Add(new cArrayList("@col_no", "9"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }

                if (txtdocno10.Text != "")
                {
                    if (txtvendoramt10.Text == "")
                    {
                        msg = "Vendor Amount(row 10) Cannot empty";
                        return;
                    }

                    if (txtpmtamt10.Text == "")
                    {
                        msg = "Payment Amount(row 10) Cannot empty";
                        return;
                    }

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@Doctype", txtdoctype10.Text));
                    arr.Add(new cArrayList("@DocNo", txtdocno10.Text));
                    arr.Add(new cArrayList("@VendorCurr", ddlcurbayar10.Text));
                    arr.Add(new cArrayList("@VendorAmt", txtvendoramt10.Text));
                    arr.Add(new cArrayList("@PaymentTyp", ddlpmttyp10.SelectedValue));
                    arr.Add(new cArrayList("@PaymentAmt", txtpmtamt10.Text));
                    arr.Add(new cArrayList("@coa", txtcostcenter10.Text));
                    arr.Add(new cArrayList("@Remarks", txtremarks10.Text));
                    arr.Add(new cArrayList("@col_no", "10"));
                    arr.Add(new cArrayList("@kdsite", ddlsite.SelectedValue));

                    bll.vUpdPAEXTD(arr);
                }
                //end detail

                //doc

                if (txtdocname.Text != "" || txtdocurl.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks.Text));
                    arr.Add(new cArrayList("@col_no", "1"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname2.Text != "" || txtdocurl2.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname2.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl2.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks2.Text));
                    arr.Add(new cArrayList("@col_no", "2"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname3.Text != "" || txtdocurl3.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname3.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl3.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks3.Text));
                    arr.Add(new cArrayList("@col_no", "3"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname4.Text != "" || txtdocurl4.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname4.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl4.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks4.Text));
                    arr.Add(new cArrayList("@col_no", "4"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname5.Text != "" || txtdocurl5.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname5.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl5.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks5.Text));
                    arr.Add(new cArrayList("@col_no", "5"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname6.Text != "" || txtdocurl6.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname6.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl6.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks6.Text));
                    arr.Add(new cArrayList("@col_no", "6"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname7.Text != "" || txtdocurl7.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname7.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl7.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks7.Text));
                    arr.Add(new cArrayList("@col_no", "1"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname8.Text != "" || txtdocurl8.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname8.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl8.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks8.Text));
                    arr.Add(new cArrayList("@col_no", "8"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname9.Text != "" || txtdocurl9.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname9.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl9.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks9.Text));
                    arr.Add(new cArrayList("@col_no", "9"));
                    bll.vUpdPAEXTDoc(arr);
                }

                if (txtdocname10.Text != "" || txtdocurl10.Text != "")
                {
                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_ext_no", val));
                    arr.Add(new cArrayList("@DocName", txtdocname10.Text));
                    arr.Add(new cArrayList("@DocUrl", txtdocurl10.Text));
                    arr.Add(new cArrayList("@Remarks", docremarks10.Text));
                    arr.Add(new cArrayList("@col_no", "10"));
                    bll.vUpdPAEXTDoc(arr);
                }
                //end doc

                msg = "<script type='text/javascript'> alert('Data has been successfully updated'); </script>";
                btnsubmit.Enabled = true;
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtvendor.Text == "")
            {
                msg = "<script type='text/javascript'> alert('vendor cannot empty'); </script>";
                btnsubmit.Enabled = false;
                return;
            }

            if (txtpmtamt.Text == "" && txtpmtamt2.Text == "" && txtpmtamt3.Text == "" && txtpmtamt4.Text == "" && txtpmtamt5.Text == "" && txtpmtamt6.Text == "" && txtpmtamt7.Text == "" && txtpmtamt8.Text == "" && txtpmtamt9.Text == "" && txtpmtamt10.Text == "")
            {
                msg = "<script type='text/javascript'> alert('payment cannot 0'); </script>";
                btnsubmit.Enabled = false;
                return;
            }

            if (txtpmtamt.Text != "" && txtvendoramt.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt.Text) > Convert.ToDecimal(txtvendoramt.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 1 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt2.Text != "" && txtvendoramt2.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt2.Text) > Convert.ToDecimal(txtvendoramt2.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 2 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt3.Text != "" && txtvendoramt3.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt3.Text) > Convert.ToDecimal(txtvendoramt3.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 3 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt4.Text != "" && txtvendoramt4.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt4.Text) > Convert.ToDecimal(txtvendoramt4.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 4 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt5.Text != "" && txtvendoramt5.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt5.Text) > Convert.ToDecimal(txtvendoramt5.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 5 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt6.Text != "" && txtvendoramt6.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt6.Text) > Convert.ToDecimal(txtvendoramt6.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 6 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt7.Text != "" && txtvendoramt7.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt7.Text) > Convert.ToDecimal(txtvendoramt7.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 7 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt8.Text != "" && txtvendoramt8.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt8.Text) > Convert.ToDecimal(txtvendoramt8.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 8 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt9.Text != "" && txtvendoramt9.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt9.Text) > Convert.ToDecimal(txtvendoramt9.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 9 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            if (txtpmtamt10.Text != "" && txtvendoramt10.Text != "")
            {
                if (Convert.ToDecimal(txtpmtamt10.Text) > Convert.ToDecimal(txtvendoramt10.Text))
                {
                    msg = "<script type='text/javascript'> alert('no. 10 payment amount cannot bigger than vendor amount'); </script>";
                    btnsubmit.Enabled = false;
                    return;
                }
            }

            List<cArrayList> arr = new List<cArrayList>();
            arr = new List<cArrayList>();
            arr.Add(new cArrayList("@pa_ext_no", txtpano.Text));
            bll.vSubmitPAEXT(arr);

            msg = "<script type='text/javascript'> alert('Data has been successfully Submited'); </script>";
            btnsubmit.Enabled = false;
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            if (hstatuswf.Value == "" || hstatuswf.Value == null)
            {
                //Page.ClientScript.RegisterStartupScript(GetType(), "UserDialogScript", "return confirm(\"Insufficient storage\");", true);
                Response.Redirect("pa_ext_entry.aspx");
            }
            else
            {
                Response.Redirect("pa_ext_list.aspx");
            }
        }
    }
}