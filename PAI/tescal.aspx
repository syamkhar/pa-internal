﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tescal.aspx.cs" Inherits="PAI.tescal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>calendar setup</title>
    <script src="assets/calendar/jquery-1.3.2.min.js" type="text/javascript"></script>
        <script src="assets/calendar/jsm01.js" type="text/javascript"></script>

    <script src="assets/calendar/jscal2.js" type="text/javascript"></script>
        <script src="assets/calendar/lang/en.js" type="text/javascript"></script>
        <link href="assets/calendar/jscal2.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/border-radius.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/win2k/win2k.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>

    <asp:TextBox ID="txtdtperiod" runat="server" CssClass="form-control datepicker date datetimepicker"></asp:TextBox><button id="bdecdetdt1">...</button>
    </div>

    </form>

       <script type="text/javascript">
               //<![CDATA[

           var cal = Calendar.setup({
               onSelect: function (cal) { cal.hide() },
               showTime: false
           });

           cal.manageFields("bdecdetdt1", "txtdtperiod", "%m/%d/%Y");

           //]]
   </script>
</body>
</html>
