﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;

namespace rpa
{
    public partial class searchvendor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }

            LoadData(true);
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] is object)
                {
                    return Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public object LoadData(bool @bool)
        {
            string ssite = Request.QueryString["comp"].ToString();
            var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
            try
            {
                var dt = new DataTable();
                if (@bool)
                {

                    var myCMD = new SqlCommand("select vendorid,vendname from v_nintex_vendor where compid=@Nama and vendname like '%" + searchkey.Text + "%'", sqlConn);
                    var param = new SqlParameter();
                    param.ParameterName = "@Nama";
                    param.Value = ssite;
                    myCMD.Parameters.Add(param);
                    sqlConn.Open();
                    SqlDataReader reader = myCMD.ExecuteReader();
                    dt.Load(reader, LoadOption.OverwriteChanges);
                    sqlConn.Close();
                    ViewState["dt"] = dt;
                    PageNumber = 0;
                }

                var pgitems = new PagedDataSource();
                //var dv = new DataView(ViewState["dt"]);

                DataView dv = dt.DefaultView;
                ViewState.Add("key", dv.Table);

                pgitems.DataSource = dv;
                pgitems.AllowPaging = false;
                pgitems.PageSize = 1000;
                pgitems.CurrentPageIndex = PageNumber;
                if (pgitems.PageCount > 1)
                {
                    rptPages.Visible = true;
                    List<cArrayList> pages = new List<cArrayList>();
                    for (int i = 0, loopTo = pgitems.PageCount - 1; i <= loopTo; i++)
                        pages.Add(new cArrayList((i + 1).ToString(), (i + 1).ToString()));
                    rptPages.DataSource = pages;
                    rptPages.DataBind();
                }
                else
                {
                    rptPages.Visible = false;
                }

                rptResult.DataSource = pgitems;
                rptResult.DataBind();

                Session["NikObh"] = "";
            }
            catch (Exception ex)
            {
                //MsgBox[ex.Message];
            }
            finally
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
            }
            return @bool;

        }

        private void rptPages_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            LoadData(false);
        }


        protected void btnsearch_Click(object sender, EventArgs e)
        {

        }
    }

    
}