﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace rpa
{
    public partial class print_pa : System.Web.UI.Page
    {
        laporan.ReportLogic rptlogic = new laporan.ReportLogic();
        string sNik = "";
        string sNiksite = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            sNik = HttpContext.Current.Session["sNik"].ToString();
            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();
            if (IsPostBack == false)
            {
                if (Request.QueryString["cat"] == "0")
                {
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/laporan/rpt_payment_application_internal.rdlc");

                    DataTable table1 = rptlogic.GetPaymentApplicationUsage(sNik, Session["pa_no"].ToString(), Session["PaReasn"].ToString(), Session["PaAmt"].ToString(), Session["padt"].ToString(), sNiksite);
                    DataSet dspa = new DataSet("PaymentApplicationInternal");
                    dspa.Tables.Add(table1);

                    ReportDataSource datasource = new ReportDataSource("DataSetReport_PaymentApplicationInternal", dspa.Tables[0]);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(datasource);

                    DataTable table2 = rptlogic.approvelist(Session["pa_no"].ToString());
                    DataSet dspa2 = new DataSet("PaymentApplicationInternal_appv");
                    dspa2.Tables.Add(table2);

                    ReportDataSource datasource2 = new ReportDataSource("DataSetReport_PaymentApplicationInternal_appv", dspa2.Tables[0]);
                    ReportViewer1.LocalReport.DataSources.Add(datasource2);
                }
                else
                {
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/laporan/print_cellphone.rdlc");

                    DataTable table1 = rptlogic.GetPaymentAplicationCellphone(sNik, Session["pa_no"].ToString(), Session["PaAmt"].ToString(), Session["padt"].ToString(), sNiksite);
                    DataSet dspa = new DataSet("PaymentApplicationInternal");
                    dspa.Tables.Add(table1);

                    ReportDataSource datasource = new ReportDataSource("DataSetReport_PaymentApplicationCellphone", dspa.Tables[0]);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(datasource);

                    DataTable table2 = rptlogic.approvelist(Session["pa_no"].ToString());
                    DataSet dspa2 = new DataSet("PaymentApplicationInternal_appv");
                    dspa2.Tables.Add(table2);

                    ReportDataSource datasource2 = new ReportDataSource("DataSetReport_PaymentApplicationInternal_appv", dspa2.Tables[0]);
                    ReportViewer1.LocalReport.DataSources.Add(datasource2);
                }
            }
        }
    }
}