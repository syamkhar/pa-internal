﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rpa_entry.aspx.cs" Inherits="rpa.rpa_entry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PA ENTRY</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <script src="assets/calendar/jsm01.js" type="text/javascript"></script>
        <script src="assets/calendar/tcal.js" type="text/javascript"></script>
        <link href="assets/calendar/tcal.css" rel="stylesheet" type="text/css" />
        <script src="assets/calendar/jscal2.js" type="text/javascript"></script>
        <script src="assets/calendar/lang/en.js" type="text/javascript"></script>
        <link href="assets/calendar/jscal2.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/border-radius.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/win2k/win2k.css" rel="stylesheet" type="text/css" />
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white form-group">
    
    <form id="form1" runat="server" class="page-wrapper">
    <input type="hidden" id="ctrlToFind" />
        <div class="page-wrapper">
             <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <img src="assets/layouts/layout/img/logo.jpg" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->                           
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <%--<li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>--%>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <%--<form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>--%>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                                <%--<li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_2.html" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Dashboard 2</span>
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_3.html" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">Dashboard 3</span>
                                            <span class="badge badge-danger">5</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>--%>
                            <%--<li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li>--%>
                            
                            
                            <li class="nav-item  active open">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">RPA</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  active open">
                                        <a href="rpa_entry.aspx" class="nav-link ">
                                            <span class="title">Entry 
                                                <br/>Payment Aplication</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                
                                
                                    <li class="nav-item">
                                        <a href="rpa_pa_list.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Payment Aplication</span>   
                                        </a>
                                    </li>

                                    <%--<li class="nav-item">
                                        <a href="pa_ext_entry.aspx" class="nav-link ">
                                            <span class="title">Entry External 
                                                <br/>Payment Request</span>   
                                        </a>
                                    </li>--%>

                                    <%--<li class="nav-item">
                                        <a href="dec_ca.aspx" class="nav-link ">
                                            <span class="title">Declaration  
                                                <br/>Cash Advanse Non BTR</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="declarationlist.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Declaration Non BTR</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>--%>

                                    <li class="nav-item" >
                                        <a href="reports.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Reports</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            <%--<li class="heading">
                                <h3 class="uppercase">Layouts</h3>
                            </li>--%>                            
                           
                            <%--<li class="heading">
                                <h3 class="uppercase">Pages</h3>
                            </li>--%>
                            
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="rpa_entry.aspx">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Payment Application</span>
                                </li>
                                
                            </ul>
                            <%-- <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">
                                                <i class="icon-bell"></i> Action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-shield"></i> Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-user"></i> Something else here</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-bag"></i> Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>--%>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> FORM PA 
                            <small>Payment Application</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
                        <div class="row ">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-share font-dark"></i>
                                            <span class="caption-subject font-dark bold uppercase">PAYMENT APPLICATION FOR INTERNAL USAGE</span>
                                        </div>
                                        <%--<div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>--%>
                                        <asp:HiddenField ID="hkdlvl" runat="server" />
                                        <asp:HiddenField ID="hkddepar" runat="server" />
                                        <asp:HiddenField ID="hkkdjabat" runat="server" />
                                        <asp:HiddenField ID="hkddiv" runat="server" />
                                        <asp:HiddenField ID="hcaglacc" runat="server" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Atas Nama</label>
                                                <asp:DropDownList ID="ddlobehalf" runat="server" CssClass="form-control" 
                                                    onselectedindexchanged="ddlobehalf_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <span class="help-block">On Behalf</span>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Pemohon</label>
                                                <asp:Label ID="lblpemohon" runat="server" CssClass="form-control"></asp:Label>
                                                <span class="help-block">Requestor</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">No. Permohonan Pembayaran</label>
                                                <asp:TextBox ID="txtpano" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                <span class="help-block"> Payment Application No</span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Status</label>
                                                <asp:TextBox ID="txtsts" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Perusahaan</label>
                                                <asp:TextBox ID="txtcomp" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                <span class="help-block"> Company </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tanggal</label>
                                                <asp:TextBox ID="txtdtpa" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                <span class="help-block"> Date </span></div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Lokasi</label>
                                                <asp:TextBox ID="txtlokasi" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Divisi</label>
                                                 <asp:TextBox ID="txtdivisi" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Kepada</label>
                                                <asp:TextBox ID="txtkepada" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Kode Biaya</label>
                                                 <asp:TextBox ID="txtkdbiaya" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                     <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Untuk Kebutuhan</label>
                                                <div class="radio-list">
                                                   <%-- <label class="radio-inline">
                                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked=""> Reimburshment </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Cash Advance </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="optionsRadios" id="optionsRadios3" value="option2"> Cell Phone </label>--%>
                                                    <asp:RadioButtonList ID="rblpatyp" runat="server" 
                                                        onselectedindexchanged="rdlchange">
                                                        <asp:ListItem Text="Reimbursement" name="rdcat" Value="Reimburshment"></asp:ListItem>
                                                        <asp:ListItem Text="Cash Advance Operational" name="rdcat" Value="CashAdvance" style="display:none;"></asp:ListItem>
                                                        <asp:ListItem Text="Cash Advance BTR" name="rdcat" Value="CashAdvancebtr"></asp:ListItem>
                                                        <asp:ListItem Text="Cell Phone (Bagi yang Berhak)" name="rdcat" Value="CellPhone"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div id="cashadvance" <%=cssca %>>
                                                    <label class="control-label">BTR #</label><img alt="add" id="Img4" src="assets/pages/img/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopuptrip('paentry')" />
                                                    <asp:TextBox ID="txtbtrno" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>
                                                    
                                                </div>
                                                <label class="control-label">Currency Bayar</label>
                                                 <asp:DropDownList ID="ddlcurbayar" runat="server" CssClass="form-control"
                                                    onselectedindexchanged="ddlcurbayar_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <span class="help-block"> Currency to be Paid </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Jumlah(IDR)</label>
                                                <asp:TextBox ID="txttotalamt" runat="server" CssClass="form-control" placeholder="" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox>
                                                <span class="help-block"> Amount(IDR) </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nilai tukar dalam IDR</label>
                                                 <asp:TextBox ID="txtcurrraterp" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                <span class="help-block"> Exchange Rate to IDR </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <h3 class="form-section">*Detail Alokasi</h3>
                                    
                                    <div id="detailreimbursh">
                                        <hr />
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="no" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                        OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand"
                                        ShowFooter="True" OnRowDeleting="GridView1_RowDeleting" CssClass="table table-striped table-hover table-bordered" ShowHeader="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Kategori" >
                                               <EditItemTemplate>
                                               <div>
                                                   <asp:DropDownList ID="ddlcatlist" runat="server"></asp:DropDownList>
                                               </div> 
                                               </EditItemTemplate>
                                               <FooterTemplate>
                                               <div>
                                                   <asp:HiddenField ID="hnewperson" runat="server" />
                                                   <asp:DropDownList ID="ddlcatlistnew" runat="server"></asp:DropDownList>
                                               </div>
                                               </FooterTemplate>
                                               <ItemTemplate>
                                                   <asp:Label ID="Label3" runat="server" Text='<%# Bind("catagory") %>'></asp:Label>
                                                   
                                               </ItemTemplate>
                                            </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Amount(IDR)" >
                                                <EditItemTemplate>
                                                   <asp:textbox ID="txtbiayatot" runat="server"  Text='<%# Eval("amt") %>' Font-Size="1.0em" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:textbox>
                                               </EditItemTemplate>
                                               <FooterTemplate>
                                                   <asp:Label ID="lblnewbiaya" runat="server" BackColor="#FCFFA3">  </asp:Label>
                                                   <asp:TextBox ID="txtNewbiayatot" runat="server" Font-Size="1.0em" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox>
                                               </FooterTemplate>
                                               <ItemTemplate>
                                                   <asp:Label ID="Label26" runat="server" Text='<%# Bind("amt", "{0:#,##.00}") %>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Keterangan">
                                                <EditItemTemplate>
                                                    <asp:textbox ID="txtremarks" runat="server" Text='<%# Eval("remarks") %>' Font-Size="1.0em"></asp:textbox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                   <asp:TextBox ID="txtNewremarks" runat="server"  Font-Size="1.0em"></asp:TextBox>
                                               </FooterTemplate>
                                               <ItemTemplate>
                                                   <asp:Label ID="Label40" runat="server" Text='<%# Bind("remarks") %>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>
                                           
                                           <asp:TemplateField HeaderText="" ShowHeader="False" FooterStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-BorderStyle="None">
                                               <EditItemTemplate>
                                                <div style="width:85px;">
                                                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update">
                                                   </asp:LinkButton>
                                                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel">
                                                   </asp:LinkButton>
                                               </div>
                                               </EditItemTemplate>
                                               <FooterTemplate>
                                               <div style="width:30px;">
                                                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew" Text="+" CssClass="font-red font-hg"></asp:LinkButton>
                                                </div> 
                                               </FooterTemplate>
                                               <ItemTemplate>
                                                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                               </ItemTemplate>
                                           </asp:TemplateField>
                                           <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
                                       </Columns>
                                        
                                   </asp:GridView>
                                    </div>
                                    <div id="detailcellphone" <%=csscp %> >
                                        <asp:Table ID="tblcellphone" runat="server" CssClass="table table-responsive">
                                            <asp:TableRow>
                                                <asp:TableHeaderCell>Biling Period</asp:TableHeaderCell>
                                                <asp:TableHeaderCell></asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Billing Amount(IDR)</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Personal Use</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Claim Amount</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Keterangan</asp:TableHeaderCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:TextBox ID="txtdtperiod" runat="server" CssClass="form-control datepicker date datetimepicker"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><button id="bdecdetdt1" class="btn-circle">...</button></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtphonebillamt" runat="server" CssClass="form-control" placeholder="Cell phone billing Amount" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtphonepersonaluse" runat="server" CssClass="form-control" placeholder="Cell phone personal use" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtphoneclaimamount" runat="server" CssClass="form-control" placeholder="Cell phone claim" onkeypress="return onlyNumbers(event);" Enabled="false" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtphoneket" runat="server" CssClass="form-control" placeholder="remarks"></asp:TextBox></asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </div>
                                    <div id="detailca" <%=csscanew %>>
                                        <asp:Table ID="tblca" runat="server" CssClass="table table-responsive">
                                            <asp:TableRow>
                                                <asp:TableHeaderCell>Amount(IDR)</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Keterangan</asp:TableHeaderCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:TextBox ID="txtamountca" runat="server" CssClass="form-control" placeholder="amount" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarksca" runat="server" CssClass="form-control" placeholder="remarks"></asp:TextBox></asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </div>
                                    <h3 class="form-section">*Informasi Bank Requestor</h3>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nama Bank</label>
                                                <asp:TextBox ID="txtbanknm" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <span class="help-block"> Bank Name </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">No. Rekening</label>
                                                <asp:TextBox ID="txtaccno" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <span class="help-block"> Account No. </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Alamat Bank</label>
                                                <asp:TextBox ID="txtbankaddress" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <span class="help-block"> Bank Address </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nama Penerima</label>
                                                 <asp:TextBox ID="txtpenerima" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <span class="help-block"> Receipt Name </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Cabang</label>
                                                <asp:TextBox ID="txtcabang" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <span class="help-block"> Branch </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <h3 class="form-section">Document Supporting</h3>
                                    <hr />
                                    <div class="portlet-body">
                                        <div class="form-inline" role="form">
                                            <asp:FileUpload ID="fuploadPA" runat="server" /><br />
                                            
                                            <asp:GridView ID="gvfupld" runat="server" CssClass="table table-striped table-hover table-bordered" 
                                            OnRowDeleting="gvfupld_RowDeleting">
                                            <Columns>

                                            <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
                                            </Columns>
                                            </asp:GridView>

                                            
                                            <asp:Button ID="btnUpload" Text="Upload" runat="server" class="btn blue" OnClick="UploadFile" />
                                            <asp:Button ID="btnprint" class="btn red-flamingo green-dark-stripe" runat="server" Text="Print" Visible="false" OnClick="btnprint_Click" OnClientClick="target ='_blank';"/>
                                            <br />
                                            <asp:Label ID="lblMessage" class="sr-only" runat="server" />
                                        </div>
                                    </div>

                                    <h3 class="form-section">Finance Information</h3>
                                    
                                    <div class="form-group form-md-line-input">
                                                <label for="inputEmail1" class="col-md-2 control-label">Ammount Paid</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtpaidamt" runat="server" CssClass="form-control" placeholder="Amount Paid" Enabled="false"></asp:TextBox>
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
                                    <div id="pymtsts" class="form-group form-md-line-input">
                                                <label for="inputEmail1" class="col-md-2 control-label">Payment Type</label>
                                                <div class="col-md-4">
                                                    <div class="md-checkbox-inline">
                                                    <div class="md-checkbox">
                                                        <asp:CheckBox ID="chbcash" runat="server" class="md-check"/>
                                                        <label for="checkbox7">
                                                            <span></span>
                                                            <span class="box"></span>
                                                            <span class="box"></span> Cash </label>
                                                    </div>
                                                    <div class="md-checkbox">
                                                        <asp:CheckBox ID="chbtransfer" runat="server" />
                                                        <label for="checkbox7">
                                                            <span></span>
                                                            <span class="box"></span>
                                                            <span class="box"></span> Bank Transfer </label>
                                                    </div>
                                                </div>
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
                                    <div class="portlet-body">
                                        <hr />
                                        <div class="form-inline" role="form">
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputEmail2">Date</label>
                                            <asp:TextBox ID="txtdt" runat="server" class="form-control spinner" Visible="false" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">Amount</label>
                                                <asp:TextBox ID="txtamount" runat="server" Visible="false" class="form-control spinner" placeholder="Amount"></asp:TextBox>
                                            </div>
                                            <div class="form-group" style="vertical-align:top">
                                                <label class="sr-only" for="exampleInputPassword2">Description</label>
                                                <asp:TextBox ID="txtreason" runat="server" Visible="false" class="form-control spinner" placeholder="Description" TextMode="MultiLine" Rows="6" Columns="50"></asp:TextBox>
                                            </div>
                                            
                                            <asp:Button ID="btnsave" class="btn blue" runat="server" Text="Save" onclick="btnsave_Click" />
                                            <asp:Button ID="btnsubmit" name="btnsubmit" class="btn blue" runat="server" Text="Submit" 
                                                onclick="btnsubmit_Click" />
                                            <button class="btn btn-home" name="discard" id="discard" <%=cssdiscard %>>Discard Changes</button>
                                            <asp:Button ID="btnprintcp" class="btn yellow" runat="server" 
                                                Text="Print Cell Phone Form" onclick="btnprintcp_Click" OnClientClick="SetTarget();"/>
                                            <asp:Button ID="btnprintpa" class="btn yellow" runat="server" 
                                                Text="Print PA Form" onclick="btnprintpa_Click" OnClientClick="SetTarget();"/>
                                            <asp:HiddenField ID="hpano" runat="server" />
                                            <asp:HiddenField ID="hfsize" runat="server" />


                                        </div>
                        
                                    </div>
                                    
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->
                                
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <%=msg %>
                
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                        <%--<a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>--%>
                
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            
            </div>
            </div>
         </div>
    </form>
    <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })

        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('input[name="rblpatyp"]').change(function () {
                    if ($(this).val() == "CellPhone") {
                        $("#detailcellphone").show();
                        $("#detailreimbursh").hide();
                        $("#cashadvance").hide();
                        $("#detailca").hide();
                    }
                    if ($(this).val() == "Reimburshment") {
                        $("#detailcellphone").hide();
                        $("#detailreimbursh").show();
                        $("#cashadvance").hide();
                        $("#detailca").hide();
                    }
                    if ($(this).val() == "CashAdvance") {
                        $("#detailcellphone").hide();
                        $("#detailreimbursh").hide();
                        $("#cashadvance").hide();
                        $("#detailca").show();
                    }
                    if ($(this).val() == "CashAdvancebtr") {
                        $("#detailcellphone").hide();
                        $("#detailreimbursh").hide();
                        $("#cashadvance").show();
                        $("#detailca").show();
                    }
                });

                //Mask the textbox as per your format 123-123-123
                $('#txtTrlrGAaccount').mask('99-9999-999', { placeholder: "#" });
            });



            $('#discard').on('click', function (evt) {
                evt.preventDefault(); //don't submit the form, which a button naturally does
                if (confirm('Are you sure you want to discard changes?'))
                    location.href = 'rpa_entry.aspx'; //redirect only on confirm
            });
            function OpenPopuptrip(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("searchtrip.aspx", "List", "scrollbars=yes,resizable=yes,width=1000,height=400");
                return false;
            }
        </script>

        <script type="text/javascript">
            function leftTrim(sString) {
                while (sString.substring(0, 1) == ' ') {
                    sString = sString.substring(1, sString.length);
                }
                return sString;
            }


            function rightTrim(sString) {
                while (sString.substring(sString.length - 1, sString.length) == ' ') {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }
            function trimAll(sString) {
                while (sString.substring(0, 1) == ' ') {
                    sString = sString.substring(1, sString.length);
                }
                while (sString.substring(sString.length - 1, sString.length) == ' ') {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
                    return false;


                return true;


            }


            var patterns = new Array(
	        "###,###,###,###",  // US/British
	        "###.###.###.###",  // German
	        "### ### ### ###",  // French
	        "###'###'###'###",  // Swiss
	        "#,##,##,##,##,###",  // Indian
	        "####\u5104 ####\u4E07 ####",  // Japanese/Chinese
	        "############" // no formatting
	        );


            // A little function to take an integer string, strip out current formatting,
            // and reformat it according to a pattern string. '#' characters in the pattern
            // are substituted with digits from the integer string, other pattern characters
            // are output literally into the returned string.
            function formatInteger(integer, pattern) {
                var result = '';


                integerIndex = integer.length - 1;
                patternIndex = pattern.length - 1;


                while ((integerIndex >= 0) && (patternIndex >= 0)) {
                    var digit = integer.charAt(integerIndex);
                    integerIndex--;


                    // Skip non-digits from the source integer (eradicate current formatting).
                    if ((digit < '0') || (digit > '9')) continue;


                    // Got a digit from the integer, now plug it into the pattern.
                    while (patternIndex >= 0) {
                        var patternChar = pattern.charAt(patternIndex);
                        patternIndex--;


                        // Substitute digits for '#' chars, treat other chars literally.
                        if (digit == '.')
                            break;
                        else if (patternChar == '#') {
                            result = digit + result;
                            break;
                        }
                        else {
                            result = patternChar + result;
                        }
                    }
                }


                return result;
            }


            function appendDollar(id) {
                var amount = document.getElementById(id).value;
                var decimalval = amount.split(".")[1];
                if (decimalval == null) decimalval = ".00"
                else {
                    decimalval = "." + decimalval;
                    amount = amount.split(".")[0];
                }


//                if (trimAll(amount) != "") {
//                    document.getElementById(id).value = 'Rp' + formatInteger(amount, '###,###,###,###') + decimalval;
//                }

                if (trimAll(amount) != "") {
                    document.getElementById(id).value = formatInteger(amount, '###,###,###,###') + decimalval;
                }
            }
   </script>
   <script type="text/javascript">
               //<![CDATA[

       var cal = Calendar.setup({
           onSelect: function (cal) { cal.hide() },
           showTime: false
       });

       cal.manageFields("bdecdetdt1", "txtdtperiod", "%m/01/%Y");

       //]]
   </script>
    <script type="text/javascript">
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
    </script>
</body>

</html>
