﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;

namespace rpa
{
    public partial class searchpa : System.Web.UI.Page
    {
        cbll bll = new cbll();
        string sNik = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            sNik = HttpContext.Current.Session["sNiksite"].ToString();
            LoadData(true);
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] is object)
                {
                    return Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public object LoadData(bool @bool)
        {
            var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
            try
            {
                var dt = new DataTable();
                if (@bool)
                {

                    //var myCMD = new SqlCommand("select TripNo, A.purpose, A.nik, A.kddepar, A.kdjabat , A.kdsite, A.kdlokasi, A.costcode, A.sup_nik, (case A.fstatus when '1' then 'Proses' else 'Unproses' end) as fstatus, B.nama, (select nama from H_A101 where nik = A.sup_nik ) as superior, (select nmjabat from H_A150 where kdjabat = A.kdjabat ) as nmjabat, (select NmDepar from H_A130 where kddepar = A.kddepar) as nmdepar, (SELECT costName FROM H_A221 WHERE costcode = A.costcode) AS costname, (case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end) As Detailtrip from V_H001 A inner join H_A101 B on A.nik = B.nik where A.stedit <> '2' and A.fstatus = 1 and A.Tripno not in (select tripno from V_H002 where stedit <> '2') and A.nik like '%' + @Nama + '%' union select TripNo, A.purpose, A.nik, A.kddepar, A.kdjabat , A.kdsite, A.kdlokasi, A.costcode, A.sup_nik, (case A.fstatus when '1' then 'Proses' else 'Unproses' end) as fstatus, B.nama, (select nama from H_A101 where nik = A .sup_nik ) as superior, (select nmjabat from H_A150 where kdjabat = A.kdjabat ) as nmjabat, (select NmDepar from H_A130 where kddepar = A.kddepar) as nmdepar, (SELECT costName FROM H_A221 WHERE costcode = A.costcode) AS costname, (case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end) As Detailtrip from V_H001 A inner join H_A101 B on A.nik = B.nik , V_A004 C where A.stedit <> '2' and A.fstatus = 1 and A.Tripno not in (select tripno from V_H002 where stedit <> '2') and C.nika like '%' + @Nama + '%' and c.nikb in(a.nik)", sqlConn);
                    var myCMD = new SqlCommand("select a.pa_no TripNo, a.reason purpose, a.pa_to nik, b.kddepar, b.kdjabatan kdjabat, a.loc_id kdsite, b.pycostcode costcode,(select niksite from jrn_test..h_a101 where kdjabatan=(select kdjabatdirect from jrn_test..h_a150 where kdjabat=b.kdjabatan)) sup_nik,a.statusWF fstatus,b.nama,(select nama from jrn_test..h_a101 where kdjabatan = (select kdjabatdirect from jrn_test..h_a150 where kdjabat=b.kdjabatan)) as superior,(select nmjabat from jrn_test..H_A150 where kdjabat = b.kdjabatan ) as nmjabat,(select NmDepar from jrn_test..H_A130 where kddepar = b.kddepar) as nmdepar,(SELECT costName FROM jrn_test..H_A221 WHERE costcode = b.pycostcode) AS costname,'Operational' Detailtrip from rpa_pa a inner join jrn_test..h_a101 b on a.pa_to=b.niksite where a.usage_typ = 'CA' and (a.nobtr is null or a.nobtr = '') and a.pa_to=@Nama and a.amt_paid > 0 and b.active=1 and a.amount > 0 and pa_no not in (select pa_no from cash_adv_dec where nik=@Nama)", sqlConn); 

                    var param = new SqlParameter();
                    param.ParameterName = "@Nama";
                    param.Value = sNik;
                    myCMD.Parameters.Add(param);
                    sqlConn.Open();
                    SqlDataReader reader = myCMD.ExecuteReader();
                    dt.Load(reader, LoadOption.OverwriteChanges);
                    sqlConn.Close();
                    ViewState["dt"] = dt;
                    PageNumber = 0;
                }

                var pgitems = new PagedDataSource();
                //var dv = new DataView(ViewState["dt"]);

                DataView dv = dt.DefaultView;
                ViewState.Add("key", dv.Table);

                pgitems.DataSource = dv;
                pgitems.AllowPaging = true;
                pgitems.PageSize = 15;
                pgitems.CurrentPageIndex = PageNumber;
                if (pgitems.PageCount > 1)
                {
                    rptPages.Visible = true;
                    List<cArrayList> pages = new List<cArrayList>();
                    for (int i = 0, loopTo = pgitems.PageCount - 1; i <= loopTo; i++)
                        pages.Add(new cArrayList((i + 1).ToString(), (i + 1).ToString()));
                    rptPages.DataSource = pages;
                    rptPages.DataBind();
                }
                else
                {
                    rptPages.Visible = false;
                }

                rptResult.DataSource = pgitems;
                rptResult.DataBind();
            }
            catch (Exception ex)
            {
                //MsgBox[ex.Message];
            }
            finally
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
            }
            return @bool;

        }

        private void rptPages_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            LoadData(false);
        }
    }
}