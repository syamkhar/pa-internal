﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace rpa
{
    public class cArrayList
    {
        private string _paramid;
        private object _paramvalue;

        public string paramid
        { set { _paramid = value; } get { return (_paramid); } }

        public object paramvalue
        { set { _paramvalue = value; } get { return (_paramvalue); } }

        public cArrayList(string sParamID, object oParamvalue)
        {
            paramid = sParamID;
            paramvalue = oParamvalue;
        }
    }

    class cdal
    {
        public SqlDataReader vGetRecordsetSQL(string sSQL)
        {
            SqlDataReader rs = null;
            SqlCommand cmd = new SqlCommand(sSQL, cd.conn());
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            rs = cmd.ExecuteReader();
            cmd.Dispose();
            return (rs);
        }

        public SqlDataReader vGetRecordsetSP(string sSPName, List<cArrayList> carr)
        {
            SqlDataReader rs = null;
            SqlCommand cmd = new SqlCommand(sSPName, cd.conn());
            for (int i = 0; i < carr.Count; i++)
            {
                cmd.Parameters.AddWithValue(carr[i].paramid, carr[i].paramvalue);
            }
            cmd.CommandText = sSPName;
            cmd.CommandType = CommandType.StoredProcedure;
            rs = cmd.ExecuteReader();
            return (rs);
            // cmd.Dispose();
        }

        public SqlDataReader vGetRecordsetSP(string sSPName)
        {
            SqlDataReader rs = null;
            SqlCommand cmd = new SqlCommand(sSPName, cd.conn());
            cmd.CommandText = sSPName;
            cmd.CommandType = CommandType.StoredProcedure;
            rs = cmd.ExecuteReader();
            cmd.Dispose();
            return (rs);
        }

        public void vExecuteSp(string sSPName)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cd.conn();
            cmd.CommandText = sSPName;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }

        public void vExecuteSp(string sSPName, List<cArrayList> carr)
        {

            SqlCommand cmd = new SqlCommand(sSPName, cd.conn());
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < carr.Count(); i++)
            {
                cmd.Parameters.AddWithValue(carr[i].paramid, carr[i].paramvalue);
            }
            cmd.ExecuteNonQuery();
        }

        public void vExecuteScalarSp(string sSPName, List<cArrayList> carr, ref string value)
        {
            SqlDataReader rs = null;
            SqlDataAdapter sda = null;
            SqlCommand cmd = new SqlCommand(sSPName, cd.conn());
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < carr.Count(); i++)
            {
                cmd.Parameters.AddWithValue(carr[i].paramid, carr[i].paramvalue);
            }
            value = Convert.ToString(cmd.ExecuteScalar());
        }
    }
}