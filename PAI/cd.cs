﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace rpa
{
    public static class cd
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString;
        static SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

        static SqlConnection _conn;
        static string sUser = builder.UserID;
        static string sPwd = builder.Password;
        static string sDB = builder.InitialCatalog;
        static string sServer = builder.DataSource;
        //public static string sNik = "";
        //public static string sNiksite = "";
        public static string iPano = "";
        public static string sPaDt = "";
        public static string sPaAmt = "";
        public static string sPaReasn = "";
        public static Boolean bListPa = false;
        public static string coacellphone = "6-1-07-091";

        public static string sConnString()
        {
            return ("user id=" + sUser + ";password=" + sPwd + ";initial catalog=" + sDB + ";server=" + sServer + ";MultipleActiveResultsets=true");
        }

        public static SqlConnection conn()
        {
            if (_conn == null)
            {
                _conn = new SqlConnection();
                _conn.ConnectionString = sConnString(); 
                _conn.Open();
            }
            return (_conn);
        }
    }

}