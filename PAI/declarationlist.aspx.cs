﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;

namespace rpa
{
    public partial class declarationlist : System.Web.UI.Page
    {
        public string tglaw;
        public string th;
        public string v1;
        public string v2;
        public int imonth;
        cbll bll = new cbll();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            if (!IsPostBack)
            {
                tglaw = Request[ddlbulan.UniqueID];
                th = Request[ddltahun.UniqueID];
                int i;
                ddlbulan.Items.Clear();
                //ddlbulan.Items.Add("January");
                //ddlbulan.Items.Add("February");
                //ddlbulan.Items.Add("March");
                //ddlbulan.Items.Add("April");
                //ddlbulan.Items.Add("May");
                //ddlbulan.Items.Add("June");
                //ddlbulan.Items.Add("July");
                //ddlbulan.Items.Add("August");
                //ddlbulan.Items.Add("September");
                //ddlbulan.Items.Add("October");
                //ddlbulan.Items.Add("November");
                //ddlbulan.Items.Add("December");
                ddlbulan.Items.Add(new ListItem("January", "1"));
                ddlbulan.Items.Add(new ListItem("February", "2"));
                ddlbulan.Items.Add(new ListItem("March", "3"));
                ddlbulan.Items.Add(new ListItem("April", "4"));
                ddlbulan.Items.Add(new ListItem("Nay", "5"));
                ddlbulan.Items.Add(new ListItem("June", "6"));
                ddlbulan.Items.Add(new ListItem("July", "7"));
                ddlbulan.Items.Add(new ListItem("August", "8"));
                ddlbulan.Items.Add(new ListItem("September", "9"));
                ddlbulan.Items.Add(new ListItem("October", "10"));
                ddlbulan.Items.Add(new ListItem("November", "11"));
                ddlbulan.Items.Add(new ListItem("December", "12"));
                ddlbulan.SelectedValue = DateTime.Today.Month.ToString();

                var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_citrixcon"].ConnectionString);
                sqlConn.Open();
                string strcon = "select max(transdate) as max_date, min(transdate) as min_date from V_H002";
                var dtb = new DataTable();
                var sda = new SqlDataAdapter(strcon, sqlConn);
                sda.Fill(dtb);
                v1 = dtb.Rows[0]["min_date"].ToString();
                v1 = Convert.ToDateTime(v1).ToString("yyyy");
                v2 = dtb.Rows[0]["max_date"].ToString();
                v2 = Convert.ToDateTime(v2).ToString("yyyy");
                ddltahun.Items.Clear();
                var loopTo = v2;

                for (i = Convert.ToInt16(v1); i <= Convert.ToInt16(v2); i++)
                    ddltahun.Items.Add(i.ToString());
                ddltahun.SelectedValue = (i - 1).ToString();
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
            try
            {
                //sqlConn.Open();
                //var dtb = new DataTable();
                //string strcon = "select decno, pa_no, transdate, remarks, createdby, modifiedby, modifiedtime, stedit, fstatus, nik, currcode, check_sup, reqby, reqdate,(select nama from jrn_test..H_A101 where nik = CASH_ADV_DEC.nik) as nama ";
                //strcon = strcon + "from CASH_ADV_DEC where createdby = '" + Session["sNiksite"].ToString() + "' and month(transdate) = '" + ddlbulan.SelectedValue.ToString() + "' and year(transdate)='" + ddltahun.SelectedValue.ToString() +"'";
                //var sda = new SqlDataAdapter(strcon, sqlConn);
                //sda.Fill(dtb);
                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@niksite", Session["sNiksite"].ToString()));
                arr.Add(new cArrayList("@month", ddlbulan.SelectedValue.ToString()));
                arr.Add(new cArrayList("@year", ddltahun.SelectedValue.ToString()));
                DataTable dta = new DataTable();
                bll.vGetListDec(ref dta, arr);

                GridView1.DataSource = dta;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow row = GridView1.SelectedRow;
            Session["vdec"] = GridView1.DataKeys[row.RowIndex].Value.ToString();
            Response.Redirect("dec_ca.aspx?ID=" + GridView1.DataKeys[row.RowIndex].Value.ToString());
        }
    }

}