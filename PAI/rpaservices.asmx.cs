﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services.Protocols;

namespace rpa
{
    /// <summary>
    /// Summary description for rpaservices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class rpaservices : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(EnableSession = true)]
        public string Adddec(string _decno, string _site, string _costcode, string _tripno, string _kddepar, string _snik, string _nik, string _nmjabat, string _fstatus, string _nama, string _decdate, string _remark, string _reqby, string _reqbydt, string _apprby, string _apprdate, string _reviewby, string _reviewdate, string _proceedby, string _proceeddate, string _qry)
        {
                string decno="";
                string dtmnow;
                string dtynow;
                string stripno;
                int i;
                var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
                try
                {
                        conn.Open();
                        dtmnow = DateTime.Now.ToString("MM");
                        dtynow = DateTime.Now.Year.ToString();
                        stripno = dtynow + dtmnow + "/" + "PAI" + "/" + "D" + "/";
                        var dtb = new DataTable();
                        string strcon = "select Decno from CASH_ADV_DEC where Decno like '%" + stripno + "%'";
                        var sda = new SqlDataAdapter(strcon, conn);
                        sda.Fill(dtb);
                        if (dtb.Rows.Count == 0)
                        {
                            decno = stripno + "000001";
                        }
                        else if (dtb.Rows.Count > 0)
                        {
                            i = 0;
                            i = dtb.Rows.Count + 1;
                            if (dtb.Rows.Count > 0 & dtb.Rows.Count < 9)
                            {
                                decno = stripno + "00000" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 9 & dtb.Rows.Count < 99)
                            {
                                decno = stripno + "0000" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 99 & dtb.Rows.Count < 999)
                            {
                                decno = stripno + "000" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 999 & dtb.Rows.Count < 9999)
                            {
                                decno = stripno + "00" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 9999 & dtb.Rows.Count < 99999)
                            {
                                decno = stripno + "0" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 99999 & dtb.Rows.Count < 999999)
                            {
                                decno = stripno + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 999999)
                            {
                                decno = "Error on generate Tripnum";
                            }
                        }

                        conn.Close();

                        var conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
                        string sqlQuery;
                        string sqlquery2;
                        SqlCommand cmd;
                        SqlCommand cmd2;
                        string ins;
                        string ins2;
                        conn2.Open();

                        // sqlQuery = "insert into V_H002 (decno, tripno, transdate, remarks, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, createdby, modifiedby, modifiedtime, stedit, fstatus, nik, currcode, check_sup)"
                        sqlQuery = "insert into CASH_ADV_DEC (decno, pa_no, transdate, remarks, createdby, modifiedby, modifiedtime, stedit, fstatus, nik, currcode, check_sup, reqby, reqdate,kddepar)";
                        sqlQuery = sqlQuery + " Values ('" + decno + "', '" + _tripno + "', '" + _decdate + "', '" + _remark + "', '" + Session["sNik"].ToString() + "', '" + Session["sNik"].ToString() + "', '" + DateTime.Now + "', 0, 0, '" + _nik + "', 'IDR', '0', '" + _nik + "', '" + _decdate + "', '" + _kddepar + "')";
                        cmd = new SqlCommand(sqlQuery, conn2);
                        cmd.ExecuteScalar();
                        if (_qry == "null")
                        {
                        }
                        else
                        {
                            ins = _qry.Replace("+", " insert into CASH_ADV_DEC_D (genid,decno,decdate, expensetype, currency, subtotal, remarks) values ");
                            ins = ins.Replace("||", decno);
                            ins2 = "insert into CASH_ADV_DEC_D (genid,decno,decdate, expensetype, currency, subtotal, remarks) values " + ins;
                            sqlquery2 = ins2;
                            cmd2 = new SqlCommand(sqlquery2, conn2);
                            cmd2.ExecuteScalar();
                        }

                        Session["decno"] = decno;
                        conn2.Close();
                
                    }
                catch (Exception ex)
                {

                }
                _decno = Session["decno"].ToString();
                return _decno;
        }

        [WebMethod(EnableSession = true)]
        public string insertdec(string _decno, string _site)
        {
            string decno="";
                string dtmnow;
                string dtynow;
                string stripno;
                int i;
                var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
                try
                {
                        conn.Open();
                        dtmnow = DateTime.Now.ToString("MM");
                        dtynow = DateTime.Now.Year.ToString();
                        stripno = dtynow + dtmnow + "/" + "D" + "/";
                        var dtb = new DataTable();
                        string strcon = "select Decno from CASH_ADV_DEC where Decno like '%" + stripno + "%'";
                        var sda = new SqlDataAdapter(strcon, conn);
                        sda.Fill(dtb);
                        if (dtb.Rows.Count == 0)
                        {
                            decno = stripno + "000001";
                        }
                        else if (dtb.Rows.Count > 0)
                        {
                            i = 0;
                            i = dtb.Rows.Count + 1;
                            if (dtb.Rows.Count > 0 & dtb.Rows.Count < 9)
                            {
                                decno = stripno + "00000" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 9 & dtb.Rows.Count < 99)
                            {
                                decno = stripno + "0000" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 99 & dtb.Rows.Count < 999)
                            {
                                decno = stripno + "000" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 999 & dtb.Rows.Count < 9999)
                            {
                                decno = stripno + "00" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 9999 & dtb.Rows.Count < 99999)
                            {
                                decno = stripno + "0" + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 99999 & dtb.Rows.Count < 999999)
                            {
                                decno = stripno + i.ToString();
                            }
                            else if (dtb.Rows.Count >= 999999)
                            {
                                decno = "Error on generate Tripnum";
                            }
                        }
                        Session["decno"] = decno;
                        conn.Close();

                        var conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["jrn_rpa"].ConnectionString);
                        string sqlQuery;
                        string sqlquery2;
                        SqlCommand cmd;
                        SqlCommand cmd2;
                        string ins;
                        string ins2;
                        conn2.Open();

                        sqlQuery = "insert into CASH_ADV_DEC (DecNo, pa_no)";
                        sqlQuery = sqlQuery + " Values ('" + decno + "', '1')";
                        cmd = new SqlCommand(sqlQuery, conn2);
                        cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {

                }
                _decno = Session["decno"].ToString();
                return _decno;


        }
    }

}
