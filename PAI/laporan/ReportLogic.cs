﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net;
using System.IO;
using System.Web.UI.WebControls;

namespace rpa.laporan
{
    class ReportLogic
    {
        string sNiksite = "";
        string sNik = "";
        public DataTable GetPaymentApplicationUsage(string UserId, string PaymentNo, string PaymentType, string Amount, string DateCreated, string ToPerson)
        {
            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();
            sNik = HttpContext.Current.Session["sNik"].ToString();
            cbll bll = new cbll();
            DataTable dtPaymentUsage = new DataTable("PaymentApplicationInternal");

            dtPaymentUsage.Columns.Add("NikSite");
            dtPaymentUsage.Columns.Add("Nik");
            dtPaymentUsage.Columns.Add("Nama");
            dtPaymentUsage.Columns.Add("Position");
            dtPaymentUsage.Columns.Add("Division");
            dtPaymentUsage.Columns.Add("Site");
            dtPaymentUsage.Columns.Add("PaymentNo");
            dtPaymentUsage.Columns.Add("CostCenter");
            dtPaymentUsage.Columns.Add("Amount");
            dtPaymentUsage.Columns.Add("To");
            dtPaymentUsage.Columns.Add("Usage");
            dtPaymentUsage.Columns.Add("DateCreated");
            dtPaymentUsage.Columns.Add("DateRequest");
            dtPaymentUsage.Columns.Add("Company");
            dtPaymentUsage.Columns.Add("NameBank");
            dtPaymentUsage.Columns.Add("AddressBank");
            dtPaymentUsage.Columns.Add("BankBranch");
            dtPaymentUsage.Columns.Add("NoRekening");
            dtPaymentUsage.Columns.Add("NameRekening");

            List<cArrayList> arr = new List<cArrayList>();
            //sNiksite = "0001576";
            arr.Add(new cArrayList("@nik", sNik));
            DataTable dta = new DataTable();

            bll.vGetEmpAcc(ref dta, arr);

            if (dta.Rows.Count > 0)
            {
                System.Data.DataRow item = dta.Rows[0];

                dtPaymentUsage.Rows.Add(item["KodeSite"], item["Nik"], item["Nama"], item["Jabatan"], item["Divisi"], item["Site"], PaymentNo, item["CostCenter"], Amount, ToPerson, PaymentType, DateCreated, DateTime.Now.ToString("dd MMM yyyy"), item["Company"], item["NameBank"], item["AddressBank"], item["BankBranch"], item["NoRekening"], item["NameRekening"]);
            }

            return dtPaymentUsage;
        }

        public DataTable GetPaymentAplicationCellphone(string UserID, string PaymentNo,string Amount, string DateCreated, string ToPerson)
        {
            cbll bll = new cbll();
            DataTable dtPaymentUsage = new DataTable("PaymentApplicationInternal");
            dtPaymentUsage.Columns.Add("pano");
            dtPaymentUsage.Columns.Add("pato");
            dtPaymentUsage.Columns.Add("nama");
            dtPaymentUsage.Columns.Add("Department");
            dtPaymentUsage.Columns.Add("jabatan");
            dtPaymentUsage.Columns.Add("divisi");
            dtPaymentUsage.Columns.Add("period_dt");
            dtPaymentUsage.Columns.Add("bill_amt");
            dtPaymentUsage.Columns.Add("personal_amt");
            dtPaymentUsage.Columns.Add("detail_amt");
            dtPaymentUsage.Columns.Add("ccenter");
            dtPaymentUsage.Columns.Add("dt");
            dtPaymentUsage.Columns.Add("usage_typ");
            dtPaymentUsage.Columns.Add("request_sts");

            List<cArrayList> arr = new List<cArrayList>();
            
            arr.Add(new cArrayList("@pa_no", PaymentNo));
            DataTable dta = new DataTable();

            bll.vGetEmpAccCellph(ref dta, arr);

            if (dta.Rows.Count > 0)
            {
                System.Data.DataRow item = dta.Rows[0];

                dtPaymentUsage.Rows.Add(item["pano"],item["pato"], item["nama"], item["Department"], item["jabatan"], item["divisi"], item["period_dt"], item["bill_amt"], item["personal_amt"], item["detail_amt"], item["ccenter"], item["dt"], item["usage_typ"], item["request_sts"]);
            }

            return dtPaymentUsage;
        }

        public static DataTable GetPaymentApplicationUsagedec(string UserId, string PaymentNo, string PaymentType, string Amount, string DateCreated, string ToPerson)
        {
            cbll bll = new cbll();
            DataTable dtPaymentUsage = new DataTable("PaymentApplicationInternal");
            dtPaymentUsage.Columns.Add("pano");
            dtPaymentUsage.Columns.Add("pato");
            dtPaymentUsage.Columns.Add("nama");
            dtPaymentUsage.Columns.Add("Department");
            dtPaymentUsage.Columns.Add("jabatan");
            dtPaymentUsage.Columns.Add("divisi");
            dtPaymentUsage.Columns.Add("period_dt");
            dtPaymentUsage.Columns.Add("bill_amt");
            dtPaymentUsage.Columns.Add("personal_amt");
            dtPaymentUsage.Columns.Add("detail_amt");
            dtPaymentUsage.Columns.Add("ccenter");
            dtPaymentUsage.Columns.Add("dt");
            dtPaymentUsage.Columns.Add("usage_typ");
            dtPaymentUsage.Columns.Add("request_sts");

            List<cArrayList> arr = new List<cArrayList>();

            arr.Add(new cArrayList("@pa_no", PaymentNo));
            DataTable dta = new DataTable();

            bll.vGetEmpAccCellph(ref dta, arr);

            if (dta.Rows.Count > 0)
            {
                System.Data.DataRow item = dta.Rows[0];

                dtPaymentUsage.Rows.Add(item["pano"], item["pato"], item["nama"], item["Department"], item["jabatan"], item["divisi"], item["period_dt"], item["bill_amt"], item["personal_amt"], item["detail_amt"], item["ccenter"], item["dt"], item["usage_typ"], item["request_sts"]);
            }

            return dtPaymentUsage;
        }

        public DataTable approvelist(string pa_no)
        {
            cbll bll = new cbll();
            DataTable dtappvlist = new DataTable("PaymentApplicationInternal_appv");
            dtappvlist.Columns.Add("ReqNiksite");
            dtappvlist.Columns.Add("ReqRole");
            dtappvlist.Columns.Add("RequestDate");
            dtappvlist.Columns.Add("Approve1Nama");
            dtappvlist.Columns.Add("Approve1Role");
            dtappvlist.Columns.Add("Approve1Action");
            dtappvlist.Columns.Add("Approve1Date");
            dtappvlist.Columns.Add("Approve2Nama");
            dtappvlist.Columns.Add("Approve2Role");
            dtappvlist.Columns.Add("Approve2Action");
            dtappvlist.Columns.Add("Approve2Date");
            dtappvlist.Columns.Add("Approve3Nama");
            dtappvlist.Columns.Add("Approve3Role");
            dtappvlist.Columns.Add("Approve3Action");
            dtappvlist.Columns.Add("Approve3Date");

            dtappvlist.Columns.Add("Approve4Nama");
            dtappvlist.Columns.Add("Approve4Role");
            dtappvlist.Columns.Add("Approve4Action");
            dtappvlist.Columns.Add("Approve4Date");

            dtappvlist.Columns.Add("Approve5Nama");
            dtappvlist.Columns.Add("Approve5Role");
            dtappvlist.Columns.Add("Approve5Action");
            dtappvlist.Columns.Add("Approve5Date");

            dtappvlist.Columns.Add("Approve6Nama");
            dtappvlist.Columns.Add("Approve6Role");
            dtappvlist.Columns.Add("Approve6Action");
            dtappvlist.Columns.Add("Approve6Date");

            dtappvlist.Columns.Add("Approve7Nama");
            dtappvlist.Columns.Add("Approve7Role");
            dtappvlist.Columns.Add("Approve7Action");
            dtappvlist.Columns.Add("Approve7Date");

            dtappvlist.Columns.Add("Approve8Nama");
            dtappvlist.Columns.Add("Approve8Role");
            dtappvlist.Columns.Add("Approve8Action");
            dtappvlist.Columns.Add("Approve8Date");

            List<cArrayList> arr = new List<cArrayList>();

            arr.Add(new cArrayList("@DocNumber", pa_no));
            DataTable dta = new DataTable();

            bll.vGetAppvList(ref dta, arr);

            if (dta.Rows.Count > 0)
            {
                System.Data.DataRow item = dta.Rows[0];

                dtappvlist.Rows.Add(item["ReqNiksite"], item["ReqRole"], item["RequestDate"], item["Approve1Nama"], item["Approve1Role"], item["Approve1Action"], item["Approve1Date"], item["Approve2Nama"], item["Approve2Role"], item["Approve2Action"], item["Approve2Date"], item["Approve3Nama"], item["Approve3Role"], item["Approve3Action"], item["Approve3Date"], item["Approve4Nama"], item["Approve4Role"], item["Approve4Action"], item["Approve4Date"], item["Approve5Nama"], item["Approve5Role"], item["Approve5Action"], item["Approve5Date"], item["Approve6Nama"], item["Approve6Role"], item["Approve6Action"], item["Approve6Date"], item["Approve7Nama"], item["Approve7Role"], item["Approve7Action"], item["Approve7Date"], item["Approve8Nama"], item["Approve8Role"], item["Approve8Action"], item["Approve8Date"]);
            }
            return dtappvlist;
        }

        public DataTable GetPAList(string UserId)
        {
            cbll bll = new cbll();
            DataTable dtpalist = new DataTable("pa_list");

            dtpalist.Columns.Add("PA#");
            dtpalist.Columns.Add("COMPANY");
            dtpalist.Columns.Add("DIVISION");
            dtpalist.Columns.Add("NAME");
            dtpalist.Columns.Add("COSTCODE");
            dtpalist.Columns.Add("DATE");
            dtpalist.Columns.Add("TIPE");
            dtpalist.Columns.Add("AMOUNT");
            dtpalist.Columns.Add("STATUS");

            List<cArrayList> arr = new List<cArrayList>();

            arr.Add(new cArrayList("@nik", UserId));
            DataTable dta = new DataTable();

            bll.vGetListPA(ref dta, arr);

            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    System.Data.DataRow item = dta.Rows[i];

                    dtpalist.Rows.Add(item["PA#"], item["COMPANY"], item["DIVISION"], item["NAME"], item["COSTCODE"], item["DATE"], item["TIPE"], item["AMOUNT"], item["STATUS"]);
                }
            }

            return dtpalist;
        }
    }
}