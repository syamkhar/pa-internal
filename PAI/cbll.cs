﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.DirectoryServices;
using System.Text;

namespace rpa
{
    class cbll
    {
        DataTable dta;
        cdal dal = new cdal();
        ArrayList arr = new ArrayList();

        public void vInsRPA(List<cArrayList> arr, ref string val)
        {
            dal.vExecuteScalarSp("SP_RPA_INSERT", arr, ref val);
        }

        public void vInsRPAD(List<cArrayList> arr, ref string val)
        {
            dal.vExecuteScalarSp("SP_RPA_INSERT_D", arr, ref val);
        }

        public void vInsDecD(List<cArrayList> arr, ref string val)
        {
            dal.vExecuteScalarSp("SP_CASH_ADV_DEC_D_INS", arr, ref val);
        }

        public void vInsRPACP(List<cArrayList> arr, ref string val)
        {
            dal.vExecuteScalarSp("SP_RPA_INSERT_cellphone", arr, ref val);
        }

        public void vInsDecH(List<cArrayList> arr, ref string val)
        {
            dal.vExecuteScalarSp("SP_CASH_ADV_DEC_INS", arr, ref val);
        }

        public void vInsPAEXT(List<cArrayList> arr, ref string val)
        {
            dal.vExecuteScalarSp("sp_insert_update_pa_ext", arr, ref val);
        }

        public void vInsPAEXTD(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_insert_pa_ext_d", arr);
        }

        public void vUpdPAEXTD(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_update_pa_ext_d", arr);
        }

        public void vInsPAEXTDoc(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_insert_pa_ext_doc", arr);
        }

        public void vUpdPAEXTDoc(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_update_pa_ext_doc", arr);
        }

        public void vSubmitPAEXT(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_submit_pa_ext", arr);
        }

        public void vGetListPAEXT(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_pa_get_list_by_nik", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAEXTbyid(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_pa_ext_get_by_id", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAEXTDbyid(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_pa_ext_d_get_by_id", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAEXTDocbyid(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_pa_ext_doc_get_by_id", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAExtHist(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("pa_ext_history", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetEmpAcc(ref SqlDataReader rs, List<cArrayList> arr)
        {
            rs = dal.vGetRecordsetSP("sp_emp_acc", arr);
        }

        public void vgetGLCA(ref DataTable dta)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_CA_GL_Acc");
            dta.Load(rs);
            rs.Close();
        }

        public void vGetOnBehalf(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getonbehalf",arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vChkCP(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_chkCP", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetListPA(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getpa", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetListDec(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_list_dec", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetListPASearch(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getpalistsearch", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAList(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getpa_from_list", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetAmtSettle(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_checkamtset", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetFilenm(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getfilenm", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetRestrictFn(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getrestricchar", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetDec(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getdec", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAListd(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getpa_from_list_d", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAListd2(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getpa_from_list_d2", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetDecList(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getdec_d", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetCompListPA(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getcomplist", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetDivListPA(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_getdivlist", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetDescList(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_cat_list_get", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetListfupld(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_get_fupload", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetPAHist(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("PA_INT_LOGWF_REKAP_RPA", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetListCurr(ref DataTable dta)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_list_curr");
            dta.Load(rs);
            rs.Close();
        }

        public void vGetExcRate(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_chk_conv_curr", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vUpdateRPA(List<cArrayList> arr)
        {
            dal.vExecuteSp("SP_RPA_UPDATE_cellphone", arr);
        }

        public void vUpdateDecH(List<cArrayList> arr)
        {
            dal.vExecuteSp("SP_CASH_ADV_DEC_UPD", arr);
        }

        public void vSubmitPA(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_submit_pa", arr);
        }

        public void vSubmitPA2(List<cArrayList> arr)
        {
            dal.vExecuteSp("PA_INT_SUBMIT_PROCESS", arr);
        }

        public void vSubmitDec(List<cArrayList> arr)
        {
            dal.vExecuteSp("DEC_CASHADVAN_NON_SUBMIT_PROCESS", arr);
        }

        public void vInsertfupload(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_save_files", arr);
        }

        public void vDeleteCatList(List<cArrayList> arr)
        {
            dal.vExecuteSp("SP_delete_pa_d", arr);
        }

        public void vDeleteFilename(List<cArrayList> arr)
        {
            dal.vExecuteSp("sp_deletefilenm", arr);
        }

        public void vDeleteDecList(List<cArrayList> arr)
        {
            dal.vExecuteSp("SP_delete_dec_d", arr);
        }

        public void vGetListSite(ref DataTable dta)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_site_get");
            dta.Load(rs);
            rs.Close();
        }

        public void vGetListDept(ref DataTable dta)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_department_get");
            dta.Load(rs);
            rs.Close();
        }

        public string sGetEmpIDFromAD(string sDomain, string sUserID, string sPassword)
        {
            string sNIK = "";
            string sTemp = "";
            string spwd = "";

            if (sDomain == "PCS")
            {
                //List<cArrayList> arr = new List<cArrayList>();
                //arr.Add(new cArrayList("@username", sUserID));
                //SqlDataReader rs = dal.vGetRecordsetSP("sp_pcs_user", arr);
                //while (rs.Read())
                //{
                //    sNIK = rs["username"].ToString();
                //    sTemp = rs["password"].ToString();
                //}
                //rs.Close();

                //if (sNIK != "")
                //{
                //    spwd = Decrypt(sTemp);
                //    if (spwd == sPassword)
                //    {
                //        sNIK = sUserID;
                //    }
                //    else
                //    {
                //        sNIK = "NOT VALID PCS USER";
                //    }
                //}
            }
            else
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://" + sDomain, sUserID, sPassword);
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(&(objectClass=user)(sAMAccountname=" + sUserID.Trim() + "))";
                try
                {
                    search.PropertiesToLoad.Add("EmployeeID");

                    SearchResult result = search.FindOne();
                    try
                    {
                        sNIK = result.Properties["EmployeeID"][0].ToString(); //Ambil email properties dari AD

                    }
                    catch (Exception ex)
                    {
                        sNIK = ex.Message;
                    }
                }
                catch (Exception ex)
                {
                    sNIK = "NOT VALID USER";
                }

            }
            return (sNIK);
        }

        private string Decrypt(string cipherText)
        {
            //string EncryptionKey = "MAKV2SPBNI99212";
            string EncryptionKey = "JOLIS_Woles";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public void vGetEmpAcc(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_emp_acc", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetEmpAccCellph(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_print_pa_cellphone", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetAppvList(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("PA_INT_WATERMARK_CETAK_2", arr);
            dta.Load(rs);
            rs.Close();
        }

        public void vGetFupload(ref DataTable dta, List<cArrayList> arr)
        {
            SqlDataReader rs = dal.vGetRecordsetSP("sp_get_fupload", arr);
            dta.Load(rs);
            rs.Close();
        }

        public string sExecuteSQL(string sSQL)
        {
            string sTemp = null;
            SqlDataReader rs = dal.vGetRecordsetSQL(sSQL);
            while (rs.Read())
            { sTemp = rs[0].ToString(); }
            rs.Close();
            return (sTemp);
        }

        public DataTable FetchReimbursh()
        {
            DataTable mydatatable = new DataTable();
            DataColumn mydatacolumn;

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.Int32");
            mydatacolumn.ColumnName = "no";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "catagory";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "amt";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "remarks";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "category-id";
            mydatatable.Columns.Add(mydatacolumn);
            return mydatatable;
        }

        public void InsertReimbursh(string newcat, string newamt,string newremark, DataTable mytable, string newcat_id)
        {
            DataRow row;
            row = mytable.NewRow();
            int no;
            if (mytable.Rows.Count == 1 && mytable.Rows[0]["no"].ToString() == "1")
            {
                no = mytable.Rows.Count + 1;
            }
            else if(mytable.Rows.Count > 1)
            { no = mytable.Rows.Count + 1; }
            else { no = mytable.Rows.Count; }

            int index = newamt.IndexOf(".");
            if (index > 0)
            {
                newamt = newamt.Remove(index, 3);
                newamt = newamt.Replace(",", "");
            }

            row["no"] = no;
            row["catagory"] = newcat;
            row["amt"] = newamt;
            row["remarks"] = newremark;
            row["category-id"] = newcat_id;
            mytable.Rows.Add(row);

            if (mytable.Rows[0]["amt"].ToString() == "")
            {
                mytable.Rows.RemoveAt(0);
            }
        }

        public void UpdateReimbursh(string newcat, string newamt, string newremark, DataTable mytable, int baris, string newcat_id)
        {
            int index = newamt.IndexOf(".");
            if (index > 0)
            {
                newamt = newamt.Remove(index, 3);
                newamt = newamt.Replace(",", "");
            }

            mytable.Rows[baris]["catagory"] = newcat;
            mytable.Rows[baris]["amt"] = newamt;
            mytable.Rows[baris]["remarks"] = newremark;
            mytable.Rows[baris]["category-id"] = newcat_id;
        }

        public void DeleteReimbursh(DataTable mytable, int baris)
        {
            mytable.Rows[baris].Delete();
            mytable.AcceptChanges();
        }

        public DataTable FetchDec()
        {
            DataTable mydatatable = new DataTable();
            DataColumn mydatacolumn;

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.Int32");
            mydatacolumn.ColumnName = "no";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "decdate";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "dectype";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "curr";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "amt";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "remarks";
            mydatatable.Columns.Add(mydatacolumn);

            mydatacolumn = new DataColumn();
            mydatacolumn.DataType = Type.GetType("System.String");
            mydatacolumn.ColumnName = "dectype-id";
            mydatatable.Columns.Add(mydatacolumn);

            return mydatatable;
        }

        public void InsertDec(string newdate, string newtyp, string newcurr, string newamt, string newremark, DataTable mytable, string newtyp_id)
        {
            DataRow row;
            row = mytable.NewRow();
            int no;
            if (mytable.Rows.Count == 1 && mytable.Rows[0]["no"].ToString() == "1")
            {
                no = mytable.Rows.Count + 1;
            }
            else if (mytable.Rows.Count > 1)
            { no = mytable.Rows.Count + 1; }
            else { no = mytable.Rows.Count; }

            int index = newamt.IndexOf(".");
            if (index > 0)
            {
                newamt = newamt.Remove(index, 3);
                newamt = newamt.Replace(",", "");
            }

            if (newamt == "")
            {
                newamt = "0";
            }

            row["no"] = no;
            row["decdate"] = newdate;
            row["dectype"] = newtyp;
            row["curr"] = newcurr;
            row["amt"] = newamt;
            row["remarks"] = newremark;
            row["dectype-id"] = newtyp_id;
            mytable.Rows.Add(row);

            if (mytable.Rows[0]["amt"].ToString() == "")
            {
                mytable.Rows.RemoveAt(0);
            }
        }

        public void UpdateDec(string newdate, string newtyp, string newcurr, string newamt, string newremark, DataTable mytable, string newtyp_id, int baris)
        {
            int index = newamt.IndexOf(".");
            if (index > 0)
            {
                newamt = newamt.Remove(index, 3);
                newamt = newamt.Replace(",", "");
            }

            if (newamt == "")
            {
                newamt = "0";
            }
            
            mytable.Rows[baris]["decdate"] = newdate;
            mytable.Rows[baris]["dectype"] = newtyp;
            mytable.Rows[baris]["curr"] = newcurr;
            mytable.Rows[baris]["amt"] = newamt;
            mytable.Rows[baris]["remarks"] = newremark;
            mytable.Rows[baris]["dectype-id"] = newtyp_id;
        }

        public void DeleteDec(DataTable mytable, int baris)
        {
            mytable.Rows[baris].Delete();
            mytable.AcceptChanges();
        }
    }
}