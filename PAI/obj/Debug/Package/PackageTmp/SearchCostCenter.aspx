﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCostCenter.aspx.cs" Inherits="rpa.SearchCostCenter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Search BTR</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />

    <script type="text/javascript">
        function UseSelected(ccode, desc) {
            var pw = window.opener;
            var inputFrm = pw.document.forms['form1'];
            if (inputFrm.elements['ctrlToFind'].value == 'Originator') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_Originator'].value = nopeg;
                inputFrm.elements['ctl00_ContentPlaceHolder1_OriginatorName'].value = nama;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode1') {
                inputFrm.elements['txtcostcenter'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode2') {
                inputFrm.elements['txtcostcenter2'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode3') {
                inputFrm.elements['txtcostcenter3'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode4') {
                inputFrm.elements['txtcostcenter4'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode4') {
                inputFrm.elements['txtcostcenter4'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode5') {
                inputFrm.elements['txtcostcenter5'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode6') {
                inputFrm.elements['txtcostcenter6'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode7') {
                inputFrm.elements['txtcostcenter7'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode8') {
                inputFrm.elements['txtcostcenter8'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode9') {
                inputFrm.elements['txtcostcenter9'].value = ccode;
            } else if (inputFrm.elements['ctrlToFind'].value == 'ccode10') {
                inputFrm.elements['txtcostcenter10'].value = ccode;
            } else {
                
            }
            window.close();
        }
        function Left(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else
                return String(str).substring(0, n);
        }

        function Right(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else {
                var iLen = String(str).length;
                return String(str).substring(iLen, iLen - n);
            }
        }

        function keyPressed(e) {
            switch (e.keyCode) {
                case 13:
                    {
                        document.getElementById("btnSearch").click();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        function cekArgumen() {
            var d = document;
            if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)) {
                alert("Please Entry Specific NIK / Name (minimal 3 Character)");
                return false;
            }
        }
    </script>

</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white form-group" style="background-color:White;">
    <form id="form1" runat="server" class="page-wrapper">
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <div class="page-content">
    <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div class="page-content">           
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center">Search Field Code</caption>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td>
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0" class="table table-responsive">
      <tr>
         <%--<td style="vertical-align:top"><b>Page:</b>&nbsp;</td>--%>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <%--<asp:LinkButton ID="btnPage" CommandName="Page" 
                        CommandArgument="<%#Container.DataItem %>" 
                        CssClass="text" Runat="server" 
                        Visible='<%#(CInt(Container.DataItem) <> (PageNumber+1)).ToString()%>'>
                         <%#Container.DataItem %>
                         </asp:LinkButton>--%>
                         <%--Visible='<%# (Convert.ToInt32(Container.DataItem) == (PageNumber+1)).ToString()%>' --%>
                         <%--<asp:Label ID="lblPage" runat="server" 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>--%>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td>
               <asp:Repeater ID="rptResult" runat="server">
               <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No</th>
                                <th>CCode</th>
                                <th>Description</th>
                                <th>Company ID</th>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td><%#Container.ItemIndex + 1%></td>
                        <td><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "value")%>','<%#DataBinder.Eval(Container.DataItem, "vdesc")%>');"><%#DataBinder.Eval(Container.DataItem, "value")%></a></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "vdesc")%></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "compid")%></td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
        <ProgressTemplate>
        <asp:Panel ID="pnlProgressBar" runat="server">    
            <div><asp:Image ID="Image1" runat="Server" ImageUrl="assets/pages/img/ajax-loader.gif"/>Please 
                Wait...
            </div>
        </asp:Panel>
        </ProgressTemplate> 
    </asp:UpdateProgress>    
    </div>
    </form>
</body>
</html>
