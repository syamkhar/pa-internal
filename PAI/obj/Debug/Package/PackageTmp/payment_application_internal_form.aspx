﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payment_application_internal_form.aspx.cs" Inherits="rpa.payment_application_internal_form" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cash Advance Delaration Form</title>
    <style type="text/css">
    body:nth-of-type(1) img[src*="Blank.gif"] {
        display: none;
    }
    </style>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="page-container">
            <div class="page-wrapper">
                <div class="page-content-wrapper">
                    <div class="page-content">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:TextBox ID="arg1" runat="server" Visible ="false"></asp:TextBox>
    <asp:TextBox ID="arg2" runat="server" Visible ="false"></asp:TextBox>
    <asp:TextBox ID="arg3" runat="server" Visible ="false"></asp:TextBox>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="756px" Width="949px" ShowExportControls="True" 
            ShowPrintButton="False" InteractiveDeviceInfos="(Collection)" 
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
            <LocalReport ReportPath="laporan\business_declatation.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet6_V_H002" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource2" Name="DataSet6_V_H00105" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource3" Name="DataSet6_V_H001051" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource4" Name="DataSet6_BTRGroupPrice" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource5" Name="Dataset6_appv" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_rpa %>"         
                
            SelectCommand="SELECT Decno, GenId, decdate, expensetype, currency, subtotal, remarks, remarks2, (SELECT currcode FROM rpa_pa WHERE (pa_no IN (SELECT pa_no FROM [dbo].[CASH_ADV_DEC] WHERE (DecNo = [dbo].[CASH_ADV_DEC_D].Decno)))) AS CurrAdvan,(SELECT amt_paid FROM rpa_pa AS V_H001_1 WHERE (pa_no IN (SELECT pa_no FROM [dbo].[CASH_ADV_DEC] AS V_H002_1 WHERE (DecNo = [dbo].[CASH_ADV_DEC_D].Decno)))) AS CashAdvan FROM [dbo].[CASH_ADV_DEC_D] WHERE (Decno = @trip) ORDER BY currency">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg3" Name="trip" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_rpa %>"   
                
            SelectCommand="SELECT decno, genid, decdate, (select [description] from category_list where coa=[CASH_ADV_DEC_D].expensetype)expensetype, currency, subtotal, remarks, remarks2 FROM [dbo].[CASH_ADV_DEC_D] WHERE (decno = @trip) ORDER BY decdate">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg2" Name="trip" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_rpa %>" 
                
            SelectCommand="SELECT DecNo, pa_no, TransDate, Remarks, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, check_sup, decTotal, currcode, CreatedBy, CreatedIn, CreatedTime, ModifiedBy, ModifiedIn, ModifiedTime, StEdit, DeleteBy, DeleteTime, fstatus, nik, (SELECT Nama FROM jrn_test..H_A101 WHERE (Niksite = [dbo].[CASH_ADV_DEC] .nik)) AS nama, (SELECT NmDepar FROM jrn_test..H_A130 WHERE (KdDepar = (SELECT kddepar FROM rpa_pa WHERE (pa_no = [dbo].[CASH_ADV_DEC].pa_no)))) AS nmdepar, (SELECT NmJabat FROM jrn_test..H_A150 WHERE (KdJabat = (SELECT kdjabatan FROM jrn_test..h_a101 AS V_H001_4 WHERE (niksite = [dbo].[CASH_ADV_DEC].nik)))) AS nmjabat,(SELECT pycostcode FROM jrn_test..H_A101 AS V_H001_3 WHERE (niksite = [CASH_ADV_DEC].nik)) AS costcode,(SELECT amount FROM rpa_pa AS V_H001_2 WHERE (pa_no = [CASH_ADV_DEC].pa_no)) AS cashadvan,(SELECT Nama FROM jrn_test..H_A101 AS H_A101_5 WHERE (Niksite = [CASH_ADV_DEC].sup_nik)) AS superior,(SELECT Nama FROM jrn_test..H_A101 AS H_A101_4 WHERE (Niksite = [CASH_ADV_DEC].reqby)) AS req,(SELECT Nama FROM jrn_test..H_A101 AS H_A101_3 WHERE (Nik = [CASH_ADV_DEC].apprby)) AS appr, (SELECT Nama FROM jrn_test..H_A101 AS H_A101_2 WHERE (Nik = [CASH_ADV_DEC].reviewby)) AS review, (SELECT Nama FROM jrn_test..H_A101 AS H_A101_1 WHERE (Nik = [CASH_ADV_DEC].proceedby)) AS proceed, 'Operational' AS detailtrip,(SELECT costName FROM jrn_test..H_A221 WHERE (costcode = (SELECT pycostcode FROM jrn_test..H_A101 AS V_H001_3 WHERE (nik = [CASH_ADV_DEC].nik)))) AS costname,(SELECT NIKSITE FROM jrn_test..H_A101 AS H_A101_6 WHERE (Nik = [CASH_ADV_DEC].reqby)) AS niksite FROM [dbo].[CASH_ADV_DEC] WHERE (DecNo = @argReg)">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg1" Name="argReg" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_rpa %>" SelectCommand="SELECT currency, '' AS Tickets, '' AS Hotel, '' AS Transport, '' AS Others, '' AS Food, 
(select [description] from category_list where coa=expensetype)expensetype,sum(subtotal) total
FROM [CASH_ADV_DEC_D] WHERE Decno = @trip
GROUP BY currency, expensetype ">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg1" Name="trip" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_rpa %>" 
            SelectCommand="DEC_CASHADVAN_NON_WATERMARK_CETAK_2" SelectCommandType="StoredProcedure">
        <selectparameters>
              <asp:ControlParameter ControlID="arg1" Name="DocNumber" PropertyName="Text" 
                  Type="String" />
              </selectparameters>
        </asp:SqlDataSource>

        
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H001051TableAdapter">
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H00105TableAdapter">
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H002TableAdapter">
        </asp:ObjectDataSource>
        
        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.BTRGroupPriceTableAdapter">
        </asp:ObjectDataSource>
     </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
