﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dec_ca.aspx.cs" Inherits="rpa.dec_ca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <meta charset="utf-8" />
        <title>CASH ADVANCE DECLARATION</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="jresources PA Internal Usage" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        
        <script src="assets/calendar/jquery.min.js" type="text/javascript"></script>
         <script src="assets/calendar/tcal.js" type="text/javascript"></script>
        <link href="assets/calendar/tcal.css" rel="stylesheet" type="text/css" />
        <script src="assets/calendar/jscal2.js" type="text/javascript"></script>
        <script src="assets/calendar/lang/en.js" type="text/javascript"></script>
        <link href="assets/calendar/jscal2.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/border-radius.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/win2k/win2k.css" rel="stylesheet" type="text/css" />
    <script src="assets/calendar/jsm01.js" type="text/javascript"></script>
    <script src="assets/calendar/jquery-1.3.2.min.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" >
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode < 45 || charCode > 57 || charCode == 47 || charCode == 45)
                    return false;

                return true;
            }

            function chkchar(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 39 || charCode == 43)
                    return false;

                return true;
            }

            function OpenPopup(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("SearchEmployee.aspx", "List", "scrollbars=no,resizable=no,width=500,height=400");
                return false;
            }

            function OpenPopupfieldcostcd(key) {
                var d = document;
                var jml = d.getElementById("jml").value;
                document.getElementById("ctrlToFind").value = key;
                window.open("searchcostcode.aspx?M" + jml, "List", "scrollbars=no,resizable=no,width=500,height=400");
                return false;
            }

            function OpenPopuptrip(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("Searchpa.aspx", "List", "scrollbars=yes,resizable=yes,width=1000,height=400");
                return false;
            }

            function frmcetak() {
                var d = document;
                window.location = "business_dec_trip_report.aspx?id=" + d.getElementById("decno").value
            }

            
        </script>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white form-group">
    <form id="form1" runat="server" class="page-wrapper">
    <div class="page-wrapper">
             <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="rpa_entry.aspx">
                            <img src="assets/layouts/layout/img/logo.jpg" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->                           
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <%--<li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>--%>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
             <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <%--<form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>--%>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <%--<li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_2.html" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Dashboard 2</span>
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_3.html" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">Dashboard 3</span>
                                            <span class="badge badge-danger">5</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>--%>
                           <%-- <li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li>--%>
                            
                            
                            <li class="nav-item  active open">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">RPA</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="rpa_entry.aspx" class="nav-link ">
                                            <span class="title">
                                                Entry  
                                                <br/>Payment Aplication</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                    
                                
                                    <li class="nav-item">
                                        <a href="rpa_pa_list.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Payment Aplication</span>
                                            <span></span>
                                        </a>
                                    </li>

                                     <li class="nav-item active open">
                                        <a href="dec_ca.aspx" class="nav-link ">
                                            <span class="title">Declaration  
                                                <br/>Cash Advanse Non BTR</span>
                                            <span ></span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="declarationlist.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Declaration Non BTR</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>

                                   <li class="nav-item" >
                                        <a href="reports.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Reports</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                 </ul>
                            </li>
                            
                            <%--<li class="heading">
                                <h3 class="uppercase">Layouts</h3>
                            </li>--%>
                            
                           
                            <%--<li class="heading">
                                <h3 class="uppercase">Pages</h3>
                            </li>--%>
                            
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <%--<div class="theme-panel hidden-xs hidden-sm">
                            <div class="toggler"> </div>
                            <div class="toggler-close"> </div>
                            <div class="theme-options">
                                <div class="theme-option theme-colors clearfix">
                                    <span> THEME COLOR </span>
                                    <ul>
                                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                    </ul>
                                </div>
                                <div class="theme-option">
                                    <span> Theme Style </span>
                                    <select class="layout-style-option form-control input-sm">
                                        <option value="square" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Layout </span>
                                    <select class="layout-option form-control input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Header </span>
                                    <select class="page-header-option form-control input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Top Menu Dropdown</span>
                                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                                        <option value="light" selected="selected">Light</option>
                                        <option value="dark">Dark</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Mode</span>
                                    <select class="sidebar-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Menu </span>
                                    <select class="sidebar-menu-option form-control input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Style </span>
                                    <select class="sidebar-style-option form-control input-sm">
                                        <option value="default" selected="selected">Default</option>
                                        <option value="light">Light</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Position </span>
                                    <select class="sidebar-pos-option form-control input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Footer </span>
                                    <select class="page-footer-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                            </div>
                        </div>--%>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Payment Application</span>
                                </li>
                                
                               
                            </ul>
                           <%-- <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">
                                                <i class="icon-bell"></i> Action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-shield"></i> Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-user"></i> Something else here</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-bag"></i> Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>--%>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> PAYMENT APPLICATION (INTERNAL)
                            <small>Declaration Cash Advance</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
    <div class="row ">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-dark"></i>
                        <span class="caption-subject font-dark bold uppercase">ENTRY DEC</span>
                    </div>
                    <%--<div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>--%>
                    <br />
                    <hr />
    <div class="row">
                <!-- DEC START-->
                
    <input type="hidden" id="ctrlToFind" />
    <input style="width:90px;" class="entri" type="hidden" id="decno" value='<%=decno %>' />
    <input type="hidden" id="cmpltsts" runat="server" />
    <input type="hidden" id="htotamt" runat="server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
    <caption style="text-align: center; font-size: 1.5em; color:White;">Business Declaration</caption>
        <tr>
             <%--<td style ="width:10%;">Declaration Number</td>
             <td style ="width:15%;"> </td>--%>
             <td style ="width:10%;">CA(Operation)Req.No.</td>
            <td style ="width:15%;">
            <input type="hidden" id="htripno" runat="server" />
            <asp:TextBox ID="tripno1" runat="server" Enabled="false" style="width:70px;"></asp:TextBox>
              <img alt="add" id="Img4" src="assets/pages/img/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopuptrip('dec')" /></td>
             <td style ="width:10%;"> Site</td>
             <td style ="width:15%;"><input type="hidden" id="hsite" runat="server" />
             <input style="width:90px;" class="entri" disabled="disabled" type="text" id="site" runat="server"/></td>
             <td style ="width:10%;">Cost Code</td>
             <td style ="width:15%;"> <input type="hidden" id="hcostcode" runat="server" />
             <input type="text" id="costcode" style="width:90px;" class="entri"  disabled= "disabled" runat="server"/> <input type="hidden" id="costcd" runat="server"/>
             <%--<img alt="add" id="Img1" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfieldcostcd('cc')" />--%> 
             </td>
             <td></td>
        </tr>
        <tr>
            
            <td>Department</td>
            <td><input type="hidden" id="kddepar" runat="server" /> <input type="hidden" id="nmdepar" runat="server" />
            <input type="text" id="dep" disabled= "disabled" style="width:90px;" class="entri" runat="server"/> </td> 
            <td>Superior Name </td>
            <td><input type="hidden" id="supnik" runat="server"/> <input type="hidden" id="hsuperior" runat="server" />
            <input type="text" id="superior" disabled= "disabled" style="width:90px;" class="entri" runat="server"/>
                          <%--<img alt="add" id="Img5" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('suser')" />--%> </td>
             <td>Status</td>
            <td><input type="hidden" id="hfstatus" runat="server" />
            <input style="width:90px;" class="entri" disabled="disabled" type="text" id="fstatus" runat="server"/></td>   
            <td></td><td class="style8"></td>
        </tr>
        <tr>
            <td>NIK</td>
            <td colspan="0">
            <input type="hidden" id="hnik" runat="server" />
            <input style="width:90px;" class="entri" disabled="disabled" type="text" id="nik" runat="server"/> 
            </td>
            <td>Position</td>
            <td><input type="hidden" id="nmjabat" runat="server"/> <input type="hidden" id="hposition" runat="server" />
            <input type="text" id="position" disabled= "disabled" style="width:90px;" class="entri" runat="server"/></td>
            <td>Type</td><td><input type="hidden" id="htype" runat="server" />
            <input type="text" id="type" disabled= "disabled" style="width:90px;" class="entri" runat="server"/></td>
        </tr>
        <tr>
        <td>Name</td>
        <td> <input type="hidden" id="hnama" runat="server" />
        <input style="width:90px;" class="entri" disabled="disabled" type="text" id="nama" runat="server"/> </td>
        </tr>
        <tr>
        <td>Declare Date</td>
        <td><input type="hidden" id="hdecdate" runat="server" />
        <asp:TextBox ID="decdate" runat="Server" width="90px" Enabled="false" class="entri"></asp:TextBox><%--<button id="bdecdt">...</button>--%></td>
        </tr>
        <tr>
          <td>Remark</td>
          <td colspan="2"><textarea id="remark" style="width:80%;Height:100px" onkeypress="return chkchar(event)" class="entri" cols="5" rows="5" runat="server"></textarea></td>
          <td colspan="4"> 
              <%--<div style="float: left; position: absolute; top: 105px; left: 507px;">Checked by direct Outcome Report<br />
              &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chktripoutcrpt" /> Business Trip Outcome Report </div>--%>         
              <table>
              <tr>
              <td></td><td style="width:200px"><input type="hidden" id="reqby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img2" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('reqby')" />--%>
              </td>
              <td></td><td><input type="hidden" id="reqbydt" /> <%--<button id="breqbydt">...</button>--%>
                                                                                            </td>
              </tr>
              <tr>
              <td></td><td><input type="hidden" id="apprby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img3" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('supappr')" />--%>
              </td>
              <td></td><td><input type ="hidden" id="apprdate" /> <%--<button id="bapprdt">...</button>--%></td>
              </tr>
              <tr>
              <td></td><td><input type="hidden" id="reviewby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img6" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('revby')" />--%>
              </td>
              <td></td><td><input type="hidden" id="reviewdate" /> <%--<button id="breviewdt" >...</button>--%></td>
              </tr>
              <tr>
              <td></td><td><input type="hidden" id="proceedby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img7" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('finappr')" />--%>
              </td>
              <td></td><td><input type="hidden" id="proceeddate" /> <%--<button id="bproceeddt">...</button>--%></td>
              <td><input type="hidden" id="jml" value="1" /></td>
              </tr>
              <tr>
              <td>
                      
              </td>
              </tr>
              </table>
          </td> 
        </tr>
        </table> 
        
        <br />
        <h3 class="form-section">*Detail Alokasi</h3>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="no" 
        OnRowCancelingEdit="GridView1_RowCancelingEdit"
        OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand"
        ShowFooter="True" OnRowDeleting="GridView1_RowDeleting" CssClass="table table-striped table-hover table-bordered" 
        ShowHeader="True">
                <Columns>
                <asp:TemplateField HeaderText="no" >
                    <EditItemTemplate>
                        <div>
                            <asp:Label ID="Lblno" runat="server" Text='<%# Bind("no") %>'></asp:Label>
                        </div> 
                    </EditItemTemplate>
                    <FooterTemplate>
                            <asp:Label ID="Lblnewno" runat="server" Text='<%# Bind("no") %>'></asp:Label>
                    </FooterTemplate>
                    <ItemTemplate>
                            <asp:Label ID="Lblno2" runat="server" Text='<%# Bind("no") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date">
                    <EditItemTemplate>
                        <div>
                        <asp:TextBox ID="txtdtperiod" runat="server" Text='<%# Bind("decdate") %>' Width="70%"></asp:TextBox>
                        <button id="btn2">...</button>
                        </div> 
                    </EditItemTemplate>
                    <FooterTemplate>
                        <div>
                            <asp:TextBox ID="txtdtperiodnew" runat="server" Width="70%"></asp:TextBox><button id="btn1">...</button>
                         </div>
                    </FooterTemplate>
                    <ItemTemplate>
                            <asp:Label ID="lbldtperiod" runat="server" Text='<%# Bind("decdate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Kategori" >
                        <EditItemTemplate>
                        <div>
                            <asp:DropDownList ID="ddlcatlist" runat="server"></asp:DropDownList>
                        </div> 
                        </EditItemTemplate>
                        <FooterTemplate>
                        <div>
                            <asp:DropDownList ID="ddlcatlistnew" runat="server"></asp:DropDownList>
                        </div>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("dectype") %>'></asp:Label>
                                                   
                        </ItemTemplate>
                 </asp:TemplateField>
                    <asp:TemplateField HeaderText="Curr" >
                        <EditItemTemplate>
                        <div>
                            <asp:DropDownList ID="ddlcurrlist" runat="server">
                            <asp:ListItem value="IDR" Text="IDR">IDR</asp:ListItem>
		                    <asp:ListItem value="USD" Text="USD">USD</asp:ListItem>
		                    <asp:ListItem value="MYR" Text="MYR">MYR</asp:ListItem>
		                    <asp:ListItem value="SGD" Text="SGD">SGD</asp:ListItem>
                            </asp:DropDownList>
                        </div> 
                        </EditItemTemplate>
                        <FooterTemplate>
                        <div>
                            <asp:DropDownList ID="ddlcurrlistnew" runat="server">
                            <asp:ListItem value="IDR" Text="IDR">IDR</asp:ListItem>
		                    <asp:ListItem value="USD" Text="USD">USD</asp:ListItem>
		                    <asp:ListItem value="MYR" Text="MYR">MYR</asp:ListItem>
		                    <asp:ListItem value="SGD" Text="SGD">SGD</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Lblcurr" runat="server" Text='<%# Bind("curr") %>'></asp:Label>               
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount(IDR)" >
                        <EditItemTemplate>
                            <asp:textbox ID="txtbiayatot" runat="server"  Text='<%# Eval("amt") %>' Font-Size="1.0em" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:textbox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewbiayatot" runat="server" Font-Size="1.0em" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label26" runat="server" Text='<%# Bind("amt", "{0:#,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Keterangan">
                        <EditItemTemplate>
                            <asp:textbox ID="txtremarks" runat="server" Text='<%# Eval("remarks") %>' Font-Size="1.0em"></asp:textbox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewremarks" runat="server"  Font-Size="1.0em"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label40" runat="server" Text='<%# Bind("remarks") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                                           
                    <asp:TemplateField HeaderText="" ShowHeader="False" FooterStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-BorderStyle="None">
                        <EditItemTemplate>
                        <div style="width:85px;">
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update">
                            </asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel">
                            </asp:LinkButton>
                        </div>
                        </EditItemTemplate>
                        <FooterTemplate>
                        <div style="width:30px;">
                            <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="AddNew" Text="+" CssClass="font-red font-hg"></asp:LinkButton>
                        </div> 
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
                </Columns>
                                        
            </asp:GridView>
        
                                    <h3 class="form-section">Document Supporting</h3>
                                    <hr />
                                    <div class="portlet-body">
                                        <div class="form-inline" role="form">
                                            <asp:FileUpload ID="fuploadPA" runat="server" /><br />
                                            
                                            <asp:GridView ID="gvfupld" runat="server" CssClass="table table-striped table-hover table-bordered"
                                            OnRowDeleting="gvfupld_RowDeleting">
                                            <Columns>

                                            <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
                                            </Columns>
                                            </asp:GridView>

                                            
                                            <asp:Button ID="btnUpload" Text="Upload" runat="server" class="btn blue" OnClick="UploadFile" />
                                            
                                            <br />
                                            <asp:Label ID="lblMessage" class="sr-only" runat="server" />
                                        </div>
                                    </div>       
    <br />
    <br />
    <table>
    <tr>
    <td>
        <asp:Button ID="btnsave" runat="server" class="btn blue" Text="Save" 
            onclick="btnsave_Click"/>
        <asp:Button ID="btnsubmit" name="btnsubmit" class="btn blue" runat="server" Text="Submit" 
                                                onclick="btnsubmit_Click" />
    <asp:Button ID="btnprint" class="btn red-flamingo green-dark-stripe" runat="server" Text="Print" OnClick="btnprint_Click" OnClientClick="target ='_blank';"/>
    
    </td>

    <td>
    
        
    </td>
    </tr>   
    </table>
    <asp:HiddenField ID="hdecno" runat="server" />
    <asp:HiddenField ID="hfsize" runat="server" />
    
    <script type ="text/javascript" >

        //<![CDATA[

        var cal = Calendar.setup({
            onSelect: function (cal) { cal.hide() },
            showTime: false
        });

        cal.manageFields("bdecdetdt1", "txtdtperiod", "%m/%d/%Y");
        cal.manageFields("bdecdetdt2", "txtdtperiodnew", "%m/%d/%Y");
        //]]
        </script>
    </div>
        
        </div>
        </div>
        </div>

        <!-- END SAMPLE FORM PORTLET-->
                                
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
                <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
                    <div class="page-quick-sidebar">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Users
                                    <span class="badge badge-danger">2</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-target="#quick_sidebar_tab_2" data-toggle="tab"> Alerts
                                    <span class="badge badge-success">7</span>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> More
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-bell"></i> Alerts </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-info"></i> Notifications </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-speech"></i> Activities </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-settings"></i> Settings </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                                <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
                                    <h3 class="list-heading">Staff</h3>
                                    <ul class="media-list list-items">
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-success">8</span>
                                            </div>
                                            <img class="media-object" src="assets/layouts/layout/img/avatar3.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Bob Nilson</h4>
                                                <div class="media-heading-sub"> Project Manager </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="assets/layouts/layout/img/avatar1.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Nick Larson</h4>
                                                <div class="media-heading-sub"> Art Director </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-danger">3</span>
                                            </div>
                                            <img class="media-object" src="assets/layouts/layout/img/avatar4.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Deon Hubert</h4>
                                                <div class="media-heading-sub"> CTO </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="assets/layouts/layout/img/avatar2.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Ella Wong</h4>
                                                <div class="media-heading-sub"> CEO </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <h3 class="list-heading">Customers</h3>
                                    <ul class="media-list list-items">
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-warning">2</span>
                                            </div>
                                            <img class="media-object" src="assets/layouts/layout/img/avatar6.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Lara Kunis</h4>
                                                <div class="media-heading-sub"> CEO, Loop Inc </div>
                                                <div class="media-heading-small"> Last seen 03:10 AM </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="label label-sm label-success">new</span>
                                            </div>
                                            <img class="media-object" src="assets/layouts/layout/img/avatar7.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Ernie Kyllonen</h4>
                                                <div class="media-heading-sub"> Project Manager,
                                                    <br> SmartBizz PTL </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="assets/layouts/layout/img/avatar8.jpg" alt="...">
                                            <div class="media-body">
                                                <h4 class="media-heading">Lisa Stone</h4>
                                                <div class="media-heading-sub"> CTO, Keort Inc </div>
                                                <div class="media-heading-small"> Last seen 13:10 PM </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-success">7</span>
                                            </div>
                                            <img class="media-object" src="assets/layouts/layout/img/avatar9.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Deon Portalatin</h4>
                                                <div class="media-heading-sub"> CFO, H&D LTD </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="assets/layouts/layout/img/avatar10.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Irina Savikova</h4>
                                                <div class="media-heading-sub"> CEO, Tizda Motors Inc </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-danger">4</span>
                                            </div>
                                            <img class="media-object" src="assets/layouts/layout/img/avatar11.jpg" alt="..."/>
                                            <div class="media-body">
                                                <h4 class="media-heading">Maria Gomez</h4>
                                                <div class="media-heading-sub"> Manager, Infomatic Inc </div>
                                                <div class="media-heading-small"> Last seen 03:10 AM </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="page-quick-sidebar-item">
                                    <div class="page-quick-sidebar-chat-user">
                                        <div class="page-quick-sidebar-nav">
                                            <a href="javascript:;" class="page-quick-sidebar-back-to-list">
                                                <i class="icon-arrow-left"></i>Back</a>
                                        </div>
                                        <div class="page-quick-sidebar-chat-user-messages">
                                            <div class="post out">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar3.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:15</span>
                                                    <span class="body"> When could you send me the report ? </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar2.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:15</span>
                                                    <span class="body"> Its almost done. I will be sending it shortly </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar3.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:15</span>
                                                    <span class="body"> Alright. Thanks! :) </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar2.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:16</span>
                                                    <span class="body"> You are most welcome. Sorry for the delay. </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar3.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:17</span>
                                                    <span class="body"> No probs. Just take your time :) </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar2.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:40</span>
                                                    <span class="body"> Alright. I just emailed it to you. </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar3.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:17</span>
                                                    <span class="body"> Great! Thanks. Will check it right away. </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar2.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:40</span>
                                                    <span class="body"> Please let me know if you have any comment. </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="assets/layouts/layout/img/avatar3.jpg" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:17</span>
                                                    <span class="body"> Sure. I will check and buzz you if anything needs to be corrected. </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="page-quick-sidebar-chat-user-form">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Type a message here..."/>
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn green">
                                                        <i class="icon-paper-clip"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
                                <div class="page-quick-sidebar-alerts-list">
                                    <h3 class="list-heading">General</h3>
                                    <ul class="feeds list-items">
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 4 pending tasks.
                                                            <span class="label label-sm label-warning "> Take action
                                                                <i class="fa fa-share"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> Just now </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-bar-chart-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> Finance Report for year 2013 has been released. </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-danger">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> New order received with
                                                            <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 30 mins </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-bell-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> Web server hardware needs to be upgraded.
                                                            <span class="label label-sm label-warning"> Overdue </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 2 hours </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-default">
                                                                <i class="fa fa-briefcase"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> IPO Report for year 2013 has been released. </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <h3 class="list-heading">System</h3>
                                    <ul class="feeds list-items">
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 4 pending tasks.
                                                            <span class="label label-sm label-warning "> Take action
                                                                <i class="fa fa-share"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> Just now </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-danger">
                                                                <i class="fa fa-bar-chart-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> Finance Report for year 2013 has been released. </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-default">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> New order received with
                                                            <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 30 mins </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> Web server hardware needs to be upgraded.
                                                            <span class="label label-sm label-default "> Overdue </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 2 hours </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-info">
                                                                <i class="fa fa-briefcase"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> IPO Report for year 2013 has been released. </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane page-quick-sidebar-settings" id="quick_sidebar_tab_3">
                                <div class="page-quick-sidebar-settings-list">
                                    <h3 class="list-heading">General Settings</h3>
                                    <ul class="list-items borderless">
                                        <li> Enable Notifications
                                            <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                        <li> Allow Tracking
                                            <input type="checkbox" class="make-switch" data-size="small" data-on-color="info" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                        <li> Log Errors
                                            <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                        <li> Auto Sumbit Issues
                                            <input type="checkbox" class="make-switch" data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                        <li> Enable SMS Alerts
                                            <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    </ul>
                                    <h3 class="list-heading">System Settings</h3>
                                    <ul class="list-items borderless">
                                        <li> Security Level
                                            <select class="form-control input-inline input-sm input-small">
                                                <option value="1">Normal</option>
                                                <option value="2" selected>Medium</option>
                                                <option value="e">High</option>
                                            </select>
                                        </li>
                                        <li> Failed Email Attempts
                                            <input class="form-control input-inline input-sm input-small" value="5" /> </li>
                                        <li> Secondary SMTP Port
                                            <input class="form-control input-inline input-sm input-small" value="3560" /> </li>
                                        <li> Notify On System Error
                                            <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                        <li> Notify On SMTP Error
                                            <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    </ul>
                                    <div class="inner-content">
                                        <button class="btn btn-success">
                                            <i class="icon-settings"></i> Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            
            </div>
            </div>
            <%=js%>
            <%=msg %>
    </form>
            <script type="text/javascript">
                function leftTrim(sString) {
                    while (sString.substring(0, 1) == ' ') {
                        sString = sString.substring(1, sString.length);
                    }
                    return sString;
                }


                function rightTrim(sString) {
                    while (sString.substring(sString.length - 1, sString.length) == ' ') {
                        sString = sString.substring(0, sString.length - 1);
                    }
                    return sString;
                }
                function trimAll(sString) {
                    while (sString.substring(0, 1) == ' ') {
                        sString = sString.substring(1, sString.length);
                    }
                    while (sString.substring(sString.length - 1, sString.length) == ' ') {
                        sString = sString.substring(0, sString.length - 1);
                    }
                    return sString;
                }
                function onlyNumbers(evt) {
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
                        return false;


                    return true;


                }


                var patterns = new Array(
	        "###,###,###,###",  // US/British
	        "###.###.###.###",  // German
	        "### ### ### ###",  // French
	        "###'###'###'###",  // Swiss
	        "#,##,##,##,##,###",  // Indian
	        "####\u5104 ####\u4E07 ####",  // Japanese/Chinese
	        "############" // no formatting
	        );


                // A little function to take an integer string, strip out current formatting,
                // and reformat it according to a pattern string. '#' characters in the pattern
                // are substituted with digits from the integer string, other pattern characters
                // are output literally into the returned string.
                function formatInteger(integer, pattern) {
                    var result = '';


                    integerIndex = integer.length - 1;
                    patternIndex = pattern.length - 1;


                    while ((integerIndex >= 0) && (patternIndex >= 0)) {
                        var digit = integer.charAt(integerIndex);
                        integerIndex--;


                        // Skip non-digits from the source integer (eradicate current formatting).
                        if ((digit < '0') || (digit > '9')) continue;


                        // Got a digit from the integer, now plug it into the pattern.
                        while (patternIndex >= 0) {
                            var patternChar = pattern.charAt(patternIndex);
                            patternIndex--;


                            // Substitute digits for '#' chars, treat other chars literally.
                            if (digit == '.')
                                break;
                            else if (patternChar == '#') {
                                result = digit + result;
                                break;
                            }
                            else {
                                result = patternChar + result;
                            }
                        }
                    }


                    return result;
                }


                function appendDollar(id) {
                    var amount = document.getElementById(id).value;
                    var decimalval = amount.split(".")[1];
                    if (decimalval == null) decimalval = ".00"
                    else {
                        decimalval = "." + decimalval;
                        amount = amount.split(".")[0];
                    }


                    //                if (trimAll(amount) != "") {
                    //                    document.getElementById(id).value = 'Rp' + formatInteger(amount, '###,###,###,###') + decimalval;
                    //                }

                    if (trimAll(amount) != "") {
                        document.getElementById(id).value = formatInteger(amount, '###,###,###,###') + decimalval;
                    }
                }
   </script>
</body>
</html>
