﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchvendor.aspx.cs" Inherits="rpa.searchvendor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Search BTR</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />

    <script type="text/javascript">
        function UseSelected(Tripno, nik, nama, kddepar, kdjabat, kdsite, costcode, sup_nik, fstatus, superior, nmjabat, nmdepar, costname, detailtrip) {
            var pw = window.opener;
            var inputFrm = pw.document.forms['form1'];
            if (inputFrm.elements['ctrlToFind'].value == 'Originator') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_Originator'].value = nopeg;
                inputFrm.elements['ctl00_ContentPlaceHolder1_OriginatorName'].value = nama;
            } else if (inputFrm.elements['ctrlToFind'].value == 'user') {
                inputFrm.elements['nik'].value = nopeg;
                inputFrm.elements['nama'].value = nama;
                inputFrm.elements['kdcabang'].value = kdcabang;
            } else if (inputFrm.elements['ctrlToFind'].value == 'bidang') {
                inputFrm.elements['kddepar'].value = bidang_code;
            } else if (inputFrm.elements['ctrlToFind'].value == 'paentry') {
                inputFrm.elements['txtbtrno'].value = Tripno;
            } else if (inputFrm.elements['ctrlToFind'].value == 'dec') {
                inputFrm.elements['tripno'].value = Tripno;
                inputFrm.elements['nik'].value = nik;
                inputFrm.elements['nama'].value = nama;
                inputFrm.elements['kddepar'].value = kddepar;
                inputFrm.elements['nmjabat'].value = kdjabat;
                inputFrm.elements['site'].value = kdsite;
                inputFrm.elements['costcode'].value = costcode;
                inputFrm.elements['snik'].value = sup_nik;
                inputFrm.elements['fstatus'].value = fstatus;
                inputFrm.elements['dep'].value = nmdepar;
                inputFrm.elements['superior'].value = superior;
                inputFrm.elements['position'].value = nmjabat;
                inputFrm.elements['costcd'].value = costname;
                inputFrm.elements['type'].value = detailtrip;
            } else if (inputFrm.elements['ctrlToFind'].value == 'paxvendor') {
                inputFrm.elements['hvendorid'].value = Tripno;
                inputFrm.elements['txtvendor'].value = nik;
            } else {
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = nopeg;
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
            }
            window.close();
        }
        function Left(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else
                return String(str).substring(0, n);
        }

        function Right(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else {
                var iLen = String(str).length;
                return String(str).substring(iLen, iLen - n);
            }
        }

        function keyPressed(e) {
            switch (e.keyCode) {
                case 13:
                    {
                        document.getElementById("btnSearch").click();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        function cekArgumen() {
            var d = document;
            if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)) {
                alert("Please Entry Specific NIK / Name (minimal 3 Character)");
                return false;
            }
        }
    </script>

</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white form-group" style="background-color:White;">
    <form id="form1" runat="server" class="page-wrapper">
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <div class="page-content">

    <asp:TextBox ID="searchkey" runat="server" CssClass="form-control"></asp:TextBox>
    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click"/>
    <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div class="page-content">           
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center"></caption>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td>
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0" class="table table-responsive">
      <tr>
         <%--<td style="vertical-align:top"><b>Page:</b>&nbsp;</td>--%>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <%--<asp:LinkButton ID="btnPage" CommandName="Page" 
                        CommandArgument="<%#Container.DataItem %>" 
                        CssClass="text" Runat="server" 
                        Visible='<%#(CInt(Container.DataItem) <> (PageNumber+1)).ToString()%>'>
                         <%#Container.DataItem %>
                         </asp:LinkButton>--%>
                         <%--Visible='<%# (Convert.ToInt32(Container.DataItem) == (PageNumber+1)).ToString()%>' --%>
                         <%--<asp:Label ID="lblPage" runat="server" 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>--%>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td>
               <asp:Repeater ID="rptResult" runat="server">
               <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No</th>
                                <th>Vendor ID.</th>
                                <th>Vendor Name</th>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td><%#Container.ItemIndex + 1%></td>
                        <td><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "vendorid")%>','<%#DataBinder.Eval(Container.DataItem, "vendname")%>');"><%#DataBinder.Eval(Container.DataItem, "vendorid")%></a></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "vendname")%></td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
        <ProgressTemplate>
        <asp:Panel ID="pnlProgressBar" runat="server">    
            <div><asp:Image ID="Image1" runat="Server" ImageUrl="assets/pages/img/ajax-loader.gif"/>Please 
                Wait...
            </div>
        </asp:Panel>
        </ProgressTemplate> 
    </asp:UpdateProgress>    
    </div>
    </form>
</body>
</html>
