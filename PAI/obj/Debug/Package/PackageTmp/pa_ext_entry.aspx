﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pa_ext_entry.aspx.cs" Inherits="rpa.pa_ext_entry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PA External</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <script src="assets/calendar/jsm01.js" type="text/javascript"></script>
        <script src="assets/calendar/tcal.js" type="text/javascript"></script>
        <link href="assets/calendar/tcal.css" rel="stylesheet" type="text/css" />
        <script src="assets/calendar/jscal2.js" type="text/javascript"></script>
        <script src="assets/calendar/lang/en.js" type="text/javascript"></script>
        <link href="assets/calendar/jscal2.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/border-radius.css" rel="stylesheet" type="text/css" />
        <link href="assets/calendar/win2k/win2k.css" rel="stylesheet" type="text/css" />
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white form-group">
    
    <form id="form1" runat="server" class="page-wrapper">
    <input type="hidden" id="ctrlToFind" />
        <div class="page-wrapper">
             <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="home.aspx">
                            <img src="assets/layouts/layout/img/logo.jpg" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            
                            <li class="nav-item  active open">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">RPA</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item ">
                                        <a href="rpa_entry.aspx" class="nav-link ">
                                            <span class="title">Entry 
                                                <br/>Payment Aplication</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                
                                
                                    <li class="nav-item">
                                        <a href="rpa_pa_list.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Payment Aplication</span>   
                                        </a>
                                    </li>

                                    <li class="nav-item active open">
                                        <a href="pa_ext_entry.aspx" class="nav-link ">
                                            <span class="title">Entry External 
                                                <br/>Payment Request</span>   
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="pa_ext_list.aspx" class="nav-link ">
                                            <span class="title">List External 
                                                <br/>Payment Request</span>   
                                        </a>
                                    </li>

                                    <%--<li class="nav-item">
                                        <a href="dec_ca.aspx" class="nav-link ">
                                            <span class="title">Declaration  
                                                <br/>Cash Advanse Non BTR</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="declarationlist.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Declaration Non BTR</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>--%>

                                    <li class="nav-item" >
                                        <a href="reports.aspx" class="nav-link ">
                                            <span class="title">List 
                                                <br/>Reports</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="rpa_entry.aspx">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Payment Request</span>
                                </li>
                            </ul>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> EXTERNAL PA 
                            <small>Payment Request</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
                        <div class="row ">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-share font-dark"></i>
                                            <span class="caption-subject font-dark bold uppercase">EXTERNAL PAYMENT APPLICATION</span>
                                        </div>
                                        
                                        <asp:HiddenField ID="hkdlvl" runat="server" />
                                        <asp:HiddenField ID="hkddepar" runat="server" />
                                        <asp:HiddenField ID="hkkdjabat" runat="server" />
                                        <asp:HiddenField ID="hkddiv" runat="server" />
                                        <asp:HiddenField ID="hcaglacc" runat="server" />
                                        <asp:HiddenField ID="hstatuswf" runat="server" />
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Request No</label>
                                                <asp:TextBox ID="txtpano" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                <span class="help-block">External PA Number</span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Status</label>
                                                <asp:TextBox ID="txtsts" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Lokasi Site</label>
                                                <asp:DropDownList ID="ddlsite" runat="server" CssClass="form-control">
                                                    <%--<asp:ListItem Text="Head Office" Value="JRN"></asp:ListItem>
                                                    <asp:ListItem Text="Lanut" Value="LAN"></asp:ListItem>
                                                    <asp:ListItem Text="Bakan" Value="BAK"></asp:ListItem>
                                                    <asp:ListItem Text="Seruyung" Value="SER"></asp:ListItem>
                                                    <asp:ListItem Text="Penjom" Value="PEN"></asp:ListItem>--%>
                                                </asp:DropDownList>
                                                <span class="help-block">Site Location</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tanggal</label>
                                                <asp:TextBox ID="txtdtpa" runat="server" CssClass="form-control" ReadOnly="true" placeholder=""></asp:TextBox>
                                                <span class="help-block">Request Date</span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Departemen</label>
                                                <asp:DropDownList ID="ddldpt" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <span class="help-block"> Department </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Pemohon</label>
                                                <asp:Label ID="lblpemohon" runat="server" CssClass="form-control"></asp:Label>
                                                <asp:HiddenField ID="hnikpemohon" runat="server" />
                                                <span class="help-block">Requestor</span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Vendor</label>
                                                <img alt="add" id="ImgSv" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupvendor('paxvendor')" align="absmiddle" />
                                                <asp:TextBox ID="txtvendor" runat="server" CssClass="form-control" placeholder="" Enabled="true"></asp:TextBox>
                                                <asp:HiddenField ID="hvendorid" runat="server" />
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Request Type</label>
                                                 <asp:DropDownList ID="ddlreqtyp" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="Payment" Value="Payment"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <asp:TextBox ID="txtremarksh" runat="server" CssClass="form-control" placeholder="" TextMode="MultiLine" Columns="8" Rows="3"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <h3 class="form-section">*O/S Payment</h3>

                                    <div id="detailpayment">
                                        <asp:Table ID="tblcellphone" runat="server" CssClass="table table-responsive">
                                            <asp:TableRow>
                                                <asp:TableHeaderCell>Doc Type</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Document No</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Vendor Currency</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Vendor Amount</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Payment Type</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Payment Amount</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Cost Center</asp:TableHeaderCell>
                                                <asp:TableHeaderCell> </asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Remarks</asp:TableHeaderCell>
                                                <asp:TableHeaderCell> </asp:TableHeaderCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr1" Value="1" runat="server"/> <asp:TextBox ID="txtdoctype" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                
                                                <asp:DropDownList ID="ddlcurbayar" runat="server" CssClass="form-control"
                                                     AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                <asp:TextBox ID="txtcostcenter" runat="server" CssClass="form-control"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img1" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode1')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px"  Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                <i id="del1" class="fa fa-trash form-control" onclick="cleartext(1)" style="cursor:pointer;"></i>
                                                    
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr2" Value="2" runat="server"/> <asp:TextBox ID="txtdoctype2" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtdocno2" runat="server" CssClass="form-control"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                <asp:DropDownList ID="ddlcurbayar2" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>

                                                <asp:TableCell><asp:TextBox ID="txtvendoramt2" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp2" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt2" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter2" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img2" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode2')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks2" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I1" class="fa fa-trash form-control" onclick="cleartext(2)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr3" Value="3" runat="server"/> <asp:TextBox ID="txtdoctype3" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno3" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar3" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt3" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp3" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt3" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter3" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img3" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode3')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks3" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I2" class="fa fa-trash form-control" onclick="cleartext(3)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr4" Value="4" runat="server"/> <asp:TextBox ID="txtdoctype4" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno4" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar4" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt4" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp4" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt4" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter4" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img4" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode4')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks4" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I3" class="fa fa-trash form-control" onclick="cleartext(4)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr5" Value="5" runat="server"/> <asp:TextBox ID="txtdoctype5" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno5" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar5" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt5" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp5" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt5" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter5" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img5" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode5')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks5" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I4" class="fa fa-trash form-control" onclick="cleartext(5)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow> 
                                            
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr6" Value="6" runat="server"/> <asp:TextBox ID="txtdoctype6" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno6" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar6" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt6" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp6" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt6" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter6" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img6" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode6')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks6" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I5" class="fa fa-trash form-control" onclick="cleartext(6)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>  

                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr7" Value="7" runat="server"/> <asp:TextBox ID="txtdoctype7" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno7" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar7" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt7" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp7" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt7" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter7" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img7" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode7')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks7" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I6" class="fa fa-trash form-control" onclick="cleartext(7)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr8" Value="8" runat="server"/> <asp:TextBox ID="txtdoctype8" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno8" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar8" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt8" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp8" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt8" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter8" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img8" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode8')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks8" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I7" class="fa fa-trash form-control" onclick="cleartext(8)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>  

                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr9" Value="9" runat="server"/> <asp:TextBox ID="txtdoctype9" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno9" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar9" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt9" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp9" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt9" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter9" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img9" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode9')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks9" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I8" class="fa fa-trash form-control" onclick="cleartext(9)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>  
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hr10" Value="10" runat="server"/> <asp:TextBox ID="txtdoctype10" runat="server" Width="80px" CssClass="form-control" Enabled="false">Invoice</asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocno10" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlcurbayar10" runat="server" CssClass="form-control"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtvendoramt10" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlpmttyp10" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Full" Value="Full"></asp:ListItem>
                                                        <asp:ListItem Text="Partial" Value="Partial"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtpmtamt10" runat="server" CssClass="form-control" onkeypress="return onlyNumbers(event);" onblur="appendDollar(this.id);"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtcostcenter10" runat="server" CssClass="form-control"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell>
                                                    <img alt="add" id="Img10" src="assets/pages/img/Search.gif" style="cursor: pointer" onclick="OpenPopupCCenter('ccode10')" align="absmiddle" />
                                                </asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtremarks10" runat="server" CssClass="form-control" TextMode="MultiLine" style="height:35px;width:200px" Rows="1" Columns="10"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I9" class="fa fa-trash form-control" onclick="cleartext(10)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>  
                                        </asp:Table>
                                    </div>
                                    
                                    <h3 class="form-section">*Document Supporting</h3>
                                    <h4 class="portlet-body font-red">Please make sure document exist in document center and reviewer has access on this document</h4>
                                    <div class="portlet-body">
                                        <div class="form-inline" role="form">
                                            <asp:Table ID="tbldoc" runat="server" CssClass="table table-responsive">
                                            <asp:TableRow>
                                                <asp:TableHeaderCell>Document Name</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Document URL(Document Center)</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Remarks</asp:TableHeaderCell>
                                                <asp:TableHeaderCell> </asp:TableHeaderCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc" Value="1" runat="server"/><asp:TextBox ID="txtdocname" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I10" class="fa fa-trash form-control" onclick="cleartextdoc(1)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc2" Value="2" runat="server"/><asp:TextBox ID="txtdocname2" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl2" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks2" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I11" class="fa fa-trash form-control" onclick="cleartextdoc(2)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc3" Value="3" runat="server"/><asp:TextBox ID="txtdocname3" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl3" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks3" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I12" class="fa fa-trash form-control" onclick="cleartextdoc(3)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc4" Value="4" runat="server"/><asp:TextBox ID="txtdocname4" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl4" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks4" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I13" class="fa fa-trash form-control" onclick="cleartextdoc(4)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc5" Value="5" runat="server"/><asp:TextBox ID="txtdocname5" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl5" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks5" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I14" class="fa fa-trash form-control" onclick="cleartextdoc(5)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc6" Value="6" runat="server"/><asp:TextBox ID="txtdocname6" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl6" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks6" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I15" class="fa fa-trash form-control" onclick="cleartextdoc(6)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc7" Value="7" runat="server"/><asp:TextBox ID="txtdocname7" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl7" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks7" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I16" class="fa fa-trash form-control" onclick="cleartextdoc(7)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc8" Value="8" runat="server"/><asp:TextBox ID="txtdocname8" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl8" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks8" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I17" class="fa fa-trash form-control" onclick="cleartextdoc(8)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc9" Value="9" runat="server"/><asp:TextBox ID="txtdocname9" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl9" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks9" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I18" class="fa fa-trash form-control" onclick="cleartextdoc(9)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                             <asp:TableRow>
                                                <asp:TableCell><asp:HiddenField ID="hrdoc10" Value="10" runat="server"/><asp:TextBox ID="txtdocname10" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="txtdocurl10" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell><asp:TextBox ID="docremarks10" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="1" Columns="40"></asp:TextBox></asp:TableCell>
                                                <asp:TableCell> 
                                                    <i id="I19" class="fa fa-trash form-control" onclick="cleartextdoc(10)" style="cursor:pointer;"></i>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            </asp:Table>
                                            <br />
                                            <asp:Label ID="lblMessage" class="sr-only" runat="server" />
                                        </div>
                                    </div>


                                    <div class="portlet-body">
                                        <hr />
                                        <div class="form-inline" role="form">
                                            
                                            
                                            <asp:Button ID="btnsave" class="btn blue" runat="server" Text="Save" onclick="btnsave_Click" />
                                            <asp:Button ID="btnsubmit" name="btnsubmit" class="btn blue" runat="server" Text="Submit" 
                                                onclick="btnsubmit_Click" />
                                            <button id="btncancel" class="btn blue" type="button" onclick ="if (confirm('Your changes will discarded. Are you sure to continue?')) window.location.href='pa_ext_list.aspx';">Cancel</button>
                                            
                                            <asp:HiddenField ID="hpano" runat="server" />
                                            <asp:HiddenField ID="hfsize" runat="server" />


                                        </div>
                        
                                    </div>


                                    <h3 class="form-section">History Approval</h3>
                                    <hr />
                                    <div class="portlet-body">
                                       
                                        <div class="form-inline" role="form">
                                            <asp:GridView ID="grdpahist" runat="server" CssClass="table table-striped table-hover table-bordered">
                                            
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->
                                
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                                
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
            </div>
            <!-- END CONTAINER -->
            
            </div>
    </form>
    <%=msg %>

    <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <script src="assets/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

<script type="text/javascript">
    function OpenPopupvendor(key) {
        comp = document.getElementById("ddlsite").value
        document.getElementById("ctrlToFind").value = key;
        window.open("searchvendor.aspx?comp="+comp, "List", "scrollbars=yes,resizable=yes,width=1000,height=400");
        return false;
    }
    function OpenPopupCCenter(key) {
        comp = document.getElementById("ddlsite").value
        document.getElementById("ctrlToFind").value = key;
        window.open("searchcostcenter.aspx?comp="+comp, "List", "scrollbars=yes,resizable=yes,width=1000,height=400");
        return false;
    }

    function cleartext(key) {
        if (key > 1 && key <= 10) {
            document.getElementById("txtdocno" + key).value = "";
            document.getElementById("txtvendoramt" + key).value = "";
            document.getElementById("txtpmtamt" + key).value = "";
            document.getElementById("txtcostcenter" + key).value = "";
            document.getElementById("txtremarks" + key).value = "";
        }
        else {
            document.getElementById("txtdocno").value = "";
            document.getElementById("txtvendoramt").value = "";
            document.getElementById("txtpmtamt").value = "";
            document.getElementById("txtcostcenter").value = "";
            document.getElementById("txtremarks").value = "";
        }
    }

    function cleartextdoc(key) {
        if (key > 1 && key <= 10) {
            document.getElementById("txtdocname" + key).value = "";
            document.getElementById("txtdocurl" + key).value = "";
            document.getElementById("docremarks" + key).value = "";
            
        }
        else {
            document.getElementById("txtdocname").value = "";
            document.getElementById("txtdocurl").value = "";
            document.getElementById("docremarks").value = "";
            
        }
    }

    function onlyNumbers(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
            return false;


        return true;
    }

    function confirm_meth() {
        if (confirm("Your changes will discarded. Are you sure want to continue?") == true) {
            window.location = "http://localhost:33010/pa_ext_list.aspx";
        }
        else {

        }
    }

    function appendDollar(id) {
        var amount = document.getElementById(id).value;
        var decimalval = amount.split(".")[1];
        if (decimalval == null) decimalval = ".00"
        else {
            decimalval = "." + decimalval;
            amount = amount.split(".")[0];
        }


        //                if (trimAll(amount) != "") {
        //                    document.getElementById(id).value = 'Rp' + formatInteger(amount, '###,###,###,###') + decimalval;
        //                }

        if (trimAll(amount) != "") {
            document.getElementById(id).value = formatInteger(amount, '###,###,###,###') + decimalval;
        }
    }

    function trimAll(sString) {
        while (sString.substring(0, 1) == ' ') {
            sString = sString.substring(1, sString.length);
        }
        while (sString.substring(sString.length - 1, sString.length) == ' ') {
            sString = sString.substring(0, sString.length - 1);
        }
        return sString;
    }

    function formatInteger(integer, pattern) {
        var result = '';


        integerIndex = integer.length - 1;
        patternIndex = pattern.length - 1;


        while ((integerIndex >= 0) && (patternIndex >= 0)) {
            var digit = integer.charAt(integerIndex);
            integerIndex--;


            // Skip non-digits from the source integer (eradicate current formatting).
            if ((digit < '0') || (digit > '9')) continue;


            // Got a digit from the integer, now plug it into the pattern.
            while (patternIndex >= 0) {
                var patternChar = pattern.charAt(patternIndex);
                patternIndex--;


                // Substitute digits for '#' chars, treat other chars literally.
                if (digit == '.')
                    break;
                else if (patternChar == '#') {
                    result = digit + result;
                    break;
                }
                else {
                    result = patternChar + result;
                }
            }
        }


        return result;
    }  
</script>
    
</body>


</html>

