﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace rpa
{
    public partial class pa_history : System.Web.UI.Page
    {
        cbll bll = new cbll();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["sNiksite"] == null)
            //{
            //    Response.Redirect("login.aspx");
            //}

            //Session["pa_no"] = "105";

            List<cArrayList> arr2 = new List<cArrayList>();
            arr2.Add(new cArrayList("@DOC", Session["pa_no"].ToString()));
            DataTable dta = new DataTable();
            bll.vGetPAHist(ref dta, arr2);

            GridView1.DataSource = dta;
            GridView1.DataBind();
        }
    }
}