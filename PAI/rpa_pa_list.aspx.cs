﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;

namespace rpa
{
    public partial class rpa_pa_list : System.Web.UI.Page
    {
        cbll bll = new cbll();
        string sNik = "";
        string sNiksite = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            sNik = HttpContext.Current.Session["sNik"].ToString();
            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();
            if (sNik=="")
            {
                Response.Redirect("login.aspx");
            }

            if (IsPostBack == false)
            {
                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@nik", sNiksite));
                DataTable dta = new DataTable();
                bll.vGetListPA(ref dta, arr);

                GridView1.DataSource = dta;
                GridView1.DataBind();

                dta = new DataTable();
                bll.vGetCompListPA(ref dta, arr);
                if (dta.Rows.Count > 0)
                {
                    ddlcomplist.DataSource = dta;
                    ddlcomplist.DataTextField = "comp_id";
                    ddlcomplist.DataValueField = "comp_id";
                    ddlcomplist.DataBind();
                }

                dta = new DataTable();
                bll.vGetDivListPA(ref dta, arr);
                if (dta.Rows.Count > 0)
                {
                    ddldivlist.DataSource = dta;
                    ddldivlist.DataTextField = "div_id";
                    ddldivlist.DataValueField = "div_id";
                    ddldivlist.DataBind();
                }
            }
        }

        //Onclick Submit Button
        [WebMethod(EnableSession = true)]
        //[System.Web.Services.WebMethod(EnableSession = true)]

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cd.iPano = Convert.ToString(GridView1.SelectedRow.Cells[1].Text);
            Session["pa_no"] = Convert.ToString(GridView1.SelectedRow.Cells[2].Text);
            Session["padt"] = Convert.ToString(GridView1.SelectedRow.Cells[7].Text);
            Session["PaAmt"] = Convert.ToString(GridView1.SelectedRow.Cells[9].Text);
            if (Convert.ToString(GridView1.SelectedRow.Cells[8].Text) == "CP")
            {
                Session["PaReasn"] = "CELLPHONE";
            }

            if (Convert.ToString(GridView1.SelectedRow.Cells[8].Text) == "CA")
            {
                Session["PaReasn"] = "CASH ADVANCE";
            }

            if (Convert.ToString(GridView1.SelectedRow.Cells[8].Text) == "RI")
            {
                Session["PaReasn"] = "REIMBURSEMENT";
            }
            //cd.sPaDt = GridView1.SelectedRow.Cells[2].Text;
            //cd.sPaAmt = GridView1.SelectedRow.Cells[8].Text;
            //cd.sPaReasn = GridView1.SelectedRow.Cells[4].Text;
            //cd.bListPa = true;
            Session["bListPa"] = true;
            Response.Redirect("rpa_entry.aspx");

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("rpa_pa_list.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            List<cArrayList> arr = new List<cArrayList>();
            arr.Add(new cArrayList("@nama", txtnm.Text));
            arr.Add(new cArrayList("@comp", ddlcomplist.SelectedValue.ToString()));
            arr.Add(new cArrayList("@div", ddldivlist.SelectedValue.ToString()));
            arr.Add(new cArrayList("@usage", (ddlusagetyplist.SelectedValue.ToString() == "all") ? null : ddlusagetyplist.SelectedValue.ToString()));
            arr.Add(new cArrayList("@reqsts", ddlreqstslist.SelectedValue.ToString()));
            arr.Add(new cArrayList("@nik", sNiksite));
            
            DataTable dta = new DataTable();
            bll.vGetListPASearch(ref dta, arr);

            GridView1.DataSource = dta;
            GridView1.DataBind();
        }

        protected void btnNewReq_Click(object sender, EventArgs e)
        {
            Response.Redirect("rpa_entry.aspx");
        }

        protected void btnPrintList_Click(object sender, EventArgs e)
        {
            Response.Redirect("print_palist.aspx");
        }

        protected void gridViewPAList(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    var hyperLink = e.Row.FindControl("lkpahistory") as HyperLink;
            //    if (hyperLink != null)
            //    {
                    //Session["pa_no"] = DataBinder.Eval(e.Row.DataItem, "pa#").ToString();
                    //hyperLink.Attributes.Add("onclick", "javascript:window.open('pa_history.aspx','','height=500,width=950,scrollbars');");
                    //hyperLink.NavigateUrl = "pa_history.aspx";
               // }
                    //hyperLink.NavigateUrl = CreateShowActionLogUrl(e.Row) + "?id=" + DataBinder.Eval(e.Row.DataItem, "pa#");
            //}
        }

        protected void passdata(object sender, EventArgs e)
        {
            var linkButton = (sender as LinkButton);
            var tableName = linkButton.Attributes["data-tablename"];
            Session["pa_no"] = tableName;
            //window.open("pa_history.aspx", "_blank", "menubar=0,width=100,height=100");
            //Response.Redirect("pa_history.aspx");
            ResponseHelper.Redirect("pa_history.aspx", "_blank", "menubar=0,width=800,height=700");
        } 
    }

    public static class ResponseHelper
    {
        public static void Redirect(string url, string target, string windowFeatures)
        {
            HttpContext context = HttpContext.Current;

            if ((String.IsNullOrEmpty(target) ||
                target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
                String.IsNullOrEmpty(windowFeatures))
            {

                context.Response.Redirect(url);
            }
            else
            {
                Page page = (Page)context.Handler;
                if (page == null)
                {
                    throw new InvalidOperationException(
                        "Cannot redirect to new window outside Page context.");
                }
                url = page.ResolveClientUrl(url);

                string script;
                if (!String.IsNullOrEmpty(windowFeatures))
                {
                    script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
                }
                else
                {
                    script = @"window.open(""{0}"", ""{1}"");";
                }

                script = String.Format(script, url, target, windowFeatures);
                ScriptManager.RegisterStartupScript(page,
                    typeof(Page),
                    "Redirect",
                    script,
                    true);
            }
        }
    }
}