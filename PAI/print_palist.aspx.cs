﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace rpa
{
    public partial class print_palist : System.Web.UI.Page
    {
        laporan.ReportLogic rptlogic = new laporan.ReportLogic();
        string sNik = "";
        string sNiksite = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }
            sNik = HttpContext.Current.Session["sNik"].ToString();
            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();
            if (IsPostBack == false)
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/laporan/rpt_pa_list.rdlc");

                DataTable table1 = rptlogic.GetPAList(sNiksite);
                DataSet dspa = new DataSet("pa_list");
                dspa.Tables.Add(table1);

                ReportDataSource datasource = new ReportDataSource("pa_list", dspa.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
        }
    }
}