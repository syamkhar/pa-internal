﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace rpa
{
    public class getfile
    {
        public int Id { set; get; }
        public string FileName { set; get; }

        public DataSet GetFileFromDB()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter();

            using (SqlConnection conn = new SqlConnection("Data Source=20.110.1.4;" +
                             "Initial Catalog=JRN_RPA;User ID=dev;Password=hrishris"))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_get_fupload";
                cmd.Connection = conn;

                cmd.Parameters.AddWithValue("@pa_no", 0);

                try
                {
                    conn.Open();

                    sda.SelectCommand = cmd;
                    sda.Fill(ds);

                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    sda.Dispose();
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        }
    }
}