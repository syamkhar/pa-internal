﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace rpa.admin
{
    public partial class admin_signin : System.Web.UI.Page
    {
        string sNIK;
        public string msg;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            cbll bll = new cbll();
            
            int idxdomain = txtusrnm.Text.IndexOf("\\");
            string dm = "";
            string nm = "";
            int cnt = 0;

            if (idxdomain > 0)
            {
                cnt = txtusrnm.Text.Length;
                dm = txtusrnm.Text.Substring(0, idxdomain);
                nm = txtusrnm.Text.Substring(idxdomain, cnt - idxdomain);
                nm = nm.Replace("\\", "");

            }

            if (txtnikuser.Text == "")
            {
                msg = "<script type='text/javascript'> alert('Please fill NIK User'); </script>";
                return;
            }

            //sNIK = "JRN0152";
            //sNIK = "JRN0101";
            //sNIK = "JRN0164"
            sNIK = bll.sGetEmpIDFromAD(dm, nm, txtpwd.Text);
            if (sNIK == "NOT VALID USER" || sNIK == "NOT VALID PCS USER" || sNIK == "Object reference not set to an instance of an object.")
            {
                //nCount++;
                //MessageBox.Show("Wrong password or user name !", "Please try again ");
            }
            else
            {
                //string snm = bll.sGetFullName(sNIK);
                //MessageBox.Show(this, "Welcome " + snm + " in Pit Control System ..", "LOGIN SUCCESSFULL", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);



                if (sNIK == "Index was out of range. Must be non-negative and less than the size of the collection.\r\nParameter name: index")
                {

                }
                sNIK = txtnikuser.Text;
                Session["sNiksite"] = sNIK;
                Session["sNik"] = bll.sExecuteSQL("select nik from jrn_test..h_A101 where stedit = 0 and active=1 and niksite = '" + Session["sNiksite"].ToString() + "'");
                Session["sNama"] = bll.sExecuteSQL("select nama from jrn_test..h_A101 where stedit = 0 and active=1 and niksite = '" + Session["sNiksite"].ToString() + "'");
                Session["bListPa"] = false;
                Session["NikObh"] = "";

                Response.Redirect("../rpa_entry.aspx");
            }
        }
    }
}