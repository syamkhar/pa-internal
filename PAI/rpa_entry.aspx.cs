﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.IO;
using System.Web.SessionState;
using System.Configuration;

namespace rpa
{
    
    public partial class rpa_entry : System.Web.UI.Page
    {
        cbll bll = new cbll();
        string sNik = "";
        public static DataTable dtaaddcat = new DataTable();
        public string msg;
        public string csscp;
        public string cssca;
        public string cssdiscard;
        public string csscanew;
        private int gveditindex = 0;
        private string rateusd = "";
        //private string rateaccpac = "";
        public static string btrnumber = "";
        string sNiksite = "";
        string sNama = "";
        string ServerUpload = ConfigurationManager.AppSettings["FileServer"]; 

        [WebMethod(EnableSession = true)]
        //[System.Web.Services.WebMethod(EnableSession = true)]

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNiksite"] == null)
            {
                Response.Redirect("login.aspx");
            }

            //string.Format("$ {0:#,##0.00}", double.Parse(txtdumpGLAccount.Text));
            
            //if (sNik == "")
            //{
            //    Response.Redirect("login.aspx");
            //}

            sNiksite = HttpContext.Current.Session["sNiksite"].ToString();
            sNik = HttpContext.Current.Session["sNik"].ToString();
            sNama = HttpContext.Current.Session["sNama"].ToString();
            //string fuploadloc = ServerUpload;

            if (!IsPostBack)
                txtbtrno.Attributes.Add("readonly", "readonly");

            //List<cArrayList> arramt = new List<cArrayList>();
            //arramt = new List<cArrayList>();
            //arramt.Add(new cArrayList("@niksite", Session["sNiksite"].ToString()));
            //DataTable dtaAMT = new DataTable();
            //bll.vGetAmtSettle(ref dtaAMT, arramt);

            //if (dtaAMT.Rows.Count > 0)
            //{
            //    if (Convert.ToDecimal(dtaAMT.Rows[0]["Amount_Settlement"].ToString()) > 0)
            //    {
            //        rblpatyp.Items[1].Enabled = false;
            //        rblpatyp.Items[2].Enabled = false;
            //    }
            //}

            if (IsPostBack==false)
            {
                txtsts.Text = "Draft";
                txtdtpa.Text = DateTime.Now.ToString("yyyy-MM-dd h:mm tt");
                rblpatyp.SelectedValue = "Reimburshment";
                csscp = "style='display:none;'";
                cssca = "style='display:none;'";
                csscanew = "style='display:none;'";
                //txtbtrno.Visible = false;
                btnprintpa.Visible = false;
                btnprintcp.Visible = false;
                btnUpload.Enabled = false;
                btnprint.Visible = false;
                txttotalamt.Enabled = false;
                btnsubmit.Enabled = false;
                fuploadPA.Enabled = false;

                dtaaddcat = bll.FetchReimbursh();

                lblpemohon.Text = sNama;
                txtdt.Text = DateTime.Now.ToString("yyyy-MM-dd h:mm tt");
                Session["myDatatable"] = dtaaddcat;
                fillgrid();

                //onbehalf
                List<cArrayList> arrobehalf = new List<cArrayList>();
                arrobehalf.Add(new cArrayList("@nik", sNik));
                DataTable dtobehalf = new DataTable();
                bll.vGetOnBehalf(ref dtobehalf,arrobehalf);
                if (dtobehalf.Rows.Count > 0)
                {
                    for (int i = 0; i < dtobehalf.Rows.Count; i++)
                    {
                        ddlobehalf.Items.Add(new ListItem(dtobehalf.Rows[i]["nama"].ToString(), dtobehalf.Rows[i]["niksite"].ToString()));
                    }
                }

                chkCP();

                DataTable dtCAGL = new DataTable();
                bll.vgetGLCA(ref dtCAGL);
                if (dtCAGL.Rows.Count > 0)
                {
                    hcaglacc.Value = dtCAGL.Rows[0]["coa"].ToString();
                }

                SqlDataReader rs = null;
                List<cArrayList> arr = new List<cArrayList>();

                rs = null;

                DataTable dtacurr = new DataTable();
                bll.vGetListCurr(ref dtacurr);

                if (dtacurr.Rows.Count > 0)
                {
                    ddlcurbayar.DataSource = dtacurr;
                    ddlcurbayar.DataTextField = "currtyp";
                    ddlcurbayar.DataValueField = "currtyp";
                    ddlcurbayar.DataBind();
                    ddlcurbayar.SelectedValue = "IDR";
                }
                txtcurrraterp.Text = "1";

                if (Convert.ToBoolean(Session["bListPa"].ToString()) == true)
                {
                    rblpatyp.Enabled = false;

                    arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@pa_no", Session["pa_no"].ToString()));
                    DataTable dtah = new DataTable();
                    bll.vGetPAList(ref dtah, arr);

                    if (dtah.Rows.Count > 0)
                    {
                        ddlobehalf.SelectedValue = dtah.Rows[0]["pa_to"].ToString();
                        txttotalamt.Text = dtah.Rows[0]["amount"].ToString();
                        txtdt.Text = dtah.Rows[0]["dt"].ToString();
                        txtreason.Text = dtah.Rows[0]["reason"].ToString();
                        hpano.Value = Session["pa_no"].ToString();
                        txtpano.Text = hpano.Value;
                        txtsts.Text = dtah.Rows[0]["request_sts"].ToString();
                        txtcomp.Text = dtah.Rows[0]["comp_id"].ToString();
                        txtdtpa.Text = dtah.Rows[0]["dt"].ToString();
                        txtlokasi.Text = dtah.Rows[0]["loc_id"].ToString();
                        //txtlokasi.Text = dtah.Rows[0]["divisi"].ToString();
                        txtkepada.Text = dtah.Rows[0]["pa_to"].ToString();
                        txtkdbiaya.Text = dtah.Rows[0]["ccenter"].ToString();
                        txtkdbiaya.Text = dtah.Rows[0]["ccenter"].ToString();
                        txtbtrno.Text = dtah.Rows[0]["noBTR"].ToString();
                        txtpaidamt.Text = dtah.Rows[0]["amt_paid"].ToString();
                        ddlcurbayar.SelectedValue = dtah.Rows[0]["curr_typ"].ToString();
                        txttotalamt.Text = dtah.Rows[0]["amount"].ToString();
                        txtcurrraterp.Text = dtah.Rows[0]["curr_rate_accpac"].ToString();

                        if (dtah.Rows[0]["payment_typ"].ToString() == "cash")
                        {
                            chbcash.Checked = true;
                        }

                        if (dtah.Rows[0]["payment_typ"].ToString() == "transfer")
                        {
                            chbtransfer.Checked = true;
                        }

                        if (txtsts.Text == "Draft")
                        {
                            btnsubmit.Enabled = true;
                            fuploadPA.Enabled = true;
                        }

                        if (dtah.Rows[0]["usage_typ"].ToString() == "CP")
                        {
                            rblpatyp.SelectedValue = "CellPhone";
                            GridView1.Visible = false;
                            btnprintcp.Visible = true;
                            csscp = "style='display:block;'";

                            DataTable dtad = new DataTable();
                            bll.vGetPAListd(ref dtad, arr);

                            if (dtad.Rows.Count > 0)
                            {
                                txtdtperiod.Text = dtad.Rows[0]["period_dt"].ToString();
                                txtphonebillamt.Text = dtad.Rows[0]["bill_amt"].ToString();
                                txtphonepersonaluse.Text = dtad.Rows[0]["personal_amt"].ToString();
                                txtphoneclaimamount.Text = dtad.Rows[0]["detail_amt"].ToString();
                                txtphoneket.Text = dtad.Rows[0]["remarks"].ToString();
                            }
                        }
                        else
                        {
                            if (dtah.Rows[0]["usage_typ"].ToString() == "RI")
                            {
                                rblpatyp.SelectedValue = "Reimburshment";
                            }
                            if (dtah.Rows[0]["usage_typ"].ToString() == "CA")
                            {
                                if (dtah.Rows[0]["noBTR"].ToString() != "" && dtah.Rows[0]["noBTR"].ToString() != null)
                                {
                                    cssca = "style='display:block;'";
                                    csscanew = "style='display:block;'";
                                    rblpatyp.SelectedValue = "CashAdvancebtr";
                                }
                                else
                                {
                                    rblpatyp.SelectedValue = "CashAdvance";
                                }
                            }
                            if (dtah.Rows[0]["usage_typ"].ToString() == "CA")
                            {
                                GridView1.Visible = false;
                                btnprintcp.Visible = true;
                                csscp = "style='display:none;'";
                                csscanew = "style='display:block;'";

                                DataTable dtad = new DataTable();
                                bll.vGetPAListd(ref dtad, arr);

                                if (dtad.Rows.Count > 0)
                                {
                                    //txtdtperiod.Text = dtad.Rows[0]["period_dt"].ToString();
                                    txtamountca.Text = dtad.Rows[0]["detail_amt"].ToString();
                                    //txtphonepersonaluse.Text = dtad.Rows[0]["personal_amt"].ToString();
                                    //txtphoneclaimamount.Text = dtad.Rows[0]["detail_amt"].ToString();
                                    txtremarksca.Text = dtad.Rows[0]["remarks"].ToString();
                                }
                            }
                            else
                            {
                                GridView1.Visible = true;
                                btnprintcp.Visible = false;
                                btnprintpa.Visible = true;
                                csscp = "style='display:none;'";
                                csscanew = "style='display:none;'";
                                DataTable dtad = new DataTable();
                                bll.vGetPAListd2(ref dtad, arr);

                                dtaaddcat = new DataTable();

                                dtaaddcat = dtad;

                                GridView1.DataSource = dtaaddcat;
                                GridView1.DataBind();
                                fillddlcat("ddlcatlistnew");

                                foreach (System.Data.DataColumn col in dtaaddcat.Columns) col.ReadOnly = false;
                                var result = dtaaddcat.AsEnumerable()
                                .Sum(x => Convert.ToInt32(x["amt"]));

                                txttotalamt.Text = result.ToString();
                            }
                        }

                        if (Session["PaReasn"].ToString() == "CASH ADVANCE")
                        {
                            if (txtbtrno.Text != "")
                            {
                                Session["PaReasn"] = "CASH ADVANCE BTR";
                            }
                            else
                            {
                                Session["PaReasn"] = "CASH ADVANCE OPERATIONAL";
                            }
                        }
                    }
                    else
                    {

                    }
                    Session["bListPa"] = false;
                    List<cArrayList> arr2 = new List<cArrayList>();
                    arr2.Add(new cArrayList("@pa_no", Session["pa_no"].ToString()));
                    DataTable dta = new DataTable();
                    bll.vGetListfupld(ref dta, arr2);

                    gvfupld.DataSource = dta;
                    gvfupld.DataBind();
                    btnUpload.Enabled = true;

                    int index = txtpaidamt.Text.IndexOf(".");

                    if (index > 0)
                    {
                        txtpaidamt.Text = txtpaidamt.Text.Substring(0, index);
                        txtpaidamt.Text = txtpaidamt.Text.Replace(",", "");
                    }
                    checktxtsts();
                }

                arr = new List<cArrayList>();
                arr.Add(new cArrayList("@nik", ddlobehalf.SelectedValue.ToString()));
                bll.vGetEmpAcc(ref rs, arr);

                while (rs.Read())
                {
                    //ListViewItem li = lvw.Items.Add(rs["polygon_id"].ToString());
                    //txtnik.Text = rs["NikSite"].ToString();
                    txtbanknm.Text = rs["NameBank"].ToString();
                    txtaccno.Text = rs["NoRekening"].ToString();
                    txtcomp.Text = rs["KdCompany"].ToString();
                    txtlokasi.Text = rs["Kdsite"].ToString();
                    txtdivisi.Text = rs["Divisi"].ToString();
                    txtkepada.Text = rs["Nama"].ToString();
                    txtkdbiaya.Text = rs["CostCenter"].ToString();
                    txtbankaddress.Text = rs["AddressBank"].ToString();
                    txtpenerima.Text = rs["NameRekening"].ToString();
                    txtcabang.Text = rs["BankBranch"].ToString();
                    hkdlvl.Value = rs["KdLevel"].ToString();
                    hkddepar.Value = rs["Kddepar"].ToString();
                    hkkdjabat.Value = rs["KdJabat"].ToString();
                    hkddiv.Value = rs["KdDivisi"].ToString();
                }
                rs.Close();

                
            }
            checktxtsts();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (rblpatyp.SelectedValue.ToString() == "CashAdvance" || rblpatyp.SelectedValue.ToString() == "CashAdvancebtr")
            {
                List<cArrayList> arramt = new List<cArrayList>();
                arramt = new List<cArrayList>();
                arramt.Add(new cArrayList("@niksite", ddlobehalf.SelectedValue.ToString()));
                DataTable dtaAMT = new DataTable();
                bll.vGetAmtSettle(ref dtaAMT, arramt);

                if (dtaAMT.Rows.Count > 0)
                {
                    //if ((Convert.ToDecimal(dtaAMT.Rows[0]["cntbtrpa"].ToString())) > (Convert.ToDecimal(dtaAMT.Rows[0]["cntbtrhist"].ToString())))
                    //{
                    //    msg = "<script type='text/javascript'> alert('You have submited Cash Advance in Progress, the process cannot continue'); </script>";
                    //    return;
                    //}

                    //if ((Convert.ToDecimal(dtaAMT.Rows[0]["amt_paidbtr"].ToString())) > (Convert.ToDecimal(dtaAMT.Rows[0]["amt_decbtr"].ToString())))
                    //{
                    //    msg = "<script type='text/javascript'> alert('You have pending Cash advance required to be declare, the process cannot continue'); </script>";
                    //    return;
                    //}
                    if (dtaAMT.Rows[0]["pa_notcomplt"].ToString() != "")
                    {
                        msg = "<script type='text/javascript'> alert('You have submited Cash Advance #" + dtaAMT.Rows[0]["pa_notcomplt"].ToString() + " in Progress, the process cannot continue'); </script>";
                        checkalokasi();
                        return;
                    }

                    if (dtaAMT.Rows[0]["chkpavsdec_nonbtr_exist"].ToString() != "")
                    {
                        msg = "<script type='text/javascript'> alert('Please Complete Declaration For PA Number #" + dtaAMT.Rows[0]["chkpavsdec_nonbtr_exist"].ToString() + "'); </script>";
                        checkalokasi();
                        return;
                    }

                    if (dtaAMT.Rows[0]["chkpavsdec_btr_exist"].ToString() != "")
                    {
                        msg = "<script type='text/javascript'> alert('Please Complete Declaration For PA Number #" + dtaAMT.Rows[0]["chkpavsdec_nonbtr_exist"].ToString() + "'); </script>";
                        checkalokasi();
                        return;
                    }

                    if (dtaAMT.Rows[0]["chkpavsdec_nonbtr_apprv_exist"].ToString() != "")
                    {
                        msg = "<script type='text/javascript'> alert('You have Declaration In Progress PA Number #" + dtaAMT.Rows[0]["chkpavsdec_nonbtr_exist"].ToString() + "'); </script>";
                        checkalokasi();
                        return;
                    }

                    if (dtaAMT.Rows[0]["chkpavsdec_btr_apprv_exist"].ToString() != "")
                    {
                        msg = "<script type='text/javascript'> alert('You have Declaration In Progress PA Number #" + dtaAMT.Rows[0]["chkpavsdec_btr_apprv_exist"].ToString() + "'); </script>";
                        checkalokasi();
                        return;
                    }

                    if (dtaAMT.Rows[0]["pa_balance"].ToString() != "")
                    {
                        msg = "<script type='text/javascript'> alert('You have amount settllement'); </script>";
                        checkalokasi();
                        return;
                    }
                }
            }

            if (txtsts.Text == "Draft")
            {
                if (rblpatyp.SelectedValue.ToString() == "CashAdvancebtr")
                {
                    if (txtbtrno.Text == "")
                    {
                        msg = "<script type='text/javascript'> alert('BTR Number Cannot Empty'); </script>";
                        checkalokasi();
                        return;
                    }
                }

                List<cArrayList> arr = new List<cArrayList>();
                string val = "";
                string vald = "";
                int icptot = 0;
                var result = 0;
                int icatot = 0;

                if (ddlcurbayar.SelectedValue.ToString() == "IDR")
                {
                    rateusd = "0.0000611808";
                }
                else { rateusd = "1"; }

                if (rblpatyp.SelectedValue.ToString() != "CellPhone")
                {
                    if (rblpatyp.SelectedValue.ToString().Contains("CashAdvance"))
                    {
                        if (txtamountca.Text == "")
                        {
                            txtamountca.Text = "0";
                            msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
                            checkalokasi();
                            fillgrid();
                            return;
                        }

                        if (Convert.ToDecimal(txtamountca.Text) <= 0)
                        {
                            msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
                            checkalokasi();
                            return;
                        }
                        int index = txtamountca.Text.IndexOf(".");
                        if (index > 0)
                        {
                            txtamountca.Text = txtamountca.Text.Substring(0, index);
                            txtamountca.Text = txtamountca.Text.Replace(",", "");
                        }
                        icatot = (Convert.ToInt32(txtamountca.Text));
                        txttotalamt.Text = txtamountca.Text;
                    }
                    else
                    {
                        if (dtaaddcat.Rows.Count > 0)
                        {
                            if (dtaaddcat.Rows.Count == 1)
                            {
                                if (dtaaddcat.Rows[0]["amt"].ToString() == "")
                                {
                                    msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
                                    checkalokasi();
                                    fillgrid();
                                    return;
                                }
                            }
                            result = dtaaddcat.AsEnumerable()
                                   .Sum(x => Convert.ToInt32(x["amt"]));
                            txttotalamt.Text = result.ToString();
                            if (result <= 0)
                            {
                                msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
                                checkalokasi();
                                fillgrid();
                                return;
                            }
                        }
                        else
                        {
                            msg = "<script type='text/javascript'> alert('Please Fill Detail Alokasi'); </script>";
                            checkalokasi();
                            return;
                        }
                    }
                }
                else
                {
                    if (txtdtperiod.Text == "" || txtdtperiod.Text == "01-01-1900")
                    {
                        msg = "<script type='text/javascript'> alert('Please select Billing Period'); </script>";
                        checkalokasi();
                        return;
                    }

                    int index = txtphonebillamt.Text.IndexOf(".");
                    int index2 = txtphonepersonaluse.Text.IndexOf(".");
                    int index3 = txtphoneclaimamount.Text.IndexOf(".");
                    if (index > 0)
                    {
                        txtphonebillamt.Text = txtphonebillamt.Text.Substring(0, index);
                        txtphonebillamt.Text = txtphonebillamt.Text.Replace(",", "");
                    }

                    if (index2 > 0)
                    {
                        txtphonepersonaluse.Text = txtphonepersonaluse.Text.Substring(0, index2);
                        txtphonepersonaluse.Text = txtphonepersonaluse.Text.Replace(",", "");
                    }

                    if (index3 > 0)
                    {
                        txtphoneclaimamount.Text = txtphoneclaimamount.Text.Substring(0, index3);
                        txtphoneclaimamount.Text = txtphoneclaimamount.Text.Replace(",", "");
                    }

                    if (txtphonebillamt.Text == "" || Convert.ToInt32(txtphonebillamt.Text) <= 0)
                    {
                        msg = "<script type='text/javascript'> alert('Billing Amount Cannot 0'); </script>";
                        checkalokasi();
                        return;
                    }

                    if (txtphonebillamt.Text == "") { txtphonebillamt.Text = "0"; }
                    if (txtphonepersonaluse.Text == "") { txtphonepersonaluse.Text = "0"; }
                    if (!string.IsNullOrEmpty(txtphonebillamt.Text) && !string.IsNullOrEmpty(txtphonepersonaluse.Text))
                        txttotalamt.Text = (Convert.ToInt32(txtphonebillamt.Text) - Convert.ToInt32(txtphonepersonaluse.Text)).ToString();

                    txtphoneclaimamount.Text = txttotalamt.Text;
                    icptot = (Convert.ToInt32(txttotalamt.Text));
                }

                arr.Add(new cArrayList("@dt", txtdt.Text));
                arr.Add(new cArrayList("@pa_no", hpano.Value));
                
                if (rblpatyp.SelectedValue.ToString() == "Reimburshment")
                {
                    arr.Add(new cArrayList("@amount", result.ToString()));
                    arr.Add(new cArrayList("@reason", ""));
                }
                if (rblpatyp.SelectedValue.ToString().Contains("CashAdvance"))
                {
                    arr.Add(new cArrayList("@amount", icatot.ToString()));
                    arr.Add(new cArrayList("@reason", txtremarksca.Text));
                }
                if (rblpatyp.SelectedValue.ToString() == "CellPhone")
                {
                    arr.Add(new cArrayList("@amount", icptot.ToString()));
                    arr.Add(new cArrayList("@reason", txtphoneket.Text));
                }
                
                arr.Add(new cArrayList("@pa_to", ddlobehalf.SelectedValue.ToString()));
                arr.Add(new cArrayList("@comp_id", txtcomp.Text));
                arr.Add(new cArrayList("@div_id", hkddiv.Value));
                arr.Add(new cArrayList("@loc_id", txtlokasi.Text));
                arr.Add(new cArrayList("@ccenter", txtkdbiaya.Text));
                arr.Add(new cArrayList("@kdlevel", hkdlvl.Value));
                arr.Add(new cArrayList("@nikrequestor", Session["sNiksite"].ToString()));
                arr.Add(new cArrayList("@kddepar", hkddepar.Value));
                arr.Add(new cArrayList("@kdjabatan", hkkdjabat.Value));
                arr.Add(new cArrayList("@Currcode", "IDR"));
                arr.Add(new cArrayList("@CurratetoUSD", rateusd.ToString()));
                arr.Add(new cArrayList("@request_sts", "draft"));
                arr.Add(new cArrayList("@curr_typ", ddlcurbayar.SelectedValue.ToString()));
                arr.Add(new cArrayList("@curr_rate_accpac", txtcurrraterp.Text));
                arr.Add(new cArrayList("@nobtr", txtbtrno.Text));

                if (rblpatyp.SelectedValue.ToString() == "CellPhone")
                {
                    if (hpano.Value != "")
                    {
                        List<cArrayList> arrdeletedet = new List<cArrayList>();
                        List<cArrayList> arrupddet = new List<cArrayList>();
                        arrdeletedet.Add(new cArrayList("@pa_no", hpano.Value));
                        arrupddet.Add(new cArrayList("@pa_no", hpano.Value));
                        arrupddet.Add(new cArrayList("@coa", cd.coacellphone));
                        arrupddet.Add(new cArrayList("@detail_amt", txtphoneclaimamount.Text));
                        arrupddet.Add(new cArrayList("@remarks", txtphoneket.Text));
                        arrupddet.Add(new cArrayList("@period_dt", txtdtperiod.Text));
                        arrupddet.Add(new cArrayList("@bill_amt", txtphonebillamt.Text));
                        arrupddet.Add(new cArrayList("@personal_amt", txtphonepersonaluse.Text));
                        arr.Add(new cArrayList("@usage_typ", "CP"));
                        bll.vDeleteCatList(arrdeletedet);
                        bll.vUpdateRPA(arr);

                        bll.vInsRPAD(arrupddet, ref vald);
                        msg = "<script type='text/javascript'> alert('Data has been successfully Update'); </script>";
                        
                        checktxtsts();
                        if (txtsts.Text == "Draft")
                        {
                            btnUpload.Enabled = true;
                            fuploadPA.Enabled = true;
                        }
                    }
                    else
                    {
                        arr.Add(new cArrayList("@coa", cd.coacellphone));
                        arr.Add(new cArrayList("@detail_amt", txtphoneclaimamount.Text));
                        arr.Add(new cArrayList("@remarks", txtphoneket.Text));
                        arr.Add(new cArrayList("@period_dt", txtdtperiod.Text));
                        arr.Add(new cArrayList("@bill_amt", txtphonebillamt.Text));
                        arr.Add(new cArrayList("@personal_amt", txtphonepersonaluse.Text));
                        arr.Add(new cArrayList("@usage_typ", "CP"));
                        bll.vInsRPACP(arr, ref val);

                        hpano.Value = val;
                        txtpano.Text = val;
                        msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                        btnprintcp.Visible = true;
                        checktxtsts();
                        if (txtsts.Text == "Draft")
                        {
                            btnUpload.Enabled = true;
                            fuploadPA.Enabled = true;
                        }
                    }
                }
                    //end cellphone
                //start Cashadvance
                if (rblpatyp.SelectedValue.ToString().Contains("CashAdvance"))
                {
                    if (hpano.Value != "")
                    {
                        List<cArrayList> arrdeletedet = new List<cArrayList>();
                        List<cArrayList> arrupddet = new List<cArrayList>();
                        arrdeletedet.Add(new cArrayList("@pa_no", hpano.Value));
                        arrupddet.Add(new cArrayList("@pa_no", hpano.Value));
                        arrupddet.Add(new cArrayList("@coa", hcaglacc.Value));
                        arrupddet.Add(new cArrayList("@detail_amt", txtamountca.Text));
                        arrupddet.Add(new cArrayList("@remarks", txtremarksca.Text));
                        arrupddet.Add(new cArrayList("@period_dt", ""));
                        arrupddet.Add(new cArrayList("@bill_amt", "0"));
                        arrupddet.Add(new cArrayList("@personal_amt", "0"));
                        arr.Add(new cArrayList("@usage_typ", "CA"));
                        bll.vDeleteCatList(arrdeletedet);
                        bll.vUpdateRPA(arr);

                        bll.vInsRPAD(arrupddet, ref vald);
                        msg = "<script type='text/javascript'> alert('Data has been successfully Update'); </script>";

                        checktxtsts();
                        if (txtsts.Text == "Draft")
                        {
                            btnUpload.Enabled = true;
                            fuploadPA.Enabled = true;
                        }
                    }
                    else
                    {
                        arr.Add(new cArrayList("@coa", hcaglacc.Value));
                        arr.Add(new cArrayList("@detail_amt", txtamountca.Text));
                        arr.Add(new cArrayList("@remarks", txtremarksca.Text));
                        arr.Add(new cArrayList("@period_dt", ""));
                        arr.Add(new cArrayList("@bill_amt", "0"));
                        arr.Add(new cArrayList("@personal_amt", "0"));
                        arr.Add(new cArrayList("@usage_typ", "CA"));
                        bll.vInsRPACP(arr, ref val);

                        hpano.Value = val;
                        txtpano.Text = val;
                        msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                        btnprintcp.Visible = true;
                        checktxtsts();
                        if (txtsts.Text == "Draft")
                        {
                            btnUpload.Enabled = true;
                            fuploadPA.Enabled = true;
                        }
                    }
                }
                //start reimbursement
                if (rblpatyp.SelectedValue.ToString() == "Reimburshment")
                {
                    if (hpano.Value != "")
                    {
                        List<cArrayList> arrdeletedet = new List<cArrayList>();
                        //List<cArrayList> arrupddet = new List<cArrayList>();
                        arrdeletedet.Add(new cArrayList("@pa_no", hpano.Value));
                        //arrupddet.Add(new cArrayList("@pa_no", hpano.Value));
                        //arrupddet.Add(new cArrayList("@coa", cd.coacellphone));
                        //arrupddet.Add(new cArrayList("@detail_amt", txtphoneclaimamount.Text));
                        //arrupddet.Add(new cArrayList("@remarks", txtphoneket.Text));
                        //arrupddet.Add(new cArrayList("@period_dt", txtdtperiod.Text));
                        //arrupddet.Add(new cArrayList("@bill_amt", txtphonebillamt.Text));
                        //arrupddet.Add(new cArrayList("@personal_amt", txtphonepersonaluse.Text));
                        if (rblpatyp.SelectedValue.ToString() == "CashAdvance" || rblpatyp.SelectedValue.ToString() == "CashAdvancebtr")
                        {
                            arr.Add(new cArrayList("@usage_typ", "CA"));
                        }
                        else
                        {
                            arr.Add(new cArrayList("@usage_typ", "RI"));
                        }
                        bll.vDeleteCatList(arrdeletedet);
                        bll.vUpdateRPA(arr);

                        if (dtaaddcat.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtaaddcat.Rows.Count; i++)
                            {
                                List<cArrayList> arrd = new List<cArrayList>();
                                arrd.Add(new cArrayList("@pa_no", hpano.Value));
                                arrd.Add(new cArrayList("@coa", dtaaddcat.Rows[i]["category-id"].ToString()));
                                arrd.Add(new cArrayList("@detail_amt", dtaaddcat.Rows[i]["amt"].ToString()));
                                arrd.Add(new cArrayList("@remarks", dtaaddcat.Rows[i]["remarks"].ToString()));
                                arrd.Add(new cArrayList("@period_dt", ""));
                                arrd.Add(new cArrayList("@bill_amt", dtaaddcat.Rows[i]["amt"].ToString()));
                                arrd.Add(new cArrayList("@personal_amt", 0));
                                bll.vInsRPAD(arrd, ref vald);
                            }
                        }
                        msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                        btnprint.Visible = true;
                        checktxtsts();
                        if (txtsts.Text == "Draft")
                        {
                            btnUpload.Enabled = true;
                            fuploadPA.Enabled = true;
                        }
                    }
                    else
                    {
                        if (rblpatyp.SelectedValue.ToString() == "CashAdvance" || rblpatyp.SelectedValue.ToString() == "CashAdvancebtr")
                        {
                            arr.Add(new cArrayList("@coa", ""));
                            arr.Add(new cArrayList("@detail_amt", ""));
                            arr.Add(new cArrayList("@remarks", ""));
                            arr.Add(new cArrayList("@period_dt", ""));
                            arr.Add(new cArrayList("@bill_amt", ""));
                            arr.Add(new cArrayList("@personal_amt", ""));
                            arr.Add(new cArrayList("@usage_typ", "CA"));

                            bll.vInsRPA(arr, ref val);

                            if (dtaaddcat.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtaaddcat.Rows.Count; i++)
                                {
                                    List<cArrayList> arrd = new List<cArrayList>();
                                    arrd.Add(new cArrayList("@pa_no", val));
                                    arrd.Add(new cArrayList("@coa", dtaaddcat.Rows[i]["category-id"].ToString()));
                                    arrd.Add(new cArrayList("@detail_amt", dtaaddcat.Rows[i]["amt"].ToString()));
                                    arrd.Add(new cArrayList("@remarks", dtaaddcat.Rows[i]["remarks"].ToString()));
                                    arrd.Add(new cArrayList("@period_dt", ""));
                                    arrd.Add(new cArrayList("@bill_amt", dtaaddcat.Rows[i]["amt"].ToString()));
                                    arrd.Add(new cArrayList("@personal_amt", 0));
                                    bll.vInsRPAD(arrd, ref vald);
                                }
                            }
                            msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                            btnprint.Visible = true;
                            checktxtsts();
                            if (txtsts.Text == "Draft")
                            {
                                btnUpload.Enabled = true;
                                fuploadPA.Enabled = true;
                            }
                        }
                        else
                        {
                            arr.Add(new cArrayList("@coa", ""));
                            arr.Add(new cArrayList("@detail_amt", ""));
                            arr.Add(new cArrayList("@remarks", ""));
                            arr.Add(new cArrayList("@period_dt", ""));
                            arr.Add(new cArrayList("@bill_amt", ""));
                            arr.Add(new cArrayList("@personal_amt", ""));
                            arr.Add(new cArrayList("@usage_typ", "RI"));
                            bll.vInsRPA(arr, ref val);

                            if (dtaaddcat.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtaaddcat.Rows.Count; i++)
                                {
                                    List<cArrayList> arrd = new List<cArrayList>();
                                    arrd.Add(new cArrayList("@pa_no", val));
                                    arrd.Add(new cArrayList("@coa", dtaaddcat.Rows[i]["category-id"].ToString()));
                                    arrd.Add(new cArrayList("@detail_amt", dtaaddcat.Rows[i]["amt"].ToString()));
                                    arrd.Add(new cArrayList("@remarks", dtaaddcat.Rows[i]["remarks"].ToString()));
                                    arrd.Add(new cArrayList("@period_dt", ""));
                                    arrd.Add(new cArrayList("@bill_amt", dtaaddcat.Rows[i]["amt"].ToString()));
                                    arrd.Add(new cArrayList("@personal_amt", 0));
                                    bll.vInsRPAD(arrd, ref vald);
                                }
                            }

                            msg = "<script type='text/javascript'> alert('Data has been successfully saved'); </script>";
                            btnprint.Visible = true;
                            checktxtsts();
                            if (txtsts.Text == "Draft")
                            {
                                btnUpload.Enabled = true;
                                fuploadPA.Enabled = true;
                            }
                        }
                        hpano.Value = val;
                        txtpano.Text = val;
                        btnUpload.Enabled = true;
                        checktxtsts();
                    }
                }
                if (rblpatyp.SelectedValue.ToString() != "CellPhone")
                {
                    csscp = "style='display:none;'";
                    csscanew = "style='display:none;'";
                }

                checkalokasi();
            }
            else
            {
                checkalokasi();
                msg = "<script type='text/javascript'> alert('Your request is in the process, changes are not allowed'); </script>";
            }
        }

        protected void UploadFile(object sender, EventArgs e)
        {
            //string folderPath = Server.MapPath("\\20.110.1.30\\Files\\upload_" + DateTime.Now.ToString("yyyyMMdd") + "/");
            //string folderPath = "\\\\20.110.1.30\\Files\\upload_" + DateTime.Now.ToString("yyyyMMdd") + "\\" + hpano.Value + "\\";
            string folderPath = "\\\\" + ServerUpload + "\\Files\\upload_" + DateTime.Now.ToString("yyyyMMdd") + "\\" + hpano.Value + "\\";
            //20.110.1.30
            //Check whether Directory (Folder) exists.
            if (!Directory.Exists(folderPath))
            {
                //If Directory (Folder) does not exists. Create it.
                Directory.CreateDirectory(folderPath);
            }
            if (fuploadPA.PostedFile.ContentLength < 5000000)
            {
                if (fuploadPA.FileName != "")
                {
                    if (fuploadPA.FileName.Length > 128)
                    {
                        msg = "<script type='text/javascript'> alert('File Name cannot over than 128 character long'); </script>"; ;
                        checkalokasi();
                        fillgrid();
                        return;
                    }

                    DataTable dtafn = new DataTable();
                    List<cArrayList> arrfn = new List<cArrayList>();
                    arrfn = new List<cArrayList>();
                    arrfn.Add(new cArrayList("@pa_no", txtpano.Text));
                    DataTable dtah = new DataTable();
                    bll.vGetFilenm(ref dtafn, arrfn);

                    if (dtafn.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtafn.Rows.Count; q++)
                        {
                            if (fuploadPA.FileName == dtafn.Rows[q]["filename"].ToString())
                            {
                                msg = "<script type='text/javascript'> alert('File already exist please select another file'); </script>"; ;
                                checkalokasi();
                                fillgrid();
                                return;
                            }
                        }
                    }

                    string fName = Path.GetFileNameWithoutExtension(fuploadPA.FileName);
                    DataTable dtchkrestct = new DataTable();
                    List<cArrayList> arrcr = new List<cArrayList>();
                    arrcr.Add(new cArrayList("@mainstring", fName));
                    bll.vGetRestrictFn(ref dtchkrestct, arrcr);
                    if (dtchkrestct.Rows.Count > 0)
                    {
                        msg = @"<script type='text/javascript'> alert(""Filename Cannot Contain " + dtchkrestct.Rows[0]["restrict_char"].ToString() + "\"); </script>"; ;
                        checkalokasi();
                        fillgrid();
                        return;
                    }

                    //Save the File to the Directory (Folder).
                    fuploadPA.SaveAs(folderPath + Path.GetFileName(fuploadPA.FileName));

                    //Display the success message.
                    lblMessage.Text = Path.GetFileName(fuploadPA.FileName) + " has been uploaded.";

                    List<cArrayList> arr = new List<cArrayList>();

                    arr.Add(new cArrayList("@FileName", fuploadPA.FileName));
                    arr.Add(new cArrayList("@FileExtension", Path.GetExtension(fuploadPA.PostedFile.FileName)));
                    arr.Add(new cArrayList("@fpath", folderPath + Path.GetFileName(fuploadPA.FileName)));
                    arr.Add(new cArrayList("@pa_no", hpano.Value));
                    arr.Add(new cArrayList("@fsize", fuploadPA.PostedFile.ContentLength));

                    bll.vInsertfupload(arr);

                    List<cArrayList> arr2 = new List<cArrayList>();
                    arr2.Add(new cArrayList("@pa_no", txtpano.Text));
                    DataTable dta = new DataTable();
                    bll.vGetListfupld(ref dta, arr2);

                    gvfupld.DataSource = dta;
                    gvfupld.DataBind();

                    var result = dta.AsEnumerable()
                            .Sum(x => Convert.ToInt32(x["fsize"]));
                    hfsize.Value = result.ToString();
                }
                else
                {
                    msg = "<script type='text/javascript'> alert('File empty, please select file first'); </script>";
                }
            }
            else
            {
                msg = "<script type='text/javascript'> alert('File Upload Cannot more than 5MB'); </script>";
            }
            if (rblpatyp.SelectedValue.ToString() != "CellPhone")
            {
                csscp = "style='display:none;'";
                csscp = "style='display:none;'";
            }
            checkalokasi();
            fillgrid();
        }

        protected void btnprint_Click(object sender, EventArgs e)
        {
            if (rblpatyp.SelectedValue.ToString() == "Reimburshment" || rblpatyp.SelectedValue.ToString() == "CashAdvance")
            {
                Response.Redirect("print_pa.aspx?cat=0");
            }
            else
            {
                Response.Redirect("print_pa.aspx?cat=1");
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;

            fillgridedit();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (dtaaddcat.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtaaddcat.Rows.Count; i++)
            //    {
            //        if (dtaaddcat.Rows[i]["amt"].ToString() == "")
            //        {
            //            msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
            //            checkalokasi();
            //            fillgrid();
            //            return;
            //        }
            //    }
            //}

            if (e.CommandName.Equals("AddNew"))
            {
                DropDownList ddlcatlist = (DropDownList)GridView1.FooterRow.FindControl("ddlcatlistnew");
                TextBox txtNewbiayatot = (TextBox)GridView1.FooterRow.FindControl("txtNewbiayatot");
                TextBox txtNewremarks = (TextBox)GridView1.FooterRow.FindControl("txtNewremarks");

                if (txtNewbiayatot.Text == "" || txtNewbiayatot.Text == "0")
                {
                    msg = "<script type='text/javascript'> alert('Amount Cannot 0'); </script>";
                    checkalokasi();
                    fillgrid();
                    return;
                }
                //dtaaddcat = new DataTable();

                //bll.InsertReimbursh(ddlcatlist.SelectedItem.ToString(), txtNewbiayatot.Text, txtNewremarks.Text, dtaaddcat);
                bll.InsertReimbursh(ddlcatlist.SelectedItem.ToString(), txtNewbiayatot.Text, txtNewremarks.Text, dtaaddcat, ddlcatlist.SelectedValue.ToString());
                fillgrid();

                var result = dtaaddcat.AsEnumerable()
                    .Sum(x => Convert.ToInt32(x["amt"]));

                txttotalamt.Text = result.ToString();

                txttotalamt.Text = String.Format("{0:n}", Convert.ToInt32(txttotalamt.Text));

                csscp = "style='display:none;'";
                csscanew = "style='display:none;'";

                btrnumber = txtbtrno.Text;

                checkalokasi();
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            bll.DeleteReimbursh(dtaaddcat, e.RowIndex);
            fillgrid();
            var result = dtaaddcat.AsEnumerable()
                    .Sum(x => Convert.ToInt32(x["amt"]));

            txttotalamt.Text = result.ToString();

            csscp = "style='display:none;'";
            csscanew = "style='display:none;'";
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            gveditindex = GridView1.EditIndex;
            fillgridedit();

            List<cArrayList> arr = new List<cArrayList>();
            DropDownList ddlcatlist = (DropDownList)GridView1.Rows[GridView1.EditIndex].FindControl("ddlcatlist");
            
            arr = new List<cArrayList>();
            DataTable dtadesc = new DataTable();
            arr.Add(new cArrayList("@desc", "%%"));
            bll.vGetDescList(ref dtadesc, arr);

            if (dtadesc.Rows.Count > 0)
            {
                ddlcatlist.DataSource = dtadesc;
                ddlcatlist.DataTextField = "DESCRIPTION";
                ddlcatlist.DataValueField = "COA";
                ddlcatlist.DataBind();
            }

            ddlcatlist.SelectedValue = dtaaddcat.Rows[GridView1.EditIndex]["category-id"].ToString();

            csscp = "style='display:none;'";
            csscanew = "style='display:none;'";
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            DropDownList ddlcatlist = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlcatlist");
            TextBox txtNewbiayatot = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtbiayatot");
            TextBox txtNewremarks = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtremarks");

            bll.UpdateReimbursh(ddlcatlist.SelectedItem.ToString(), txtNewbiayatot.Text, txtNewremarks.Text, dtaaddcat, e.RowIndex, ddlcatlist.SelectedValue.ToString());
            GridView1.EditIndex = -1;
            fillgrid();
            var result = dtaaddcat.AsEnumerable()
                    .Sum(x => Convert.ToInt32(x["amt"]));

            txttotalamt.Text = result.ToString();

            txttotalamt.Text = String.Format("{0:n}", Convert.ToInt32(txttotalamt.Text));

            csscp = "style='display:none;'";
            csscanew = "style='display:none;'";
        }

        private void fillgrid()
        {
            //DataTable dt = new DataTable();
            //dt = Session["myDatatable"] as DataTable;

            if (dtaaddcat.Rows.Count > 0)
            {
                if (dtaaddcat.Rows[0]["amt"].ToString() == "")
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();
                    int TotalColumns = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                    GridView1.Rows[0].Cells[0].Text = "Entry Reimbursement";
                    GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                    Session["myDatatable"] = dtaaddcat;

                    fillddlcat("ddlcatlist");
                }
                else
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();

                    fillddlcat("ddlcatlistnew");
                }
            }
            else
            {
                dtaaddcat.Rows.Add(dtaaddcat.NewRow());
                GridView1.DataSource = dtaaddcat;
                GridView1.DataBind();
                int TotalColumns = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                GridView1.Rows[0].Cells[0].Text = "Entry Reimbursement";
                GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                Session["myDatatable"] = dtaaddcat;

                fillddlcat("ddlcatlistnew");
            }
            //tblcellphone.Visible = false;
        }

        private void fillgridedit()
        {
            //int icount;
            if (dtaaddcat.Rows.Count > 0)
            {
                if (dtaaddcat.Rows[0]["amt"].ToString() == "")
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();
                    int TotalColumns = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                    GridView1.Rows[0].Cells[0].Text = "Entry Reimbursement";
                    GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                    Session["myDatatable"] = dtaaddcat;

                    fillddlcat("ddlcatlist");
                }
                else
                {
                    GridView1.DataSource = dtaaddcat;
                    GridView1.DataBind();
                    fillddlcat("ddlcatlistnew");
                }
            }
            else
            {
                dtaaddcat.Rows.Add(dtaaddcat.NewRow());
                GridView1.DataSource = dtaaddcat;
                GridView1.DataBind();
                int TotalColumns = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                GridView1.Rows[0].Cells[0].Text = "Entry Reimbursement";
                GridView1.Rows[0].ForeColor = System.Drawing.Color.White;
                Session["myDatatable"] = dtaaddcat;

                fillddlcat("ddlcatlistnew");
            }
            tblcellphone.Visible = false;
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            //SqlDataReader rs = null;
            //List<cArrayList> arr = new List<cArrayList>();

            //GridViewRow footrow = GridView1.FooterRow;
            ////DropDownList ddlcatlist = (DropDownList)GridView1.Rows[0].FindControl("ddlcatlist");
            //DropDownList ddlcatlist = (DropDownList)footrow.FindControl("ddlcatlist");

            //rs = null;
            //arr = new List<cArrayList>();
            //DataTable dtadesc = new DataTable();
            //arr.Add(new cArrayList("@desc", "%%"));
            //bll.vGetDescList(ref dtadesc, arr);

            //if (dtadesc.Rows.Count > 0)
            //{
            //    ddlcatlist.DataSource = dtadesc;
            //    ddlcatlist.DataTextField = "DESCRIPTION";
            //    ddlcatlist.DataValueField = "COA";
            //    ddlcatlist.DataBind();
            //}
        }

        protected void btnprintcp_Click(object sender, EventArgs e)
        {
            Response.Redirect("print_pa.aspx?cat=1&no=" + Session["pa_no"].ToString());
        }

        protected void btnprintpa_Click(object sender, EventArgs e)
        {
            Response.Redirect("print_pa.aspx?cat=0&no=" + Session["pa_no"].ToString());
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (rblpatyp.SelectedValue.ToString() == "Reimburshment" || rblpatyp.SelectedValue.ToString() == "CellPhone" || rblpatyp.SelectedValue.ToString() == "CashAdvance")
            {
                if (txttotalamt.Text == "0" || txttotalamt.Text == "")
                {
                    msg = "<script type='text/javascript'> alert('Failed : Amount Cannot 0'); </script>";
                    checkalokasi();
                    return;
                }

                if (gvfupld.Rows.Count > 0)
                {
                    List<cArrayList> arr = new List<cArrayList>();
                    arr.Add(new cArrayList("@DocNumber", hpano.Value));


                    bll.vSubmitPA2(arr);

                    txtsts.Text = "INT";
                    checktxtsts();
                    btnsubmit.Enabled = false;

                    msg = "<script type='text/javascript'> alert('Data has been successfully submit'); </script>";
                }
                else
                {
                    checktxtsts();
                    msg = "<script type='text/javascript'> alert('Supporting documents must be filled in'); </script>";
                }
            }
            else
            {
                if (txttotalamt.Text == "0" || txttotalamt.Text == "")
                {
                    msg = "<script type='text/javascript'> alert('Failed : Amount Cannot 0'); </script>";
                    checkalokasi();
                    return;
                }

                List<cArrayList> arr = new List<cArrayList>();
                arr.Add(new cArrayList("@DocNumber", hpano.Value));


                bll.vSubmitPA2(arr);

                txtsts.Text = "INT";
                checktxtsts();
                btnsubmit.Enabled = false;
                msg = "<script type='text/javascript'> alert('Data has been successfully submit'); </script>";
            }

            checkalokasi();
            
        }

        private void fillddlcat(string sddlcatid)
        {
            List<cArrayList> arr = new List<cArrayList>();

            GridViewRow footrow = GridView1.FooterRow;
            GridViewRow editrow = GridView1.SelectedRow; 
            //DropDownList ddlcatlist = (DropDownList)GridView1.Rows[0].FindControl("ddlcatlist");
            DropDownList ddlcatlist = (DropDownList)footrow.FindControl(sddlcatid);

            arr = new List<cArrayList>();
            DataTable dtadesc = new DataTable();
            arr.Add(new cArrayList("@desc", "%%"));
            bll.vGetDescList(ref dtadesc, arr);

            if (dtadesc.Rows.Count > 0)
            {
                if (ddlcatlist != null)
                {
                    ddlcatlist.DataSource = dtadesc;
                    ddlcatlist.DataTextField = "DESCRIPTION";
                    ddlcatlist.DataValueField = "COA";
                    ddlcatlist.DataBind();
                }
                else
                {
                    ddlcatlist = (DropDownList)footrow.FindControl("ddlcatlistnew");
                    ddlcatlist.DataSource = dtadesc;
                    ddlcatlist.DataTextField = "DESCRIPTION";
                    ddlcatlist.DataValueField = "COA";
                    ddlcatlist.DataBind();
                }
            }
        }

        protected void ddlcurbayar_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<cArrayList> arr = new List<cArrayList>();
            DataTable dta = new DataTable();
            arr.Add(new cArrayList("@homecur", "IDR"));
            arr.Add(new cArrayList("@sourcecur", ddlcurbayar.SelectedValue.ToString()));
            arr.Add(new cArrayList("@pycostcode", txtlokasi.Text));
            bll.vGetExcRate(ref dta, arr);


            if (dta.Rows.Count > 0)
            {
                txtcurrraterp.Text = dta.Rows[0]["rate"].ToString();
            }
            else
            {
                if (ddlcurbayar.SelectedValue == "IDR")
                {
                    txtcurrraterp.Text = "1";
                }
                else
                {
                    txtcurrraterp.Text = "";
                }
            }

            if (rblpatyp.SelectedValue.ToString() == "CellPhone")
            {
                csscp = "style='display:none;'";
                cssca = "style='display:none;'";
                csscanew = "style='display:none;'";
            }

            if (rblpatyp.SelectedValue.ToString() == "Reimburshment")
            {
                cssca = "style='display:none;'";
                csscanew = "style='display:none;'";
                csscp = "style='display:none;'";
            }

            if (rblpatyp.SelectedValue.ToString() == "CashAdvance")
            {
                cssca = "style='display:none;'";
                csscp = "style='display:none;'";
                csscanew = "style='display:block;'";
            }

            if (rblpatyp.SelectedValue.ToString() == "CashAdvancebtr")
            {
                cssca = "style='display:none;'";
                csscp = "style='display:none;'";
                csscanew = "style='display:block;'";
            }

            fillgrid();
        }

        private void checkalokasi()
        {
            if (rblpatyp.SelectedValue.ToString() == "CellPhone")
            {
                csscp = "style='display:block;'";
                cssca = "style='display:none;'";
                csscanew = "style='display:none;'";
                GridView1.Visible = false;
            }

            if (rblpatyp.SelectedValue.ToString() == "CashAdvancebtr")
            {
                csscp = "style='display:none;'";
                cssca = "style='display:block;'";
                csscanew = "style='display:block;'";
                GridView1.Visible = false;
            }

            if (rblpatyp.SelectedValue.ToString() == "CashAdvance")
            {
                csscp = "style='display:none;'";
                cssca = "style='display:none;'";
                csscanew = "style='display:block;'";
                GridView1.Visible = false;
            }

            if (rblpatyp.SelectedValue.ToString() == "Reimburshment")
            {
                csscp = "style='display:none;'";
                cssca = "style='display:none;'";
                csscanew = "style='display:none;'";
                GridView1.Visible = true;
            }
            btnprint.Visible = false;
        }

        private void checktxtsts()
        {
            if (txtsts.Text != "Draft")
            {
                btnsubmit.Enabled = false;
                btnsave.Enabled = false;
                rblpatyp.Enabled = false;
                ddlcurbayar.Enabled = false;
                GridView1.Enabled = false;
                tblcellphone.Enabled = false;
                fuploadPA.Enabled = false;
                gvfupld.Enabled = false;
                btnUpload.Enabled = false;
                cssdiscard = "style='display:none;'";
            }
            else
            {
                btnsubmit.Enabled = true;
                btnsave.Enabled = true;
                cssdiscard = "style='display:inline;'";
            }
        }

        protected void gvfupld_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //e.RowIndex
            
            List<cArrayList> arrdeletedet = new List<cArrayList>();
            arrdeletedet.Add(new cArrayList("@pa_no", hpano.Value));
            arrdeletedet.Add(new cArrayList("@filenm", gvfupld.Rows[e.RowIndex].Cells[2].Text));
            bll.vDeleteFilename(arrdeletedet);


            List<cArrayList> arr2 = new List<cArrayList>();
            arr2.Add(new cArrayList("@pa_no", txtpano.Text));
            DataTable dta = new DataTable();
            bll.vGetListfupld(ref dta, arr2);

            gvfupld.DataSource = dta;
            gvfupld.DataBind();
            checkalokasi();
        }

        protected void rdlchange(object sender, EventArgs e)
        {

        }

        protected void ddlobehalf_SelectedIndexChanged(object sender, EventArgs e)
        {
            sNiksite = ddlobehalf.SelectedValue.ToString();

            Session["NikObh"] = bll.sExecuteSQL("select nik from jrn_test..h_A101 where stedit = 0 and active=1 and niksite = '" + sNiksite + "'");
            Session["NiksiteObh"] = sNiksite;
            SqlDataReader rs = null;
            List<cArrayList> arr = new List<cArrayList>();

            rs = null;
            arr = new List<cArrayList>();
            arr.Add(new cArrayList("@nik", sNiksite));
            bll.vGetEmpAcc(ref rs, arr);

            while (rs.Read())
            {
                //ListViewItem li = lvw.Items.Add(rs["polygon_id"].ToString());
                //txtnik.Text = rs["NikSite"].ToString();
                txtbanknm.Text = rs["NameBank"].ToString();
                txtaccno.Text = rs["NoRekening"].ToString();
                txtcomp.Text = rs["KdCompany"].ToString();
                txtlokasi.Text = rs["Kdsite"].ToString();
                txtdivisi.Text = rs["Divisi"].ToString();
                txtkepada.Text = rs["Nama"].ToString();
                txtkdbiaya.Text = rs["CostCenter"].ToString();
                txtbankaddress.Text = rs["AddressBank"].ToString();
                txtpenerima.Text = rs["NameRekening"].ToString();
                txtcabang.Text = rs["BankBranch"].ToString();
                hkdlvl.Value = rs["KdLevel"].ToString();
                hkddepar.Value = rs["Kddepar"].ToString();
                hkkdjabat.Value = rs["KdJabat"].ToString();
                hkddiv.Value = rs["KdDivisi"].ToString();
            }
            rs.Close();
            checkalokasi();
            fillgrid();
            chkCP();
        }

        private void chkCP()
        {
            List<cArrayList> arrchkcp = new List<cArrayList>();
            arrchkcp = new List<cArrayList>();
            arrchkcp.Add(new cArrayList("@niksite", ddlobehalf.SelectedValue.ToString()));
            DataTable dtachk = new DataTable();
            bll.vChkCP(ref dtachk, arrchkcp);
            if (dtachk.Rows.Count > 0)
            {
                if (dtachk.Rows[0]["fhitungot"].ToString() == "1")
                {
                    rblpatyp.Items[3].Enabled = true;
                }
                else
                {
                    rblpatyp.Items[3].Enabled = false;
                }
            }
            else
            {
                rblpatyp.Items[3].Enabled = false;
            }
        }
    }
}